import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import appValidator from "../AppValidator.js";

const maxNameLength = 50;

class ProductForm extends React.Component {
  state = {
    name: "",
    isLoading: true
  };

  componentDidMount() {
    if (this.props.match.params.productId) {
      this.getProduct(this.props.match.params.productId);
    } else {
      this.setState({ isLoading: false });
    }
  }

  getProduct = productId => {
    axios
      .get("/api/product/" + productId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustProductFromDatabaseToState)
      .catch(error => {
        console.log(error);
      });
  };

  adjustProductFromDatabaseToState = product => {
    if (product) {
      this.setState({
        productId: product.productId,
        name: product.name,
        isLoading: false
      });
    } else {
      console.log("Product does not exist");
    }
  };

  adjustProductFromStateToDatabase = () => {
    var newProduct = {
      productId: this.state.productId,
      name: this.state.name
    };

    return newProduct;
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  isFormNotValid = () => {
    var isNotValid = !appValidator.isCorrectLength(this.state.name, {
      min: 1,
      max: maxNameLength
    });
    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    var product = this.adjustProductFromStateToDatabase();
    if (product.productId) {
      this.updateProduct(product);
    } else {
      this.createProduct(product);
    }
  };

  createProduct = product => {
    axios
      .post("/api/product", product)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  updateProduct = product => {
    axios
      .put("/api/product/" + product.productId, product)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <PageHeader title={this.props.title} />
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <TextField
            label="Product name"
            error={
              appValidator.sayIfNotCorrectLength(this.state.name, {
                max: maxNameLength
              })
                ? true
                : false
            }
            helperText={appValidator.sayIfNotCorrectLength(this.state.name, {
              max: maxNameLength
            })}
            className={textFieldStyle.longTextField}
            value={this.state.name}
            onChange={this.handleChange("name")}
            margin="normal"
          />

          <br />
          <br />

          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(ProductForm);
