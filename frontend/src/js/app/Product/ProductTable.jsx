import React from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class ProductTable extends React.Component {
  state = {
    initialData: [],
    chosenProductId: "",
    isLoading: true
  };

  componentDidMount() {
    this.getData();
  }

  getData() {
    axios.get("/api/product").then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "name", label: "Name" }
    ];

    const data = this.state.initialData.map(row => {
      return [row.productId, row.name];
    });

    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.productId === rowData[0];
        });
        if (this.props.actionOnChosenData) {
          this.props.actionOnChosenData(chosenObject);
        } else {
          this.setState({ chosenProductId: chosenObject.productId });
        }
      }
    };

    let showCreateNewProductButton = toShow => {
      if (toShow) {
        return (
          <React.Fragment>
            <br />

            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={"/add/product"}
            >
              New Product
            </Button>
          </React.Fragment>
        );
      }
    };

    if (this.state.chosenProductId) {
      return <Redirect push to={"/product/" + this.state.chosenProductId} />;
    }

    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <MUIDataTable
          title={"Product List"}
          data={data}
          columns={columns}
          options={options}
        />

        {showCreateNewProductButton(!this.props.noButtons)}
      </div>
    );
  }
}

export default ProductTable;
