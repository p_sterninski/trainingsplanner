import React from "react";
import PageHeader from "../other/PageHeader.jsx";

const NotFoundPage = props => {
  return (
    <div>
      <PageHeader title={props.title} />
      <p>Page cannot be found</p>
    </div>
  );
};

export default NotFoundPage;
