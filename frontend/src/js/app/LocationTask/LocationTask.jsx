import React from "react";
import DateFnsUtils from "@date-io/date-fns";
import Grid from "@material-ui/core/Grid";
import {
  MuiPickersUtilsProvider,
  TimePicker,
  DatePicker
} from "material-ui-pickers";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import PaperForm from "../other/PaperForm.jsx";

import appValidator from "../AppValidator.js";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";
import gridStyle from "../../styles/GridStyle.module.css";

const LocationTask = props => {
  return (
    <Grid container>
      <PaperForm className={paperStyle.widerPaper}>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="flex-start"
          className={gridStyle.gridWithElementsOnRow}
        >
          <Grid item>
            <Grid container alignItems="baseline">
              <TextField
                disabled
                label="Location"
                className={textFieldStyle.textField}
                value={props.locationTask.room.location.name}
                margin="normal"
              />

              <Button
                variant="contained"
                color="inherit"
                className={textFieldStyle.textField}
                size="medium"
                type="button"
                onClick={props.moveToNextStep("")}
              >
                Find Location
              </Button>
            </Grid>

            <TextField
              disabled
              label="Adress"
              className={textFieldStyle.longTextField}
              value={
                props.locationTask.room.location.address
                  ? props.locationTask.room.location.address +
                    ", " +
                    props.locationTask.room.location.city.name +
                    ", " +
                    props.locationTask.room.location.city.country.name
                  : ""
              }
              margin="normal"
            />

            <br />

            <TextField
              disabled
              label="Room"
              className={textFieldStyle.textField}
              value={props.locationTask.room.roomNo}
              margin="normal"
            />

            <TextField
              disabled
              label="Floor"
              className={textFieldStyle.textField}
              value={props.locationTask.room.floorNo}
              margin="normal"
            />

            <br />

            <TextField
              disabled
              label="Phone"
              className={textFieldStyle.longTextField}
              value={appValidator.replaceNull(
                props.locationTask.room.location.phone,
                ""
              )}
              margin="normal"
            />

            <br />

            <TextField
              disabled
              label="Email"
              className={textFieldStyle.longTextField}
              value={appValidator.replaceNull(
                props.locationTask.room.location.email,
                ""
              )}
              margin="normal"
            />
          </Grid>
          <Grid item>
            <TextField
              disabled
              label="Max capacity"
              className={textFieldStyle.textField}
              value={props.locationTask.room.capacity}
              margin="normal"
            />

            <br />

            <TextField
              required={true}
              error={
                appValidator.sayIfNotInteger(
                  props.locationTask.expectedNoOfAttendees,
                  { min: 1, max: props.locationTask.room.capacity }
                )
                  ? true
                  : false
              }
              helperText={appValidator.sayIfNotInteger(
                props.locationTask.expectedNoOfAttendees,
                { min: 1, max: props.locationTask.room.capacity }
              )}
              label="Expected no. of attendees"
              className={textFieldStyle.textField}
              value={props.locationTask.expectedNoOfAttendees}
              onChange={props.handleObjectChange(
                "locationTask",
                "expectedNoOfAttendees"
              )}
              margin="normal"
            />

            <TextField
              error={
                appValidator.sayIfNotInteger(
                  props.locationTask.actualNoOfAttendees,
                  { min: 1 }
                )
                  ? true
                  : false
              }
              helperText={appValidator.sayIfNotInteger(
                props.locationTask.actualNoOfAttendees,
                { min: 1 }
              )}
              label="Actual no. of attendees"
              className={textFieldStyle.textField}
              value={appValidator.replaceNull(
                props.locationTask.actualNoOfAttendees,
                ""
              )}
              onChange={props.handleObjectChange(
                "locationTask",
                "actualNoOfAttendees"
              )}
              margin="normal"
            />

            <br />

            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid container>
                <DatePicker
                  required={true}
                  error={
                    appValidator.compareDate(
                      props.locationTask.startDate,
                      props.locationTask.endDate
                    ) >= 0
                  }
                  helperText={appValidator.sayIfDateIsAfterOrEqual(
                    props.locationTask.startDate,
                    props.locationTask.endDate
                  )}
                  margin="normal"
                  label="Start date"
                  className={textFieldStyle.textField}
                  value={props.locationTask.startDate}
                  onChange={props.handleDateChange("locationTask", "startDate")}
                />
                <TimePicker
                  required={true}
                  error={
                    appValidator.compareDate(
                      props.locationTask.startDate,
                      props.locationTask.endDate
                    ) >= 0
                  }
                  helperText={appValidator.sayIfDateIsAfterOrEqual(
                    props.locationTask.startDate,
                    props.locationTask.endDate
                  )}
                  margin="normal"
                  label="Start time"
                  className={textFieldStyle.textField}
                  value={props.locationTask.startDate}
                  onChange={props.handleDateChange("locationTask", "startDate")}
                />
              </Grid>
            </MuiPickersUtilsProvider>

            <MuiPickersUtilsProvider utils={DateFnsUtils}>
              <Grid container>
                <DatePicker
                  required={true}
                  error={
                    appValidator.compareDate(
                      props.locationTask.startDate,
                      props.locationTask.endDate
                    ) >= 0
                  }
                  helperText={appValidator.sayIfDateIsBeforeOrEqual(
                    props.locationTask.endDate,
                    props.locationTask.startDate
                  )}
                  margin="normal"
                  label="End date"
                  className={textFieldStyle.textField}
                  value={props.locationTask.endDate}
                  onChange={props.handleDateChange("locationTask", "endDate")}
                />
                <TimePicker
                  required={true}
                  error={
                    appValidator.compareDate(
                      props.locationTask.startDate,
                      props.locationTask.endDate
                    ) >= 0
                  }
                  helperText={appValidator.sayIfDateIsBeforeOrEqual(
                    props.locationTask.endDate,
                    props.locationTask.startDate
                  )}
                  margin="normal"
                  label="End time"
                  className={textFieldStyle.textField}
                  value={props.locationTask.endDate}
                  onChange={props.handleDateChange("locationTask", "endDate")}
                />
              </Grid>
            </MuiPickersUtilsProvider>

            <TextField
              error={
                appValidator.sayIfNotCorrectLength(
                  props.locationTask.description,
                  {
                    max: props.maxDescriptionLength
                  }
                )
                  ? true
                  : false
              }
              helperText={appValidator.sayIfNotCorrectLength(
                props.locationTask.description,
                {
                  max: props.maxDescriptionLength
                }
              )}
              label="Description"
              multiline
              rowsMax="3"
              className={textFieldStyle.longTextField}
              value={props.locationTask.description}
              onChange={props.handleObjectChange("locationTask", "description")}
              margin="normal"
            />
          </Grid>
        </Grid>
      </PaperForm>
    </Grid>
  );
};

export default LocationTask;
