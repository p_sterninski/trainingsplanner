import React from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import Fab from "@material-ui/core/Fab";
import PaperForm from "../other/PaperForm.jsx";

import appValidator from "../AppValidator.js";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const LecturerTask = props => {
  return (
    <PaperForm className={paperStyle.standardPaper}>
      <Grid container alignItems="baseline">
        <TextField
          disabled
          label="Lecturer"
          className={textFieldStyle.textField}
          value={props.lecturerTask.lecturer.name}
          margin="normal"
        />

        <Button
          variant="contained"
          color="inherit"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          onClick={props.moveToNextStep(props.taskIndex)}
        >
          Find Lecturer
        </Button>
      </Grid>

      <TextField
        disabled
        label="Phone"
        className={textFieldStyle.longTextField}
        value={appValidator.replaceNull(props.lecturerTask.lecturer.phone, "")}
        margin="normal"
      />

      <br />

      <TextField
        disabled
        label="Email"
        className={textFieldStyle.longTextField}
        value={appValidator.replaceNull(props.lecturerTask.lecturer.email, "")}
        margin="normal"
      />

      <br />

      <TextField
        error={
          appValidator.sayIfNotCorrectLength(props.lecturerTask.description, {
            max: props.maxDescriptionLength
          })
            ? true
            : false
        }
        helperText={appValidator.sayIfNotCorrectLength(
          props.lecturerTask.description,
          {
            max: props.maxDescriptionLength
          }
        )}
        label="Description"
        multiline
        rowsMax="3"
        className={textFieldStyle.longTextField}
        value={props.lecturerTask.description}
        onChange={props.handleArrayChange(
          "lecturerArray",
          props.taskIndex,
          "description"
        )}
        margin="normal"
      />

      <Grid container justify="flex-end">
        <Fab
          color="default"
          aria-label="Delete"
          size="small"
          onClick={props.deleteTask(props.taskIndex)}
        >
          <DeleteIcon />
        </Fab>
      </Grid>
    </PaperForm>
  );
};

export default LecturerTask;
