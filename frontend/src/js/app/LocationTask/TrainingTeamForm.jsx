import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";
import EditIcon from "@material-ui/icons/Edit";
import Fab from "@material-ui/core/Fab";
import Divider from "@material-ui/core/Divider";
import { Link } from "react-router-dom";
import PaperForm from "../other/PaperForm.jsx";
import KeyValueColumns from "../other/KeyValueColumns.jsx";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";
import errorStyle from "../../styles/Error.module.css";
import appDate from "../AppDate.js";

class TrainingTeamForm extends React.Component {
  state = {
    step: 1,
    training: {},
    trainingCode: "CA001",
    mainTaskArray: [
      { type: "Location", employeeId: "", description: "" },
      { type: "Lecturer", employeeId: "", description: "" },
      { type: "Giveaway", employeeId: "", description: "" }
    ],
    employeeId: "",
    errorMessage: "",
    maxOtherTaskItemNo: 0,

    isLoading: true
  };

  componentDidMount() {
    this.getTraining();
  }

  getTraining = trainingCode => {
    axios
      .get("/api/training/" + this.props.match.params.trainingId)
      .then(response => {
        if (response.data) {
          this.setState({
            training: response.data,
            step: 2,
            errorMessage: ""
          });
        } else {
          this.setState({
            errorMessage: "Training not found"
          });
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({
          errorMessage: "Training not found"
        });
      })
      .then(() => this.setState({ isLoading: false }));
  };

  getLocationNameAndRoom = room => {
    var locationShortDescription = room.location.name;
    locationShortDescription += ", room " + room.roomNo;
    locationShortDescription += " (floor " + room.floorNo + ")";

    return locationShortDescription;
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  handleTaskChange = (array, input, taskIndex) => event => {
    var newMainTaskArray = this.state[array].slice();
    newMainTaskArray[taskIndex][input] = event.target.value;
    this.setState({
      [array]: newMainTaskArray
    });
  };

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    let formBody;
    if (this.state.step > 1) {
      if (this.state.errorMessage) {
        formBody = (
          <div>
            <div class={errorStyle.shortError}>{this.state.errorMessage}</div>
            <br />
            <br />
          </div>
        );
      } else {
        formBody = (
          <div>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="baseline"
            >
              <PaperForm
                className={paperStyle.widePaper}
                title="Training Description"
              >
                <KeyValueColumns
                  keyValueArray={[
                    [
                      <b>Training:</b>,
                      this.state.training.name +
                        " (" +
                        this.state.training.code +
                        ")"
                    ],
                    [<b>Category:</b>, this.state.training.category.name],
                    [
                      <b>From:</b>,
                      appDate.getDateTime(this.state.training.startDate)
                    ],
                    [
                      <b>To:</b>,
                      appDate.getDateTime(this.state.training.endDate)
                    ]
                  ]}
                />
              </PaperForm>

              <PaperForm className={paperStyle.widePaper} title="Training Team">
                <KeyValueColumns
                  keyValueArray={this.state.training.employees.map(
                    (employee, employeeIndex) => {
                      return [
                        employeeIndex + 1 + ". ",
                        employee.name + " (" + employee.employeeNo + ")"
                      ];
                    }
                  )}
                />
              </PaperForm>
            </Grid>

            <br />
            <br />
            <Divider />
            <br />
            <br />

            {this.state.training.locationTasks.map(
              (locationTaskValue, locationTaskIndex) => {
                return (
                  <div key={locationTaskIndex}>
                    <Grid
                      container
                      direction="row"
                      justify="flex-start"
                      alignItems="baseline"
                    >
                      <h2>
                        {this.getLocationNameAndRoom(locationTaskValue.room)}
                      </h2>
                      &nbsp;&nbsp;
                      {this.props.readOnly ? (
                        ""
                      ) : (
                        <Fab
                          color="default"
                          aria-label="Edit"
                          size="small"
                          component={Link}
                          to={"/locationTask/" + locationTaskValue.taskId}
                        >
                          <EditIcon />
                        </Fab>
                      )}
                    </Grid>
                    <Grid container key={locationTaskIndex}>
                      <PaperForm
                        className={paperStyle.widePaper}
                        title="Location"
                      >
                        <KeyValueColumns
                          keyValueArray={[
                            [
                              <b>Address:</b>,
                              locationTaskValue.room.location.address
                            ],
                            [
                              <b>From:</b>,
                              appDate.getDateTime(locationTaskValue.startDate)
                            ],
                            [
                              <b>To:</b>,
                              appDate.getDateTime(locationTaskValue.endDate)
                            ]
                          ]}
                        />
                      </PaperForm>
                      <Grid item>
                        <PaperForm
                          className={paperStyle.widePaper}
                          title="Lecturers"
                        >
                          <KeyValueColumns
                            keyValueArray={locationTaskValue.lecturerTasks.map(
                              (lecturerTaskValue, lecturerTaskIndex) => {
                                return [
                                  lecturerTaskIndex + 1 + ". ",
                                  lecturerTaskValue.lecturer.name
                                ];
                              }
                            )}
                          />
                        </PaperForm>
                      </Grid>
                      <Grid item>
                        <PaperForm
                          className={paperStyle.widePaper}
                          title="Giveaways"
                        >
                          <KeyValueColumns
                            keyValueArray={locationTaskValue.giveawayTasks.map(
                              (giveawayTaskValue, giveawayTaskIndex) => {
                                return [
                                  giveawayTaskIndex + 1 + ". ",
                                  giveawayTaskValue.product.name +
                                    " (" +
                                    giveawayTaskValue.amount +
                                    ")"
                                ];
                              }
                            )}
                          />
                        </PaperForm>
                      </Grid>
                    </Grid>
                    <br />
                    <br />
                  </div>
                );
              }
            )}
            {this.props.readOnly ? (
              ""
            ) : (
              <Button
                variant="contained"
                color="primary"
                className={textFieldStyle.textField}
                size="medium"
                component={Link}
                to={
                  "/add/locationTask/training/" + this.state.training.trainingId
                }
              >
                New Location
              </Button>
            )}
          </div>
        );
      }
    }

    return (
      <div>
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="baseline"
        >
          <PageHeader title={this.props.title} />
          &nbsp;&nbsp;
          {this.props.readOnly ? (
            ""
          ) : (
            <Fab
              color="default"
              aria-label="Edit"
              size="small"
              component={Link}
              to={
                "/myTrainingList/update/training/" +
                this.state.training.trainingId
              }
            >
              <EditIcon />
            </Fab>
          )}
        </Grid>
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          {formBody}
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(TrainingTeamForm);
