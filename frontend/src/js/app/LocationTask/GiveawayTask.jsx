import React from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import DeleteIcon from "@material-ui/icons/Delete";
import Fab from "@material-ui/core/Fab";
import PaperForm from "../other/PaperForm.jsx";

import appValidator from "../AppValidator.js";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const GiveawayTask = props => {
  return (
    <PaperForm className={paperStyle.standardPaper}>
      <Grid container alignItems="baseline">
        <TextField
          disabled
          label="Product"
          className={textFieldStyle.textField}
          value={props.giveawayTask.product.name}
          margin="normal"
        />

        <Button
          variant="contained"
          color="inherit"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          onClick={props.moveToNextStep(props.taskIndex)}
        >
          Find Product
        </Button>
      </Grid>

      <TextField
        required={true}
        error={
          appValidator.sayIfNotInteger(props.giveawayTask.amount, { min: 1 })
            ? true
            : false
        }
        helperText={appValidator.sayIfNotInteger(props.giveawayTask.amount, {
          min: 1
        })}
        label="Amount"
        className={textFieldStyle.textField}
        value={props.giveawayTask.amount}
        onChange={props.handleArrayChange(
          "giveawayArray",
          props.taskIndex,
          "amount"
        )}
        margin="normal"
        InputLabelProps={{
          shrink: true
        }}
      />

      <br />

      <TextField
        error={
          appValidator.sayIfNotCorrectLength(props.giveawayTask.description, {
            max: props.maxDescriptionLength
          })
            ? true
            : false
        }
        helperText={appValidator.sayIfNotCorrectLength(
          props.giveawayTask.description,
          {
            max: props.maxDescriptionLength
          }
        )}
        label="Description"
        multiline
        rowsMax="3"
        className={textFieldStyle.longTextField}
        value={props.giveawayTask.description}
        onChange={props.handleArrayChange(
          "giveawayArray",
          props.taskIndex,
          "description"
        )}
        margin="normal"
      />

      <Grid container justify="flex-end">
        <Fab
          color="default"
          aria-label="Delete"
          size="small"
          onClick={props.deleteTask(props.taskIndex)}
        >
          <DeleteIcon />
        </Fab>
      </Grid>
    </PaperForm>
  );
};

export default GiveawayTask;
