import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import PersonTable from "../person/PersonTable.jsx";
import ProductTable from "../product/ProductTable.jsx";
import LocationTable from "../location/LocationTable.jsx";
import LocationTask from "../locationTask/LocationTask.jsx";
import LecturerTask from "../locationTask/LecturerTask.jsx";
import GiveawayTask from "../locationTask/GiveawayTask.jsx";
import RoomTable from "../location/RoomTable.jsx";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import Dialog from "@material-ui/core/Dialog";
import axios from "axios";

import appValidator from "../AppValidator.js";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

const step = {
  MAIN: 0,
  LOCATION_TABLE: 1,
  PERSON_TABLE: 2,
  PRODUCT_TABLE: 3,
  ROOM_TABLE: 4
};

const maxDescriptionLength = 150;

class LocationTaskForm extends React.Component {
  state = {
    currentStep: step.MAIN,
    currentIndex: "",

    locationTask: {
      description: "",
      startDate: new Date(),
      endDate: new Date(),
      expectedNoOfAttendees: 0,
      actualNoOfAttendees: null,
      room: {
        roomNo: "",
        floorNo: "",
        capacity: "",
        location: {
          name: "",
          address: "",
          email: "",
          phone: "",
          city: { name: "", country: { name: "" } }
        }
      }
    },
    lecturerArray: [],
    giveawayArray: [],

    isLoading: true
  };

  componentDidMount() {
    if (this.props.match.params.taskId) {
      this.getLocationTask(this.props.match.params.taskId);
    } else if (this.props.match.params.trainingId) {
      this.getTraining(this.props.match.params.trainingId);
    }
  }

  getLocationTask = taskId => {
    axios
      .get("/api/locationTask/" + taskId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustLocationTaskToState)
      .catch(error => {
        console.log(error);
      });
  };

  adjustLocationTaskToState = locationTask => {
    if (locationTask) {
      var giveawayArray = JSON.parse(
        JSON.stringify(locationTask.giveawayTasks)
      );
      var lecturerArray = JSON.parse(
        JSON.stringify(locationTask.lecturerTasks)
      );
      delete locationTask.lecturerTasks;
      delete locationTask.giveawayTasks;
      this.setState({
        locationTask: locationTask,
        lecturerArray: lecturerArray,
        giveawayArray: giveawayArray,
        currentStep: step.MAIN,
        isLoading: false
      });
    } else {
      console.log("location task does not exist");
    }
  };

  getTraining = trainingId => {
    axios
      .get("/api/training/" + trainingId)
      .then(response => {
        if (response.data) {
          var locationTask = JSON.parse(
            JSON.stringify(this.state.locationTask)
          );
          locationTask.training = response.data;
          this.setState({
            locationTask: locationTask,
            isLoading: false
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  handleObjectChange = (objectName, input) => event => {
    var newObject = this.state[objectName];
    newObject[input] = event.target.value;
    this.setState({
      [objectName]: newObject
    });
  };

  handleArrayChange = (arrayName, index, input) => event => {
    var newArray = this.state[arrayName].slice();
    newArray[index][input] = event.target.value;
    this.setState({
      [arrayName]: newArray
    });
  };

  handleDateChange = (objectName, input) => date => {
    var newObject = this.state[objectName];
    newObject[input] = date;
    this.setState({
      [objectName]: newObject
    });
  };

  moveToMainStep = object => {
    if (this.state.currentStep === step.ROOM_TABLE) {
      this.addRoomAndMoveToMainStep(object);
    } else if (this.state.currentStep === step.PERSON_TABLE) {
      this.addLecturerAndMoveToMainStep(object);
    } else if (this.state.currentStep === step.PRODUCT_TABLE) {
      this.addGiveawayAndMoveToMainStep(object);
    }
  };

  addRoomAndMoveToMainStep = room => {
    var newLocationTask = this.state.locationTask;
    newLocationTask.room = room;
    this.setState({ locationTask: newLocationTask });
    this.moveToNextStep(step.MAIN)("")();
  };

  addLecturerAndMoveToMainStep = lecturer => {
    if (
      this.state.lecturerArray.find(lecturerTask => {
        return lecturerTask.lecturer.personId === lecturer.personId;
      })
    ) {
      this.props.addMessageDetails(
        "Lecturer " + lecturer.name + " was already chosen",
        true
      );
      this.moveToNextStep(step.MAIN)("")();
    } else {
      var newArray = this.state.lecturerArray.slice();
      if (appValidator.isInteger(this.state.currentIndex, { min: 0 })) {
        newArray[this.state.currentIndex].lecturer = lecturer;
      } else {
        var newLecturerTask = this.getEmptyLecturerTaskObject();
        newLecturerTask.lecturer = lecturer;
        newArray.push(newLecturerTask);
      }
      this.setState({ lecturerArray: newArray });
      this.moveToNextStep(step.MAIN)("")();
    }
  };

  addGiveawayAndMoveToMainStep = giveaway => {
    if (
      this.state.giveawayArray.find(giveawayTask => {
        return giveawayTask.product.productId === giveaway.productId;
      })
    ) {
      this.props.addMessageDetails(
        "Giveaway " + giveaway.name + " was already chosen",
        true
      );
      this.moveToNextStep(step.MAIN)("")();
    } else {
      var newArray = this.state.giveawayArray.slice();
      if (appValidator.isInteger(this.state.currentIndex, { min: 0 })) {
        newArray[this.state.currentIndex].product = giveaway;
      } else {
        var newGiveawayTask = this.getEmptyGiveawayTaskObject();
        newGiveawayTask.product = giveaway;
        newArray.push(newGiveawayTask);
      }
      this.setState({ giveawayArray: newArray });
      this.moveToNextStep(step.MAIN)("")();
    }
  };

  moveToNextStep = nextStep => index => event => {
    this.setState({
      currentIndex: index,
      currentStep: nextStep
    });
  };

  getEmptyLecturerTaskObject = () => {
    var newLecturerTask = { lecturer: { name: "" }, description: "" };
    return JSON.parse(JSON.stringify(newLecturerTask));
  };

  getEmptyGiveawayTaskObject = () => {
    var newGiveawayTask = { product: { name: "" }, amount: 0, description: "" };
    return JSON.parse(JSON.stringify(newGiveawayTask));
  };

  deleteTask = taskArrayName => index => event => {
    var newArray = this.state[taskArrayName].slice();
    newArray.splice(index, 1);

    this.setState({
      [taskArrayName]: newArray
    });
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.locationTask.room.location.locationId) ||
      appValidator.isEmpty(this.state.locationTask.room.roomId) ||
      appValidator.compareDate(
        this.state.locationTask.startDate,
        this.state.locationTask.endDate
      ) !== -1 ||
      !appValidator.isInteger(this.state.locationTask.expectedNoOfAttendees, {
        min: 1,
        max: this.state.locationTask.room.capacity
      }) ||
      !(
        appValidator.isEmpty(this.state.locationTask.actualNoOfAttendees) ||
        appValidator.isInteger(this.state.locationTask.actualNoOfAttendees, {
          min: 1
        })
      ) ||
      !appValidator.isCorrectLength(this.state.locationTask.description, {
        max: maxDescriptionLength
      });

    this.state.lecturerArray.forEach(lecturerTask => {
      isNotValid =
        isNotValid ||
        appValidator.isEmpty(lecturerTask.lecturer.personId) ||
        !appValidator.isCorrectLength(lecturerTask.description, {
          max: maxDescriptionLength
        });
    });

    this.state.giveawayArray.forEach(giveawayTask => {
      isNotValid =
        isNotValid ||
        appValidator.isEmpty(giveawayTask.product.productId) ||
        !appValidator.isInteger(giveawayTask.amount) ||
        !appValidator.isCorrectLength(giveawayTask.description, {
          max: maxDescriptionLength
        });
    });

    return isNotValid;
  };

  isLocationTaskInDatabase = () => {
    if (this.state.locationTask.taskId) {
      return true;
    } else {
      return false;
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.state.locationTask.taskId) {
      this.updateLocationTask();
    } else {
      this.createLocationTask();
    }
  };

  adjustLocationTaskToDatabase = () => {
    var locationTask = this.state.locationTask;
    locationTask.giveawayTasks = this.state.giveawayArray;
    locationTask.lecturerTasks = this.state.lecturerArray;

    return locationTask;
  };

  updateLocationTask = () => {
    var locationTask = this.adjustLocationTaskToDatabase();
    axios
      .put("/api/locationTask/" + locationTask.taskId, locationTask)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
        return response.data;
      })
      .then(this.adjustLocationTaskToState)
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  createLocationTask = () => {
    var locationTask = this.adjustLocationTaskToDatabase();
    axios
      .post("/api/locationTask", locationTask)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
        return response.data;
      })
      .then(this.adjustLocationTaskToState)
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  deleteLocationTask = () => event => {
    axios
      .delete("/api/locationTask/" + this.state.locationTask.taskId)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
        if (response.data) {
          this.props.history.push(
            "/myTrainingList/training/" +
              this.state.locationTask.training.trainingId
          );
        }
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    const chooseTable = currentStep => {
      switch (currentStep) {
        case step.PERSON_TABLE:
          return (
            <PersonTable
              actionOnChosenData={this.moveToMainStep}
              noButtons={true}
            />
          );
        case step.PRODUCT_TABLE:
          return (
            <ProductTable
              actionOnChosenData={this.moveToMainStep}
              noButtons={true}
            />
          );
        case step.LOCATION_TABLE:
          return (
            <LocationTable
              actionOnChosenData={this.moveToNextStep(step.ROOM_TABLE)}
              noButtons={true}
            />
          );
        case step.ROOM_TABLE:
          return (
            <RoomTable
              actionOnChosenData={this.moveToMainStep}
              locationId={this.state.currentIndex}
              noButtons={true}
            />
          );
        default:
          return <React.Fragment />;
      }
    };

    return (
      <div>
        <PageHeader title={this.props.title} />
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <LocationTask
            locationTask={this.state.locationTask}
            handleDateChange={this.handleDateChange}
            handleObjectChange={this.handleObjectChange}
            moveToNextStep={this.moveToNextStep(step.LOCATION_TABLE)}
            maxDescriptionLength={maxDescriptionLength}
          />

          <br />
          <Divider />
          <br />

          <h2>Lecturers</h2>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="baseline"
          >
            {this.state.lecturerArray.map((lecturerTask, index) => {
              return (
                <LecturerTask
                  key={index}
                  taskIndex={index}
                  lecturerTask={lecturerTask}
                  handleArrayChange={this.handleArrayChange}
                  deleteTask={this.deleteTask("lecturerArray")}
                  moveToNextStep={this.moveToNextStep(step.PERSON_TABLE)}
                  maxDescriptionLength={maxDescriptionLength}
                />
              );
            })}
          </Grid>

          <Button
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="button"
            onClick={this.moveToNextStep(step.PERSON_TABLE)("")}
          >
            New Lecturer
          </Button>

          <br />
          <br />
          <Divider />
          <br />

          <h2>Giveaways</h2>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="baseline"
          >
            {this.state.giveawayArray.map((giveawayTask, index) => {
              return (
                <GiveawayTask
                  key={index}
                  taskIndex={index}
                  giveawayTask={giveawayTask}
                  handleArrayChange={this.handleArrayChange}
                  deleteTask={this.deleteTask("giveawayArray")}
                  moveToNextStep={this.moveToNextStep(step.PRODUCT_TABLE)}
                  maxDescriptionLength={maxDescriptionLength}
                />
              );
            })}
          </Grid>

          <Button
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="button"
            onClick={this.moveToNextStep(step.PRODUCT_TABLE)("")}
          >
            New Giveaway
          </Button>

          <br />
          <br />
          <Divider />
          <br />

          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
          <Button
            disabled={!this.isLocationTaskInDatabase()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="button"
            onClick={this.deleteLocationTask()}
          >
            Delete
          </Button>
        </form>

        <Dialog
          open={this.state.currentStep !== step.MAIN}
          onClose={this.moveToNextStep(step.MAIN)("")}
          fullWidth={true}
          maxWidth="xl"
        >
          {chooseTable(this.state.currentStep)}
        </Dialog>
      </div>
    );
  }
}

export default withConfirmationSnackbar(LocationTaskForm);
