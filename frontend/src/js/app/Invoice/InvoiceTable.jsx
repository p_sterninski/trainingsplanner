import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class InvoiceTable extends React.Component {
  state = {
    initialData: [],
    chosenId: "",
    isCostInvoice: "",
    isCorrection: "",
    isLoading: true
  };

  componentDidMount() {
    this.getData();
  }

  getData() {
    axios.get("/api/invoice").then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "invoiceNo", label: "Invoice No" },
      { name: "invoiceDate", label: "Invoice Date" },
      { name: "salesDate", label: "Sales Date" },
      { name: "value", label: "Value" },
      { name: "isCostInvoice", label: "Is Cost Invoice" },
      { name: "isCorrection", label: "Is Correction" },
      { name: "isCancelled", label: "Is Cancelled" },
      { name: "isLast", label: "Is Last" },
      { name: "initialInvoiceNo", label: "Initial Invoice No" }
    ];

    const data = this.state.initialData
      .map(row => {
        return [
          row.invoiceId,
          row.invoiceNo,
          row.invoiceDate,
          row.salesDate,
          row.value,
          row.isCostInvoice ? "Yes" : "No",
          row.isCorrection ? "Yes" : "No",
          row.isCancelled ? "Yes" : "No",
          row.isLast ? "Yes" : "No",
          row.initialInvoice ? row.initialInvoice.invoiceNo : row.invoiceNo
        ];
      })
      .sort((invoice1, invoice2) => invoice2[0] - invoice1[0]);
    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.invoiceId === rowData[0];
        });
        this.setState({
          chosenId: chosenObject.invoiceId,
          isCostInvoice: chosenObject.isCostInvoice,
          isCorrection: chosenObject.isCorrection
        });
      }
    };

    if (this.state.chosenId) {
      if (this.state.isCostInvoice) {
        return <Redirect push to={"/costInvoice/" + this.state.chosenId} />;
      } else {
        return <Redirect push to={"/salesInvoice/" + this.state.chosenId} />;
      }
    }

    return (
      <div>
        <MUIDataTable
          title={"Invoice List"}
          data={data}
          columns={columns}
          options={options}
        />

        <br />

        <Button
          variant="contained"
          color="primary"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          component={Link}
          to={"/add/costInvoice"}
        >
          New Cost Invoice
        </Button>

        <Button
          variant="contained"
          color="primary"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          component={Link}
          to={"/add/salesInvoice"}
        >
          New Sales Invoice
        </Button>
      </div>
    );
  }
}

export default InvoiceTable;
