import "date-fns";
import React from "react";
import Button from "@material-ui/core/Button";
import PersonTable from "../person/PersonTable.jsx";
import TrainingTable from "../training/TrainingListTable.jsx";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import InvoiceItem from "./InvoiceItem.jsx";
import { Link } from "react-router-dom";
import PaperForm from "../other/PaperForm.jsx";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import PageHeader from "../other/PageHeader.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import ClientDetails from "./ClientDetails.jsx";
import InvoiceGeneralData from "./InvoiceGeneralData.jsx";
import Dialog from "@material-ui/core/Dialog";
import appValidator from "../AppValidator.js";
import axios from "axios";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const step = {
  MAIN: 0,
  PERSON_TABLE: 1,
  TRAINING_TABLE: 2
};

class InvoiceForm extends React.Component {
  state = {
    currentStep: step.MAIN,
    currentIndex: undefined,

    invoiceNo: "",
    salesDate: new Date(),
    invoiceDate: new Date(),
    isCostInvoice: "",
    isCorrection: false,
    isCancellation: false,
    isLast: true,
    items: [
      {
        value: "",
        amount: "",
        description: "",
        training: { code: "" }
      }
    ],

    person: {
      taxNo: "",
      nationalNo: "",
      name: "",
      address: "",
      city: { name: "", country: { name: "" } }
    },

    isLoading: true
  };

  componentDidMount() {
    if (this.props.match.params.invoiceId) {
      this.getInvoice(this.props.match.params.invoiceId);
    } else {
      this.setState({ isLoading: false });
    }
  }

  getInvoice = invoiceId => {
    axios
      .get("/api/invoice/" + invoiceId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustInvoiceFromDatabasetoState)
      .catch(error => {
        console.log(error);
      });
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  handleItemChange = (index, input) => event => {
    var newItems = this.state.items.slice();
    newItems[index][input] = event.target.value;
    this.setState({
      items: newItems
    });
  };

  handleDateChange = input => date => {
    this.setState({
      [input]: date
    });
  };

  adjustInvoiceFromDatabasetoState = invoice => {
    this.setState({
      invoiceId: this.props.isCorrection ? undefined : invoice.invoiceId,
      previousInvoiceId: this.props.isCorrection ? invoice.invoiceId : "",
      invoiceNo: this.props.isCorrection ? "" : invoice.invoiceNo,
      salesDate: invoice.salesDate,
      invoiceDate: invoice.invoiceDate,
      isCostInvoice: invoice.isCostInvoice,
      isCorrection: this.props.isCorrection ? true : invoice.isCorrection,
      isCancelled: this.props.isCancellation
        ? true
        : this.props.isCorrection
        ? false
        : invoice.isCancelled,
      isLast: this.props.isCorrection ? true : invoice.isLast,
      person: invoice.person,
      items: invoice.items.map(item => {
        if (this.props.isCorrection) {
          item.itemId = "";
          if (this.props.isCancellation) {
            item.value = -item.value;
            item.amount = -item.amount;
          }
        }
        return item;
      }),
      isLoading: false
    });
  };

  adjustInvoiceFromStateToDatabase = () => {
    var invoice = {
      invoiceId: this.state.invoiceId,
      invoiceNo: this.state.invoiceNo,
      invoiceDate: this.state.invoiceDate,
      salesDate: this.state.salesDate,
      person: this.state.person,
      items: this.state.items,
      isCostInvoice: this.props.isCostInvoice,
      isCorrection: this.state.isCorrection,
      isCancelled: this.state.isCancelled,
      isLast: this.state.isLast
    };

    return invoice;
  };

  handleSubmit = e => {
    e.preventDefault();
    var newInvoice = this.adjustInvoiceFromStateToDatabase();
    this.saveInvoice(newInvoice);
  };

  saveInvoice = invoice => {
    if (this.props.isCancellation) {
      this.saveCancellationInvoice(invoice);
    } else if (this.props.isCorrection) {
      this.saveCorrectiveInvoice(invoice);
    } else {
      this.saveNewInvoice(invoice);
    }
  };

  saveNewInvoice = invoice => {
    axios
      .post("/api/invoice", invoice)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  saveCorrectiveInvoice = invoice => {
    axios
      .post("/api/invoice/correction/" + this.state.previousInvoiceId, invoice)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  saveCancellationInvoice = invoice => {
    axios
      .post(
        "/api/invoice/cancellation/" +
          this.state.previousInvoiceId +
          "/" +
          invoice.invoiceNo
      )
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.person.personId) ||
      appValidator.isEmpty(this.state.salesDate) ||
      appValidator.isEmpty(this.state.invoiceDate) ||
      appValidator.isEmpty(this.state.invoiceNo) ||
      this.state.items.length <= 0;

    if (isNotValid) {
      return isNotValid;
    }
    this.state.items.forEach(item => {
      isNotValid =
        isNotValid ||
        appValidator.isEmpty(item.description) ||
        !appValidator.isFloat(item.value) ||
        !appValidator.isInteger(item.amount) ||
        appValidator.isEmpty(item.training.trainingId);
    });

    return isNotValid;
  };

  getEmptyItemObject = () => {
    return {
      value: "",
      amount: "",
      trainingId: "",
      description: "",
      training: { code: "" }
    };
  };

  addItem = () => {
    var newItems = this.state.items.slice();
    newItems.push(this.getEmptyItemObject());
    this.setState({ items: newItems });
  };

  deleteItem = index => event => {
    var newItems = this.state.items.slice();
    newItems.splice(index, 1);

    this.setState({
      items: newItems
    });
  };

  addClientAndMoveToMainStep = client => {
    this.setState({ person: client });
    this.moveToNextStep(step.MAIN)(undefined)();
  };

  addTrainingAndMoveToMainStep = newTraining => {
    var newArray = this.state.items.slice();
    newArray[this.state.currentIndex].training = newTraining;
    this.setState({ items: newArray });
    this.moveToNextStep(step.MAIN)(undefined)();
  };

  moveToNextStep = nextStep => index => event => {
    this.setState({
      currentIndex: index,
      currentStep: nextStep
    });
  };

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    let showAddItemButton = toShow => {
      if (toShow) {
        return (
          <Button
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="button"
            onClick={this.addItem}
          >
            Add Item
          </Button>
        );
      }
    };

    let showSaveButton = toShow => {
      if (toShow) {
        return (
          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
        );
      }
    };

    let showCorrectionAndCancellationButton = toShow => {
      if (toShow) {
        let linkSharedPart = this.props.isCostInvoice
          ? "costInvoice/"
          : "salesInvoice/";
        let linkCorrection = "/add/" + linkSharedPart + "correction/";
        let linkCancellation = "/cancel/" + linkSharedPart;

        return (
          <div>
            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={linkCorrection + this.state.invoiceId}
            >
              Correct Invoice
            </Button>

            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={linkCancellation + this.state.invoiceId}
            >
              Cancel Invoice
            </Button>
          </div>
        );
      }
    };

    const chooseTable = currentStep => {
      switch (currentStep) {
        case step.PERSON_TABLE:
          return (
            <PersonTable
              actionOnChosenData={this.addClientAndMoveToMainStep}
              noButtons={true}
              onlyLegalPerson={this.props.isCostInvoice ? true : false}
            />
          );
        case step.TRAINING_TABLE:
          return (
            <TrainingTable
              actionOnChosenData={this.addTrainingAndMoveToMainStep}
            />
          );
        default:
          return <React.Fragment />;
      }
    };

    return (
      <div>
        <PageHeader title={this.props.title} />
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="baseline"
          >
            <ClientDetails
              isFindButtonVisible={
                this.state.invoiceId || this.props.isCancellation ? false : true
              }
              onFindClick={this.moveToNextStep(step.PERSON_TABLE)}
              disabled={
                !this.state.invoiceId || this.props.isCancellation
                  ? true
                  : false
              }
              readOnly={this.state.invoiceId ? true : false}
              taxNo={this.state.person.taxNo}
              isNationalNoVisible={!this.props.isCostInvoice}
              nationalNo={this.state.person.nationalNo}
              name={this.state.person.name}
              address={
                this.state.person.address
                  ? this.state.person.address +
                    ", " +
                    this.state.person.city.name +
                    ", " +
                    this.state.person.city.country.name
                  : ""
              }
            />

            <InvoiceGeneralData
              disabled={
                !this.state.invoiceId || this.props.isCancellation
                  ? true
                  : false
              }
              invoiceNo={this.state.invoiceNo}
              handleInvoiceNoChange={this.handleChange("invoiceNo")}
              readOnly={this.state.invoiceId ? true : false}
              invoiceType={this.props.isCostInvoice ? "Cost" : "Sales"}
              isCorrection={this.state.isCorrection}
              salesDate={this.state.salesDate}
              handleSalesDateChange={this.handleDateChange("salesDate")}
              invoiceDate={this.state.invoiceDate}
              handleInvoiceDateChange={this.handleDateChange("invoiceDate")}
              isDatePickerUsed={
                this.state.invoiceId || this.props.isCancellation ? false : true
              }
            />
          </Grid>

          <br />
          <Divider />
          <br />

          <h2>Invoice Items</h2>
          {this.state.items.map((item, index) => (
            <div key={index}>
              <PaperForm className={paperStyle.screenWidePaper}>
                <InvoiceItem
                  index={index}
                  item={item}
                  handleItemChange={this.handleItemChange}
                  deleteItem={this.deleteItem}
                  moveToNextStep={this.moveToNextStep(step.TRAINING_TABLE)}
                  isCancellation={this.props.isCancellation}
                />
                <br />
              </PaperForm>
            </div>
          ))}

          {showAddItemButton(
            this.state.invoiceId || this.props.isCancellation ? false : true
          )}
          <br />
          <br />
          {showSaveButton(this.state.invoiceId ? false : true)}
          {showCorrectionAndCancellationButton(
            !this.props.isCorrection &&
              this.state.invoiceId &&
              this.state.isLast &&
              !this.state.isCancelled
              ? true
              : false
          )}
        </form>

        <Dialog
          open={this.state.currentStep !== step.MAIN}
          onClose={this.moveToNextStep(step.MAIN)(undefined)}
          fullWidth={true}
          maxWidth="xl"
        >
          {chooseTable(this.state.currentStep)}
        </Dialog>
      </div>
    );
  }
}

export default withConfirmationSnackbar(InvoiceForm);
