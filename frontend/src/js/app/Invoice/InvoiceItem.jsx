import React from "react";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import appValidator from "../AppValidator.js";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

const textFieldReadOnly = true;
const textFieldReadOnlyProps = {
  readOnly: textFieldReadOnly
};

const InvoiceItem = props => {
  let showFindProductButton = toShow => {
    if (toShow) {
      return (
        <Button
          variant="contained"
          color="inherit"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          onClick={props.moveToNextStep(props.index)}
        >
          Find Training
        </Button>
      );
    }
  };

  let showDeleteButton = toShow => {
    if (toShow) {
      return (
        <Button
          variant="contained"
          color="secondary"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          onClick={props.deleteItem(props.index)}
        >
          DELETE ITEM
        </Button>
      );
    }
  };

  return (
    <Grid
      container
      container
      direction="row"
      justify="flex-start"
      alignItems="baseline"
    >
      <TextField
        disabled={props.isCancellation}
        label="Description"
        className={textFieldStyle.longTextField}
        value={props.item.description}
        onChange={props.handleItemChange(props.index, "description")}
        margin="normal"
        InputProps={props.item.itemId ? textFieldReadOnlyProps : {}}
      />
      <TextField
        disabled={props.isCancellation}
        error={appValidator.sayIfNotFloat(props.item.value) ? true : false}
        helperText={appValidator.sayIfNotFloat(props.item.value)}
        label="Value"
        className={textFieldStyle.textField}
        value={props.item.value}
        onChange={props.handleItemChange(props.index, "value")}
        margin="normal"
        InputProps={{
          readOnly: props.item.itemId ? textFieldReadOnly : false,
          startAdornment: <InputAdornment position="start">zł</InputAdornment>
        }}
      />
      <TextField
        disabled={props.isCancellation}
        error={appValidator.sayIfNotInteger(props.item.amount) ? true : false}
        helperText={appValidator.sayIfNotInteger(props.item.amount)}
        label="Amount"
        className={textFieldStyle.textField}
        value={props.item.amount}
        onChange={props.handleItemChange(props.index, "amount")}
        //type="number"
        margin="normal"
        InputLabelProps={{
          shrink: true
        }}
        InputProps={props.item.itemId ? textFieldReadOnlyProps : {}}
      />
      <Grid item>
        <Grid container alignItems="baseline">
          <TextField
            disabled={!props.item.itemId || props.isCancellation ? true : false}
            label="Training Code"
            className={textFieldStyle.textField}
            value={props.item.training.code}
            margin="normal"
            InputProps={props.item.itemId ? textFieldReadOnlyProps : {}}
          />

          {showFindProductButton(
            props.item.itemId || props.isCancellation ? false : true
          )}
        </Grid>
      </Grid>

      {showDeleteButton(
        props.item.itemId || props.isCancellation ? false : true
      )}
    </Grid>
  );
};

export default InvoiceItem;
