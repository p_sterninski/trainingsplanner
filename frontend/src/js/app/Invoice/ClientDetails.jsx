import "date-fns";
import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import PaperForm from "../other/PaperForm.jsx";
import appValidator from "../AppValidator.js";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const textFieldReadOnlyProps = {
  readOnly: true
};

const ClientDetails = ({
  isFindButtonVisible = false,
  onFindClick,
  disabled = false,
  readOnly = false,
  taxNo,
  isNationalNoVisible = true,
  nationalNo,
  name,
  address
}) => {
  let showFindButton = toShow => {
    if (toShow) {
      return (
        <Button
          variant="contained"
          color="inherit"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          onClick={onFindClick(undefined)}
        >
          Find Client
        </Button>
      );
    }
  };

  let showNationalNo = toShow => {
    if (toShow) {
      return (
        <TextField
          disabled={disabled}
          label="National number"
          className={textFieldStyle.textField}
          value={appValidator.replaceNull(nationalNo, "")}
          margin="normal"
          InputProps={readOnly ? textFieldReadOnlyProps : {}}
        />
      );
    }
  };

  return (
    <PaperForm className={paperStyle.standardPaper}>
      <Grid container alignItems="baseline" justify="space-between">
        <h3 className={paperStyle.paperHeader}>Client Description</h3>

        {showFindButton(isFindButtonVisible)}
      </Grid>

      <Divider />

      <TextField
        disabled={disabled}
        label="Tax number"
        className={textFieldStyle.textField}
        value={appValidator.replaceNull(taxNo, "")}
        margin="normal"
        InputProps={readOnly ? textFieldReadOnlyProps : {}}
      />

      {showNationalNo(isNationalNoVisible)}

      <br />

      <TextField
        disabled={disabled}
        label="Name"
        className={textFieldStyle.longTextField}
        value={name}
        margin="normal"
        InputProps={readOnly ? textFieldReadOnlyProps : {}}
      />

      <br />

      <TextField
        disabled={disabled}
        label="Address"
        className={textFieldStyle.longTextField}
        value={address}
        margin="normal"
        InputProps={readOnly ? textFieldReadOnlyProps : {}}
      />
    </PaperForm>
  );
};

export default ClientDetails;
