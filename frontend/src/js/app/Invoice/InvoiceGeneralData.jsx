import "date-fns";
import React from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import PaperForm from "../other/PaperForm.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const textFieldReadOnlyProps = {
  readOnly: true
};

const InvoiceGeneralData = ({
  invoiceNo,
  handleInvoiceNoChange,
  readOnly,
  invoiceType,
  disabled = false,
  isDatePickerUsed = true,
  isCorrection = false,
  salesDate,
  handleSalesDateChange,
  invoiceDate,
  handleInvoiceDateChange
}) => {
  let showDatesAsDatePicker = asDatePicker => {
    if (asDatePicker) {
      return (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <Grid container>
            <DatePicker
              required={true}
              margin="normal"
              label="Sales date"
              className={textFieldStyle.textField}
              value={salesDate}
              onChange={handleSalesDateChange}
            />

            <DatePicker
              required={true}
              margin="normal"
              label="Invoice date"
              className={textFieldStyle.textField}
              value={invoiceDate}
              onChange={handleInvoiceDateChange}
            />
          </Grid>
        </MuiPickersUtilsProvider>
      );
    } else {
      return (
        <div>
          <TextField
            required={true}
            disabled={disabled}
            label="Sales date"
            className={textFieldStyle.textField}
            value={salesDate.toString()}
            margin="normal"
            InputProps={readOnly ? textFieldReadOnlyProps : {}}
          />

          <TextField
            required={true}
            disabled={disabled}
            label="Invoice date"
            className={textFieldStyle.textField}
            value={invoiceDate.toString()}
            margin="normal"
            InputProps={readOnly ? textFieldReadOnlyProps : {}}
          />
        </div>
      );
    }
  };

  return (
    <PaperForm
      className={paperStyle.standardPaper}
      title="Invoice General Data"
    >
      <TextField
        required={true}
        label="Invoice no."
        className={textFieldStyle.textField}
        value={invoiceNo}
        onChange={handleInvoiceNoChange}
        margin="normal"
        InputProps={readOnly ? textFieldReadOnlyProps : {}}
      />

      <br />

      <Grid container alignItems="flex-end">
        <TextField
          disabled={disabled}
          InputProps={readOnly ? textFieldReadOnlyProps : {}}
          label="Invoice type"
          className={textFieldStyle.textField}
          value={invoiceType}
          margin="normal"
        />

        <FormControlLabel
          disabled={disabled}
          control={
            <Checkbox
              checked={isCorrection}
              value="isActiveisCorrection"
              color="primary"
            />
          }
          label="Correction"
          className={textFieldStyle.textField}
        />
      </Grid>

      {showDatesAsDatePicker(isDatePickerUsed)}
    </PaperForm>
  );
};

export default InvoiceGeneralData;
