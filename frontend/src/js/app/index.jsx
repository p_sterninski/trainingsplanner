import React from "react";
import { render } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import AppMenuBar from "./appMenuBar/AppMenuBar.jsx";
import Routes from "./Routes.jsx";
import axios from "axios";
import LoadingPageIndicator from "./other/LoadingPageIndicator.jsx";
import AuthenticationProvider from "./security/AuthenticationProvider.js";

class App extends React.Component {
  state = {
    isLoading: true
  };

  componentDidMount() {
    this.checkUserDetailsInBackend();
  }

  checkUserDetailsInBackend() {
    axios
      .get("/api/auth/userDetails")
      .then(response => {
        if (response.data) {
          AuthenticationProvider.prepareAndSaveUser(response.data);
        } else {
          AuthenticationProvider.deleteUserCredentials();
        }
      })
      .then(() => this.setState({ isLoading: false }));
  }

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    } else {
      return (
        <BrowserRouter>
          <div>
            <AppMenuBar />
            <Routes />
          </div>
        </BrowserRouter>
      );
    }
  }
}

render(<App />, document.getElementById("app"));
