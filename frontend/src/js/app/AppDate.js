const emptyDateTimeString = "-";

class AppDate {
  static getDate = input => {
    var options = {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    };
    return input
      ? new Date(input).toLocaleDateString("default", options)
      : emptyDateTimeString;
  };

  static getTime = input => {
    var options = {
      hour12: true,
      hour: "numeric",
      minute: "2-digit"
    };
    return input
      ? new Date(input).toLocaleTimeString("default", options)
      : emptyDateTimeString;
  };

  static getDateTime = input => {
    return input
      ? AppDate.getDate(input) + ", " + AppDate.getTime(input)
      : emptyDateTimeString;
  };
}

export default AppDate;
