import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class LocationTable extends React.Component {
  state = {
    initialData: [],
    chosenLocationId: "",
    isLoading: true
  };

  componentDidMount() {
    this.getData();
  }

  getData() {
    axios.get("/api/location").then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "name", label: "Name" },
      { name: "address", label: "Address" },
      { name: "city", label: "City" },
      { name: "country", label: "Country" },
      { name: "email", label: "Email" },
      { name: "phone", label: "Phone" }
    ];

    const data = this.state.initialData.map(row => {
      return [
        row.locationId,
        row.name,
        row.address,
        row.city.name,
        row.city.country.name,
        row.email,
        row.phone
      ];
    });

    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.locationId === rowData[0];
        });

        if (this.props.actionOnChosenData) {
          this.props.actionOnChosenData(chosenObject.locationId)(null);
        } else {
          this.setState({ chosenLocationId: chosenObject.locationId });
        }
      }
    };

    let showCreateNewLocationButton = toShow => {
      if (toShow) {
        return (
          <React.Fragment>
            <br />

            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={"/add/location"}
            >
              New Location
            </Button>
          </React.Fragment>
        );
      }
    };

    if (this.state.chosenLocationId) {
      return <Redirect push to={"/location/" + this.state.chosenLocationId} />;
    }

    return (
      <div>
        <MUIDataTable
          title={"Location List"}
          data={data}
          columns={columns}
          options={options}
        />

        {showCreateNewLocationButton(!this.props.noButtons)}
      </div>
    );
  }
}

export default LocationTable;
