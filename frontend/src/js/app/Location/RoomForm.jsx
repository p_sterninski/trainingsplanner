import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import PaperForm from "../other/PaperForm.jsx";
import axios from "axios";
import appValidator from "../AppValidator.js";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

class LocationForm extends React.Component {
  state = {
    roomNo: "",
    capacity: "",
    floorNo: "",
    location: {},

    errorMessage: "",
    isLoading: true
  };

  componentDidMount() {
    if (this.props.match.params.roomId) {
      this.getRoom(this.props.match.params.roomId);
    } else if (this.props.match.params.locationId) {
      this.getLocation(this.props.match.params.locationId); //if this.props.match.params.roomId is provided then location will be set in adjustRoomFromDatabaseToState method
    }
  }

  getRoom = roomId => {
    axios
      .get("/api/room/" + roomId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustRoomFromDatabaseToState)
      .catch(error => {
        console.log(error);
      });
  };

  getLocation = locationId => {
    axios
      .get("/api/location/" + locationId)
      .then(response => {
        this.setState({ location: response.data, isLoading: false });
      })
      .catch(error => {
        console.log(error);
      });
  };

  adjustRoomFromDatabaseToState = room => {
    if (room) {
      this.setState({
        roomId: room.roomId,
        roomNo: room.roomNo,
        capacity: room.capacity,
        floorNo: room.floorNo,
        location: room.location,
        isLoading: false
      });
    } else {
      console.log("Location does not exist");
    }
  };

  adjustRoomFromStateToDatabase = () => {
    var newRoom = {
      roomId: this.state.roomId,
      roomNo: this.state.roomNo,
      capacity: this.state.capacity,
      floorNo: this.state.floorNo,
      location: this.state.location
    };

    return newRoom;
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.roomNo) ||
      !appValidator.isInteger(this.state.capacity, { min: 1 }) ||
      !appValidator.isInteger(this.state.floorNo);

    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    var room = this.adjustRoomFromStateToDatabase();
    if (room.roomId) {
      this.updateRoom(room);
    } else {
      this.saveNewRoom(room);
    }
  };

  saveNewRoom = room => {
    axios
      .post("/api/room", room)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  updateRoom = room => {
    axios
      .put("/api/room/" + room.roomId, room)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <PageHeader title={this.props.title} />

        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
            <PaperForm className={paperStyle.standardPaper}>
              <TextField
                required={true}
                error={
                  appValidator.sayIfEmpty(this.state.roomNo) ? true : false
                }
                helperText={appValidator.sayIfEmpty(this.state.roomNo)}
                label="Room No"
                className={textFieldStyle.textField}
                value={this.state.roomNo}
                onChange={this.handleChange("roomNo")}
                margin="normal"
              />

              <TextField
                required={true}
                error={
                  appValidator.sayIfNotInteger(this.state.capacity, { min: 1 })
                    ? true
                    : false
                }
                helperText={appValidator.sayIfNotInteger(this.state.capacity, {
                  min: 1
                })}
                label="Room Capacity"
                className={textFieldStyle.textField}
                value={this.state.capacity}
                onChange={this.handleChange("capacity")}
                margin="normal"
              />

              <TextField
                required={true}
                error={
                  appValidator.sayIfNotInteger(this.state.floorNo)
                    ? true
                    : false
                }
                helperText={appValidator.sayIfNotInteger(this.state.floorNo)}
                label="Floor No"
                className={textFieldStyle.textField}
                value={this.state.floorNo}
                onChange={this.handleChange("floorNo")}
                margin="normal"
              />
            </PaperForm>
          </Grid>
          <br />
          <br />

          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(LocationForm);
