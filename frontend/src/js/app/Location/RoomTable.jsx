import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class LocationTable extends React.Component {
  state = {
    initialData: [],
    chosenRoomId: "",
    isLoading: true
  };

  componentDidMount() {
    if (
      this.props.hasOwnProperty("match") &&
      this.props.match.hasOwnProperty("params") &&
      this.props.match.params.locationId
    ) {
      this.getData(this.props.match.params.locationId);
    } else if (this.props.locationId) {
      this.getData(this.props.locationId);
    } else {
      this.getData();
    }
  }

  getData() {
    var link = "/api/room";
    if (arguments.length === 1) {
      link += "/location/" + arguments[0];
    }

    axios.get(link).then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "location Name", label: "Location Name" },
      { name: "room", label: "Room" },
      { name: "floor", label: "Floor" },
      { name: "capacity", label: "Capacity" }
    ];

    const data = this.state.initialData.map(row => {
      return [
        row.roomId,
        row.location.name,
        row.roomNo,
        row.floorNo,
        row.capacity
      ];
    });

    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.roomId === rowData[0];
        });

        if (this.props.actionOnChosenData) {
          this.props.actionOnChosenData(chosenObject);
        } else {
          this.setState({ chosenRoomId: chosenObject.roomId });
        }
      }
    };

    let showCreateNewRoomButton = toShow => {
      if (toShow) {
        return (
          <React.Fragment>
            <br />

            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={
                "/location/" + this.props.match.params.locationId + "/add/room"
              }
            >
              New Room
            </Button>
          </React.Fragment>
        );
      }
    };

    if (this.state.chosenRoomId) {
      return (
        <Redirect
          push
          to={
            "/location/" +
            this.props.match.params.locationId +
            "/room/" +
            this.state.chosenRoomId
          }
        />
      );
    }

    return (
      <div>
        <MUIDataTable
          title={"Room List"}
          data={data}
          columns={columns}
          options={options}
        />

        {showCreateNewRoomButton(!this.props.noButtons)}
      </div>
    );
  }
}

export default LocationTable;
