import React from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";
import appValidator from "../AppValidator.js";
import { Link } from "react-router-dom";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import Grid from "@material-ui/core/Grid";
import ContactDetails from "../person/ContactDetails.jsx";
import AddressDetails from "../person/AddressDetails.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import gridStyle from "../../styles/GridStyle.module.css";

class LocationForm extends React.Component {
  state = {
    name: "",
    address: "",
    phone: "",
    email: "",

    countryId: "",
    cityName: "",
    countryArray: [],
    cityArray: [],

    fetchCounter: 0
  };

  componentDidMount() {
    this.getCountries();
    if (this.props.match.params.locationId) {
      this.getLocation(this.props.match.params.locationId);
    } else {
      this.getCities(); //if location exists cities are retrived in getLocation() based on existing countryId
    }
  }

  getCountries() {
    axios.get("/api/country").then(response => {
      if (response.data) {
        this.setState({
          countryArray: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  getCities() {
    if (arguments.length === 0) {
      axios.get("/api/city").then(response => {
        if (response.data) {
          this.setState({
            cityArray: response.data,
            fetchCounter: this.state.fetchCounter + 1
          });
        }
      });
    } else if (arguments.length === 1) {
      axios.get("/api/country/cities/" + arguments[0]).then(response => {
        if (response.data) {
          this.setState({ cityArray: response.data });
        }
      });
    }
  }

  getLocation = locationId => {
    axios
      .get("/api/location/" + locationId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustLocationFromDatabaseToState)
      .catch(error => {
        console.log(error);
      });
  };

  adjustLocationFromDatabaseToState = location => {
    if (location) {
      this.setState({
        locationId: location.locationId,
        name: location.name,
        address: location.address,
        phone: location.phone,
        email: location.email,
        countryId: location.city.country.countryId,
        cityName: location.city.name,
        fetchCounter: this.state.fetchCounter + 1
      });
      this.getCities(location.city.country.countryId);
    } else {
      console.log("Location does not exist");
    }
  };

  adjustLocationFromStateToDatabase = () => {
    var country = { countryId: this.state.countryId };
    var newCity = { name: this.state.cityName, country: country };

    var newLocation = {
      locationId: this.state.locationId,
      name: this.state.name,
      address: this.state.address,
      phone: this.state.phone,
      email: this.state.email,
      city: newCity
    };

    return newLocation;
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
    if (input === "countryId") {
      this.getCities(event.target.value);
    }
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.name) ||
      appValidator.isEmpty(this.state.address) ||
      appValidator.isEmpty(this.state.countryId) ||
      appValidator.isEmpty(this.state.cityName) ||
      !appValidator.isEmail(this.state.email);

    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    var location = this.adjustLocationFromStateToDatabase();
    if (location.loationId) {
      this.updateLocation(location);
    } else {
      this.saveNewLocation(location);
    }
  };

  saveNewLocation = location => {
    axios
      .post("/api/location", location)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  updateLocation = location => {
    axios
      .put("/api/location/" + location.locationId, location)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.fetchCounter < 2) {
      return <LoadingPageIndicator />;
    }

    let showRoomsButton = locationId => {
      if (locationId) {
        return (
          <Button
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="button"
            component={Link}
            to={"/location/" + locationId + "/room"}
          >
            Rooms
          </Button>
        );
      }
    };

    return (
      <div>
        <PageHeader title={this.props.title} />
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
            className={gridStyle.gridWithElementsOnRow}
          >
            <Grid item>
              <ContactDetails
                hideNaturalNoAndTaxNo={true}
                name={this.state.name}
                onNameChange={this.handleChange("name")}
                phone={this.state.phone}
                onPhoneChange={this.handleChange("phone")}
                email={this.state.email}
                onEmailChange={this.handleChange("email")}
              />

              <br />
              <br />
            </Grid>
            <Grid item>
              <AddressDetails
                address={this.state.address}
                onAddressChange={this.handleChange("address")}
                countryId={this.state.countryId}
                onCountryIdChange={this.handleChange("countryId")}
                countryArray={this.state.countryArray}
                cityName={this.state.cityName}
                onCityNameChange={this.handleChange("cityName")}
                cityArray={this.state.cityArray}
              />
              <br />
              <br />
            </Grid>
          </Grid>

          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>

          {showRoomsButton(this.state.locationId)}
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(LocationForm);
