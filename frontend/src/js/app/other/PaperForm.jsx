import React from "react";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";

import paperStyle from "../../styles/PaperStyle.module.css";

const PaperForm = ({
  elevation = 2,
  className,
  title,
  otherPaperProps,
  ...props
}) => {
  let header = undefined;
  if (title) {
    header = (
      <React.Fragment>
        <h3 className={paperStyle.paperHeader}>{title}</h3>

        <Divider />
      </React.Fragment>
    );
  }

  return (
    <Paper
      elevation={elevation}
      className={className}
      {...props.otherPaperProps}
    >
      {header}
      {props.children}
    </Paper>
  );
};

export default PaperForm;
