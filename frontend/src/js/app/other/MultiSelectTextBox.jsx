import React from "react";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Chip from "@material-ui/core/Chip";
import Input from "@material-ui/core/Input";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  chips: {
    display: "flex",
    flexWrap: "wrap"
  },
  chip: {
    margin: theme.spacing.unit / 4
  }
});

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

const MultiSelectTextBox = ({
  onChange,
  itemChosenArray,
  itemArray,
  itemKey,
  itemLabel,
  onItemDelete,
  className,
  label = "",
  required = false,
  classes //delivered by withStyles()
}) => {
  const handleItemDelete = chosenItem => event => {
    var newChosenItemArray = itemChosenArray.slice();
    var indexOfItemToDelete = newChosenItemArray.indexOf(chosenItem);
    newChosenItemArray.splice(indexOfItemToDelete, 1);
    return onItemDelete(chosenItem, newChosenItemArray, event);
  };

  return (
    <FormControl className={className} required={required}>
      <InputLabel htmlFor="select-multiple-chip">{label}</InputLabel>
      <Select
        multiple
        value={itemChosenArray}
        onChange={onChange}
        input={<Input id="select-multiple-chip" />}
        renderValue={selected => (
          <div className={classes.chips}>
            {selected.map(item => (
              <Chip
                key={item[itemKey]}
                label={item[itemLabel]}
                className={classes.chip}
                onDelete={handleItemDelete(item)}
              />
            ))}
          </div>
        )}
        MenuProps={MenuProps}
      >
        {itemArray.map(item => (
          <MenuItem key={item[itemKey]} value={item}>
            {item[itemLabel]}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

MultiSelectTextBox.propTypes = {
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  itemChosenArray: PropTypes.array.isRequired,
  itemArray: PropTypes.array.isRequired,
  itemKey: PropTypes.string.isRequired,
  itemLabel: PropTypes.string.isRequired,
  onItemDelete: PropTypes.func.isRequired,
  className: PropTypes.string,
  label: PropTypes.string,
  required: PropTypes.bool
};

export default withStyles(styles)(MultiSelectTextBox);
