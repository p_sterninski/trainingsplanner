import React from "react";
import { withStyles } from "@material-ui/core/styles";

const styles = {
  keyValueContainer: {
    display: "grid",
    gridTemplateColumns: "auto 1fr",
    gridColumnGap: "15px",
    gridRowGap: "10px",
    justifyContent: "start",
    alignContent: "start",
    justifyItems: "start",
    alignItems: "center",
    margin: "15px 10px"
  }
};

const KeyValueColumns = props => {
  const { classes } = props;

  return (
    <div className={classes.keyValueContainer}>
      {props.KeyValueArray}
      {props.keyValueArray.map((pair, pairIndex) => {
        return [
          <div key={2 * pairIndex}>{pair[0]}</div>,
          <div key={2 * pairIndex + 1}>{pair[1]}</div>
        ];
      })}
    </div>
  );
};

export default withStyles(styles)(KeyValueColumns);
