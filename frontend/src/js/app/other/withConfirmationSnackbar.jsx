import React from "react";
import ConfirmationSnackbar from "./ConfirmationSnackbar.jsx";

const withConfirmationSnackbar = WrappedComponent => {
  class WithConfirmationSnackbar extends React.Component {
    state = { messageDetailsArray: [] };

    addMessageDetails = (message, isError = false) => {
      var messageDetailsArrayTmp = this.state.messageDetailsArray.slice();
      messageDetailsArrayTmp.push({
        message: message,
        key: new Date().getTime(),
        isError: isError
      });
      this.setState({ messageDetailsArray: messageDetailsArrayTmp });
    };

    addStandardSaveMessageDetails = (isError = false) => {
      var message;
      if (isError) {
        message = "Error occured - data has not been saved";
      } else {
        message = "Data has been saved successfully";
      }
      this.addMessageDetails(message, isError);
    };

    addErrorMessageDetailsFromPromise = error => {
      if (error.response) {
        this.addMessageDetails(error.response.data.message, true);
        var errorDescription = error.response.data;
        errorDescription.sentText = error.config.data;
        errorDescription.url = error.config.url;
        errorDescription.method = error.config.method;
        console.log(errorDescription);
      } else if (error.request) {
        this.addStandardSaveMessageDetails(true);
        console.log(error.request);
      } else {
        this.addStandardSaveMessageDetails(true);
        console.log(error.message);
      }
    };

    getAndRemoveMessageDetails = () => {
      var messageDetails = undefined;
      if (this.state.messageDetailsArray.length > 0) {
        var messageDetailsArrayTmp = this.state.messageDetailsArray.slice();
        messageDetails = messageDetailsArrayTmp.shift();
        this.setState({ messageDetailsArray: messageDetailsArrayTmp });
      }

      return messageDetails;
    };

    render() {
      return (
        <div>
          <WrappedComponent
            addMessageDetails={this.addMessageDetails}
            addStandardSaveMessageDetails={this.addStandardSaveMessageDetails}
            addErrorMessageDetailsFromPromise={
              this.addErrorMessageDetailsFromPromise
            }
            {...this.props}
          />
          <ConfirmationSnackbar
            getAndRemoveMessageDetails={this.getAndRemoveMessageDetails}
          />
        </div>
      );
    }
  }

  return WithConfirmationSnackbar;
};

export default withConfirmationSnackbar;
