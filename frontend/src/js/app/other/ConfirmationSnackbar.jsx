import React from "react";
import Snackbar from "@material-ui/core/Snackbar";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  success: {
    backgroundColor: theme.palette.primary.dark
  },
  error: {
    backgroundColor: theme.palette.error.dark
  }
});

class ConfirmationSnackbar extends React.Component {
  state = {
    open: false,
    messageDetails: {}
  };

  componentWillUpdate() {
    if (this.state.open) {
      // immediately begin dismissing current message
      // to start showing new one
      this.setState({ open: false });
    } else {
      this.processQueue();
    }
  }

  processQueue = () => {
    var messageDetails = this.props.getAndRemoveMessageDetails();
    if (messageDetails) {
      this.setState({
        messageDetails: messageDetails,
        open: true
      });
    }
  };

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    this.setState({ open: false });
  };

  handleExited = () => {
    this.processQueue();
  };

  render() {
    const { messageDetails } = this.state;
    const { classes } = this.props;
    const backgroundType = messageDetails.isError ? "error" : "success";

    return (
      <div>
        <Snackbar
          key={messageDetails.key}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          open={this.state.open}
          autoHideDuration={4000}
          onClose={this.handleClose}
          onExited={this.handleExited}
          ContentProps={{
            "aria-describedby": "message-id"
          }}
          message={<span id="message-id">{messageDetails.message}</span>}
          ContentProps={{ className: classes[backgroundType] }}
        />
      </div>
    );
  }
}

export default withStyles(styles)(ConfirmationSnackbar);
