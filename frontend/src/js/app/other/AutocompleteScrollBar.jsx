import React from "react";
import TextField from "@material-ui/core/TextField";
import Autosuggest from "react-autosuggest";
import appValidator from "../AppValidator.js";

import "../../styles/autocompleteScrollBarSetting.css";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function renderInputComponent(inputProps) {
  const { inputRef = () => {}, ref, ...other } = inputProps;
  return (
    <TextField
      InputProps={{
        inputRef: node => {
          ref(node);
          inputRef(node);
        }
      }}
      {...other}
    />
  );
}

function getSuggestionValue(suggestion) {
  return String(suggestion.name);
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  return <span>{suggestion.name}</span>;
}

class AutoCompleteScrollBar extends React.Component {
  constructor() {
    super();

    this.state = {
      suggestions: []
    };
  }

  getSuggestions(value) {
    const escapedValue = escapeRegexCharacters(value.trim());
    if (escapedValue === "") {
      return [];
    }

    const regex = new RegExp("^" + escapedValue, "i");
    return this.props.array.filter(object => regex.test(object.name));
  }

  onChange = (event, { newValue, method }) => {
    this.props.handleChange({ target: { value: newValue } });
  };

  onSuggestionsFetchRequested = ({ value }) => {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
  };

  render() {
    const { suggestions } = this.state;

    const inputProps = {
      required: true,
      error: appValidator.sayIfEmpty(this.props.value) ? true : false,
      helperText: appValidator.sayIfEmpty(this.props.value),
      label: this.props.label,
      value: this.props.value,
      onChange: this.onChange,
      className: textFieldStyle.longTextField
    };

    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderInputComponent={renderInputComponent}
        renderSuggestion={renderSuggestion}
        inputProps={inputProps}
      />
    );
  }
}

export default AutoCompleteScrollBar;
