import React from "react";

const PageHeader = ({ title }) => {
  return <h2>{title}</h2>;
};

export default PageHeader;
