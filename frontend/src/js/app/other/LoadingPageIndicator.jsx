import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";

const styles = {
  centerOfScreen: {
    position: "fixed",
    top: "-100%",
    right: "-100%",
    left: "-100%",
    bottom: "-100%",
    margin: "auto auto"
  }
};

const LoadingPageIndicator = ({
  classes,
  className = classes.centerOfScreen,
  size = 80,
  thickness = 3.6,
  ...otherProps
}) => {
  return (
    <CircularProgress
      className={className}
      size={size}
      thickness={thickness}
      {...otherProps}
    />
  );
};

LoadingPageIndicator.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  size: PropTypes.number,
  thickness: PropTypes.number
};

export default withStyles(styles)(LoadingPageIndicator);
