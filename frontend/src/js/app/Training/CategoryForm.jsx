import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import axios from "axios";
import appValidator from "../AppValidator.js";

const maxNameLength = 20;

class CategoryForm extends React.Component {
  state = {
    name: "",
    isLoading: true
  };

  componentDidMount() {
    if (this.props.match.params.categoryId) {
      this.getCategory(this.props.match.params.categoryId);
    } else {
      this.setState({ isLoading: false });
    }
  }

  getCategory = categoryId => {
    axios
      .get("/api/trainingCategory/" + categoryId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustCategoryFromDatabaseToState)
      .catch(error => {
        console.log(error);
      });
  };

  adjustCategoryFromDatabaseToState = category => {
    if (category) {
      this.setState({
        categoryId: category.categoryId,
        name: category.name,
        isLoading: false
      });
    } else {
      console.log("Training category does not exist");
    }
  };

  adjustCategoryFromStateToDatabase = () => {
    var newCategory = {
      categoryId: this.state.categoryId,
      name: this.state.name
    };

    return newCategory;
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  isFormNotValid = () => {
    var isNotValid = !appValidator.isCorrectLength(this.state.name, {
      min: 1,
      max: maxNameLength
    });
    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.state.categoryId) {
      this.updateCategory();
    } else {
      this.createCategory();
    }
  };

  createCategory = () => {
    var newCategory = this.adjustCategoryFromStateToDatabase();

    axios
      .post("/api/trainingCategory", newCategory)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  updateCategory = () => {
    var newCategory = this.adjustCategoryFromStateToDatabase();

    axios
      .put("/api/trainingCategory/" + newCategory.categoryId, newCategory)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <PageHeader title={this.props.title} />
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <TextField
            label="Category name"
            error={
              appValidator.sayIfNotCorrectLength(this.state.name, {
                max: maxNameLength
              })
                ? true
                : false
            }
            helperText={appValidator.sayIfNotCorrectLength(this.state.name, {
              max: maxNameLength
            })}
            className={textFieldStyle.longTextField}
            value={this.state.name}
            onChange={this.handleChange("name")}
            margin="normal"
          />

          <br />
          <br />

          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(CategoryForm);
