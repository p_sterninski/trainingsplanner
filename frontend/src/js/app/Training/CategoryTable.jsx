import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class CategoryTable extends React.Component {
  state = {
    initialData: [],
    chosenCategoryId: "",
    isLoading: true
  };

  componentDidMount() {
    this.getData();
  }

  getData() {
    axios.get("/api/trainingCategory").then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "name", label: "Name" }
    ];

    const data = this.state.initialData.map(row => {
      return [row.categoryId, row.name];
    });

    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.categoryId === rowData[0];
        });
        this.setState({
          chosenCategoryId: chosenObject.categoryId
        });
      }
    };

    if (this.state.chosenCategoryId) {
      return <Redirect push to={"/category/" + this.state.chosenCategoryId} />;
    }

    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <MUIDataTable
          title={"Training Category List"}
          data={data}
          columns={columns}
          options={options}
        />

        <br />

        <Button
          variant="contained"
          color="primary"
          className={textFieldStyle.textField}
          size="medium"
          type="button"
          component={Link}
          to={"/add/category"}
        >
          New Category
        </Button>
      </div>
    );
  }
}

export default CategoryTable;
