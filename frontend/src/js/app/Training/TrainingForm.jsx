import "date-fns";
import React from "react";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import axios from "axios";
import PaperForm from "../other/PaperForm.jsx";
import Divider from "@material-ui/core/Divider";
import EmployeeShortData from "./EmployeeShortData.jsx";
import EmployeeTable from "../person/EmployeeTable.jsx";
import Dialog from "@material-ui/core/Dialog";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import appValidator from "../AppValidator.js";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const nameLength = 5;

class TrainingForm extends React.Component {
  state = {
    code: "",
    name: "",
    categoryId: "",
    categoryArray: [],
    employeeArray: [],
    chosenEmployeeArray: [],
    fetchCounter: 0,
    isEmployeeTableVisible: false
  };

  componentDidMount() {
    this.getCategories();
    this.getEmployees();
    if (this.props.match.params.trainingId) {
      this.getTraining(this.props.match.params.trainingId);
    } else {
      this.setState({ fetchCounter: this.state.fetchCounter + 1 });
    }
  }

  getCategories() {
    axios.get("/api/trainingCategory").then(response => {
      if (response.data) {
        this.setState({
          categoryArray: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  getEmployees() {
    axios.get("/api/employee").then(response => {
      if (response.data) {
        this.setState({
          employeeArray: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  getTraining(trainingId) {
    axios
      .get("/api/training/" + trainingId)
      .then(response => response.data)
      .then(this.adjustTrainingFromDatabaseToState);
  }

  adjustTrainingFromStateToDatabase = () => {
    return axios
      .get("/api/trainingCategory/" + this.state.categoryId)
      .then(response => {
        var category = response.data;
        var newTraining = {
          trainingId: this.state.trainingId,
          code: this.state.code,
          name: this.state.name,
          category: category,
          employees: this.state.chosenEmployeeArray.map(employee => {
            delete employee.trainings;
            return employee;
          })
        };

        return newTraining;
      });
  };

  adjustTrainingFromDatabaseToState = training => {
    if (training) {
      this.setState({
        trainingId: training.trainingId,
        code: training.code,
        name: training.name,
        categoryId: training.category.categoryId,
        chosenEmployeeArray: training.employees,
        fetchCounter: this.state.fetchCounter + 1
      });
    } else {
      console.log("Training does not exist");
    }
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  setIsEmployeeTableVisible = value => event => {
    this.setState({ isEmployeeTableVisible: value });
  };

  handleDeleteEmployee = (employee, newChosenEmployeeArray, event) => {
    this.setState({ chosenEmployeeArray: newChosenEmployeeArray });
  };

  deleteEmployee = personId => event => {
    var newArray = this.state.chosenEmployeeArray.filter(employee => {
      return employee.personId !== personId;
    });

    this.setState({ chosenEmployeeArray: newArray });
  };

  addEmployee = newEmployee => {
    if (
      this.state.chosenEmployeeArray.find(employee => {
        return employee.personId === newEmployee.personId;
      })
    ) {
      this.props.addMessageDetails(
        "Employee " + newEmployee.name + " was already chosen",
        true
      );
      this.setIsEmployeeTableVisible(false)();
    } else {
      var newArray = this.state.chosenEmployeeArray.slice();
      newArray.push(newEmployee);
      this.setState({ chosenEmployeeArray: newArray });
      this.setIsEmployeeTableVisible(false)();
    }
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.code) ||
      !appValidator.isCorrectLength(this.state.code, {
        min: nameLength,
        max: nameLength
      }) ||
      appValidator.isEmpty(this.state.name) ||
      appValidator.isEmpty(this.state.categoryId) ||
      this.state.chosenEmployeeArray.length === 0;

    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.state.trainingId) {
      this.updateTraining();
    } else {
      this.createTraining();
    }
  };

  createTraining = () => {
    this.adjustTrainingFromStateToDatabase()
      .then(newTraining => {
        axios
          .post("/api/training", newTraining)
          .then(response => {
            this.props.addStandardSaveMessageDetails();
          })
          .catch(this.props.addErrorMessageDetailsFromPromise);
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  updateTraining = () => {
    this.adjustTrainingFromStateToDatabase()
      .then(newTraining => {
        axios
          .put("/api/training/" + newTraining.trainingId, newTraining)
          .then(response => {
            this.props.addStandardSaveMessageDetails();
          })
          .catch(this.props.addErrorMessageDetailsFromPromise);
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    const { classes } = this.props;

    if (this.state.fetchCounter < 2) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <PageHeader title={this.props.title} />
        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
            <PaperForm className={paperStyle.widePaper}>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="baseline"
              >
                <Grid item>
                  <TextField
                    select
                    required={true}
                    label="Training category"
                    className={textFieldStyle.textField}
                    value={this.state.categoryId}
                    onChange={this.handleChange("categoryId")}
                    margin="normal"
                  >
                    {this.state.categoryArray.map(item => (
                      <MenuItem key={item.categoryId} value={item.categoryId}>
                        {item.name}
                      </MenuItem>
                    ))}
                  </TextField>
                </Grid>
                <Grid item>
                  <TextField
                    required={true}
                    error={
                      appValidator.sayIfNotCorrectLength(this.state.code, {
                        min: nameLength,
                        max: nameLength
                      })
                        ? true
                        : false
                    }
                    helperText={appValidator.sayIfNotCorrectLength(
                      this.state.code,
                      { min: nameLength, max: nameLength }
                    )}
                    label="Training code"
                    className={textFieldStyle.textField}
                    value={this.state.code}
                    onChange={this.handleChange("code")}
                    margin="normal"
                  />
                </Grid>
              </Grid>

              <TextField
                required={true}
                label="Training name"
                className={textFieldStyle.longTextField}
                value={this.state.name}
                onChange={this.handleChange("name")}
                margin="normal"
              />

              <br />
              <br />
            </PaperForm>
          </Grid>

          <br />
          <Divider />
          <br />

          <h2>Employees</h2>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="baseline"
          >
            {this.state.chosenEmployeeArray.map(employee => {
              return (
                <EmployeeShortData
                  key={employee.personId}
                  personId={employee.personId}
                  employeeNo={employee.employeeNo}
                  isActive={employee.isActive}
                  employeeName={employee.name}
                  deleteEmployee={this.deleteEmployee}
                />
              );
            })}
          </Grid>

          <Button
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="button"
            onClick={this.setIsEmployeeTableVisible(true)}
          >
            New Employee
          </Button>

          <br />
          <br />
          <Divider />
          <br />

          <Button
            disabled={this.isFormNotValid()}
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
        </form>

        <Dialog
          open={this.state.isEmployeeTableVisible === true}
          onClose={this.setIsEmployeeTableVisible(false)}
          fullWidth={true}
          maxWidth="xl"
        >
          <EmployeeTable
            actionOnChosenData={this.addEmployee}
            noButtons={true}
            type="onlyTrainingAndActive"
          />
        </Dialog>
      </div>
    );
  }
}

export default withConfirmationSnackbar(TrainingForm);
