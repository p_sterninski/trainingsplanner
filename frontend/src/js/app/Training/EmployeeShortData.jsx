import React from "react";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import DeleteIcon from "@material-ui/icons/Delete";
import Fab from "@material-ui/core/Fab";
import PaperForm from "../other/PaperForm.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const EmployeeShortData = ({
  personId,
  employeeNo,
  isActive,
  employeeName,
  deleteEmployee
}) => {
  return (
    <PaperForm className={paperStyle.standardPaper}>
      <Grid container alignItems="flex-end">
        <Grid item>
          <TextField
            disabled={true}
            label="Employee number"
            className={textFieldStyle.textField}
            value={employeeNo}
            margin="normal"
          />
        </Grid>
        <Grid item>
          <FormControlLabel
            control={
              <Checkbox
                disabled={true}
                checked={isActive}
                value="isActive"
                color="primary"
              />
            }
            label="Active"
            className={textFieldStyle.textField}
          />
        </Grid>
      </Grid>

      <br />

      <TextField
        disabled={true}
        label="Name"
        className={textFieldStyle.longTextField}
        value={employeeName}
        margin="normal"
      />

      <Grid container justify="flex-end">
        <Fab
          color="default"
          aria-label="Delete"
          size="small"
          onClick={deleteEmployee(personId)}
        >
          <DeleteIcon />
        </Fab>
      </Grid>
    </PaperForm>
  );
};

export default EmployeeShortData;
