import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import { Redirect } from "react-router";
import appDate from "../AppDate.js";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";

class TrainingListTable extends React.Component {
  state = {
    redirect: false,
    initialData: [],
    columns: [],
    data: [],
    options: [],
    chosenObjectArray: [],
    isLoading: true
  };

  componentDidMount() {
    this.getData();
  }

  getData() {
    var apiUrl = "/api/training";
    apiUrl += this.props.onlyUserTrainings ? "/currentUser" : "";
    axios.get(apiUrl).then(response => {
      if (response.data) {
        const columns = [
          {
            name: "id",
            label: "id",
            options: { display: "false", filter: false }
          },
          { name: "code", label: "Code" },
          { name: "name", label: "Name" },
          { name: "startDate", label: "Start Date" },
          { name: "endDate", label: "End Date" },
          { name: "category", label: "Category" }
        ];

        const data = response.data
          .map(row => {
            return [
              row.trainingId,
              row.code,
              row.name,
              appDate.getDateTime(row.startDate),
              appDate.getDateTime(row.endDate),
              row.category.name,
              row.startDate,
              row.endDate
            ];
          })
          .sort((training1, training2) => {
            if (training1[6] == null && training2[6] == null) {
              return 0;
            } else if (training2[6] == null) {
              return 1;
            } else if (training1[6] == null) {
              return -1;
            } else {
              return Date.parse(training2[6]) - Date.parse(training1[6]);
            }
          });

        const options = {
          filterType: "textField",
          selectableRows: "none",
          download: false,
          viewColumns: false,
          selectableRowsOnClick: true,
          print: false,
          fixedHeader: true,
          sort: false,
          onRowClick: (rowData, rowMeta) => {
            if (this.props.actionOnChosenData) {
              var chosenObject = this.state.initialData.find(object => {
                return object.trainingId === rowData[0];
              });
              this.props.actionOnChosenData(chosenObject);
            } else {
              var chosenObjectArray = this.state.data.find(objectArray => {
                return objectArray[0] === rowData[0];
              });
              this.setState({
                redirect: true,
                chosenObjectArray: chosenObjectArray
              });
            }
          }
        };

        this.setState({
          columns,
          data,
          options,
          initialData: response.data,
          isLoading: false
        });
      }
    });
  }

  render() {
    if (this.state.redirect) {
      var url = "/training/" + this.state.chosenObjectArray[0];
      var urlPrefix = this.props.onlyUserTrainings ? "/myTrainingList" : "";
      url = urlPrefix + url;
      return <Redirect push to={url} />;
    }

    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <MUIDataTable
        title={"Training List"}
        data={this.state.data}
        columns={this.state.columns}
        options={this.state.options}
      />
    );
  }
}

export default TrainingListTable;
