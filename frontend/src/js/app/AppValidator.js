import validator from "validator";

class AppValidator {
  // is functions
  static isEmpty = input => {
    if (input && typeof input === "object") {
      return Object.keys(input).length === 0 && input.constructor === Object;
    } else {
      if (!input || validator.isEmpty(validator.trim(input + ""))) {
        return true;
      }
    }
    return false;
  };

  static isEmail = input => {
    input = AppValidator.replaceNull(input, "");
    return input === "" || validator.isEmail(validator.trim(input));
  };

  static isInteger = (input, options = { min: undefined, max: undefined }) => {
    var optionsUsed = {};
    if (options.min) {
      optionsUsed.min = options.min;
    }
    if (options.max) {
      optionsUsed.max = options.max;
    }

    input = AppValidator.replaceNull(input, "");
    return validator.isInt(input.toString(), optionsUsed);
  };

  static isFloat = input => {
    input = AppValidator.replaceNull(input, "");
    return input === "" || validator.isFloat(input.toString());
  };

  static isFloatOrEmpty = input => {
    input = AppValidator.replaceNull(input, "");
    return AppValidator.isEmpty(input) || AppValidator.isFloat(input);
  };

  static isCorrectLength = (input, options = { min: 0, max: undefined }) => {
    return validator.isLength(input.trim(), options);
  };

  //sayIf functions
  static sayIfEmpty = input => {
    return input === "" || !validator.isEmpty(validator.trim(input))
      ? ""
      : "Field is required";
  };

  static sayIfNotInteger = (
    input,
    options = { min: undefined, max: undefined }
  ) => {
    var errorText = "Integer value is required";
    if (options.min || options.max) {
      errorText += ": ";
      errorText += options.min ? "min = " + options.min + " " : "";
      errorText += options.max ? "max = " + options.max : "";
      errorText = errorText.trim();
    }
    input = AppValidator.replaceNull(input, "");
    return input === "" || AppValidator.isInteger(input, options)
      ? ""
      : errorText;
  };

  static sayIfNotEmail = input => {
    input = AppValidator.replaceNull(input, "");
    return AppValidator.isEmail(input) //input === "" || validator.isEmail(validator.trim(input))
      ? ""
      : "It is not valid email";
  };

  static sayIfNotFloat = input => {
    input = AppValidator.replaceNull(input, "");
    return AppValidator.isEmpty(input) || AppValidator.isFloat(input)
      ? ""
      : "Float number is required";
  };

  static sayIfNotCorrectLength = (
    input,
    options = { min: 0, max: undefined }
  ) => {
    input = AppValidator.replaceNull(input, "");
    var errorText = "Expected length:";
    if (options.min === options.max) {
      errorText += " " + options.min;
    } else {
      errorText +=
        options.min && AppValidator.isInteger(options.min, { min: 1 })
          ? " min = " + options.min
          : "";
      errorText += " max = " + (options.max ? options.max : "Inf");
    }
    return AppValidator.isEmpty(input) ||
      AppValidator.isCorrectLength(input, options)
      ? ""
      : errorText;
  };

  static sayIfDateIsAfterOrEqual = (source, target) => {
    return AppValidator.isEmpty(source) ||
      AppValidator.compareDate(source, target) >= 0
      ? "Date is too late"
      : "";
  };

  static sayIfDateIsBeforeOrEqual = (source, target) => {
    return AppValidator.isEmpty(source) ||
      AppValidator.compareDate(source, target) <= 0
      ? "Date is too early"
      : "";
  };

  //other
  static replaceNull = (input, replacement) => {
    if (input === null) {
      return replacement;
    } else {
      return input;
    }
  };

  static compareDate = (source, target) => {
    var targetString = target.toString();
    var sourceString = source.toString();

    if (validator.isAfter(sourceString, targetString)) {
      return 1;
    } else if (validator.isBefore(sourceString, targetString)) {
      return -1;
    } else {
      return 0;
    }
  };
}

export default AppValidator;
