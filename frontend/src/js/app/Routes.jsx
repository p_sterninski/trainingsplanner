import React from "react";
import { Route, Switch } from "react-router-dom";
import CategoryForm from "./training/CategoryForm.jsx";
import TrainingForm from "./training/TrainingForm.jsx";
import InvoiceForm from "./invoice/InvoiceForm.jsx";
import PersonForm from "./person/PersonForm.jsx";
import TrainingTeamForm from "./locationTask/TrainingTeamForm.jsx";
import LocationTaskForm from "./locationTask/LocationTaskForm.jsx";
import ProductForm from "./product/ProductForm.jsx";
import LocationForm from "./location/LocationForm.jsx";
import PersonTable from "./person/PersonTable.jsx";
import EmployeeTable from "./person/EmployeeTable.jsx";
import CategoryTable from "./training/CategoryTable.jsx";
import TrainingListTable from "./training/TrainingListTable.jsx";
import InvoiceTable from "./invoice/InvoiceTable.jsx";
import ProductTable from "./product/ProductTable.jsx";
import LocationTable from "./location/LocationTable.jsx";
import RoomTable from "./location/RoomTable.jsx";
import RoomForm from "./location/RoomForm.jsx";
import NotFoundPage from "./error/NotFoundPage.jsx";
import LoginForm from "./security/LoginForm.jsx";
import ForgotPasswordForm from "./security/ForgotPasswordForm.jsx";
import ChangePasswordForm from "./security/ChangePasswordForm.jsx";
import LogoutPage from "./security/LogoutPage.jsx";
import ProtectedRoute from "./security/ProtectedRoute.jsx";
import AuthenticationProvider from "./security/AuthenticationProvider.js";
import UserRoleForm from "./person/UserRoleForm.jsx";

const roleAssignment = AuthenticationProvider.roleAssignment;

export const Routes = () => (
  <Switch>
    <Route exact path="/" component={TrainingListTable} />
    <Route
      exact
      path="/login"
      render={props => <LoginForm {...props} title="Login Page" />}
    />
    <Route
      exact
      path="/login/forgotPassword"
      render={props => <ForgotPasswordForm {...props} title="Reset Password" />}
    />
    <Route exact path="/logout" component={LogoutPage} />
    <Route
      exact
      path="/training/:trainingId"
      render={props => (
        <TrainingTeamForm
          {...props}
          readOnly={true}
          key={1}
          title="Training Overview"
        />
      )}
    />
    <ProtectedRoute
      exact
      path="/myTrainingList/training/:trainingId"
      component={TrainingTeamForm}
      componentProps={{
        key: 2,
        title: "Training Overview"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/myTrainingList/update/training/:trainingId"
      component={TrainingForm}
      componentProps={{
        title: "Update Training"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/locationTask/:taskId"
      component={LocationTaskForm}
      componentProps={{
        title: "Update Location Task"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/add/locationTask/training/:trainingId"
      component={LocationTaskForm}
      componentProps={{
        title: "Add Location Task"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/myTrainingList"
      component={TrainingListTable}
      componentProps={{
        onlyUserTrainings: true
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/invoice"
      component={InvoiceTable}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/category"
      component={CategoryTable}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/add/category"
      component={CategoryForm}
      componentProps={{
        title: "Add Category"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/category/:categoryId"
      component={CategoryForm}
      componentProps={{
        title: "Update Category"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/add/training"
      component={TrainingForm}
      componentProps={{
        title: "Add Training"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/product"
      component={ProductTable}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/add/product"
      component={ProductForm}
      componentProps={{
        title: "Add Product"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/product/:productId"
      component={ProductForm}
      componentProps={{
        title: "Update Product"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/person"
      component={PersonTable}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/employee"
      component={EmployeeTable}
      componentProps={{
        key: 1
      }}
      requiredRoles={roleAssignment.HR}
    />
    <ProtectedRoute
      exact
      path="/user"
      component={EmployeeTable}
      componentProps={{
        key: 2,
        nextPath: "/user/"
      }}
      requiredRoles={roleAssignment.ADMIN}
    />
    <ProtectedRoute
      exact
      path="/user/:personId"
      component={UserRoleForm}
      componentProps={{
        title: "Update user roles"
      }}
      requiredRoles={roleAssignment.ADMIN}
    />
    <ProtectedRoute
      exact
      path="/location"
      component={LocationTable}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/location/:locationId"
      component={LocationForm}
      componentProps={{
        title: "Update Location"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/add/location"
      component={LocationForm}
      componentProps={{
        title: "Add new Location"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/location/:locationId/room"
      component={RoomTable}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/location/:locationId/room/:roomId"
      component={RoomForm}
      componentProps={{
        title: "Update Room"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/location/:locationId/add/room"
      component={RoomForm}
      componentProps={{
        title: "Add new Room"
      }}
      requiredRoles={roleAssignment.TRAINING}
    />
    <ProtectedRoute
      exact
      path="/add/costInvoice"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: true,
        isCorrection: false,
        title: "Add new cost invoice",
        key: 1
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/costInvoice/:invoiceId"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: true,
        isCorrection: false,
        title: "Cost invoice",
        key: 2
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/add/costInvoice/correction/:invoiceId"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: true,
        isCorrection: true,
        title: "Add new cost corrective invoice",
        key: 3
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/cancel/costInvoice/:invoiceId"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: true,
        isCorrection: true,
        isCancellation: true,
        title: "Cancel cost invoice",
        key: 4
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/add/salesInvoice"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: false,
        isCorrection: false,
        title: "Add new sales invoice",
        key: 5
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/salesInvoice/:invoiceId"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: false,
        isCorrection: false,
        title: "Sales invoice",
        key: 6
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/add/salesInvoice/correction/:invoiceId"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: false,
        isCorrection: true,
        title: "Add new sales corrective invoice",
        key: 7
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/cancel/salesInvoice/:invoiceId"
      component={InvoiceForm}
      componentProps={{
        isCostInvoice: false,
        isCorrection: true,
        isCancellation: true,
        title: "Cancel sales invoice",
        key: 8
      }}
      requiredRoles={roleAssignment.ACCOUNTING}
    />
    <ProtectedRoute
      exact
      path="/add/legalPerson"
      component={PersonForm}
      componentProps={{
        isLegalPerson: true,
        title: "Add new client (legal person)",
        key: 1
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/legalPerson/:personId"
      component={PersonForm}
      componentProps={{
        isLegalPerson: true,
        title: "Edit client (legal person)",
        key: 1
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/add/naturalPerson"
      component={PersonForm}
      componentProps={{
        isLegalPerson: false,
        title: "Add new client (natural person)",
        key: 2
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/naturalPerson/:personId"
      component={PersonForm}
      componentProps={{
        isLegalPerson: false,
        title: "Edit client (natural person)",
        key: 2
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/add/legalPersonEmployee"
      component={PersonForm}
      componentProps={{
        isLegalPerson: true,
        title: "Add new employee (legal person)",
        key: 3
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/legalPersonEmployee/:personId"
      component={PersonForm}
      componentProps={{
        isLegalPerson: true,
        title: "Edit employee (legal person)",
        key: 3
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/add/naturalPersonEmployee"
      component={PersonForm}
      componentProps={{
        isLegalPerson: false,
        title: "Add new employee (natural person)",
        key: 4
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/naturalPersonEmployee/:personId"
      component={PersonForm}
      componentProps={{
        isLegalPerson: false,
        title: "Edit employee (natural person)",
        key: 4
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <ProtectedRoute
      exact
      path="/changePassword"
      component={ChangePasswordForm}
      componentProps={{
        title: "Change password"
      }}
      requiredRoles={roleAssignment.ALL}
    />
    <Route render={props => <NotFoundPage {...props} title="Error 404" />} />
  </Switch>
);

export default Routes;
