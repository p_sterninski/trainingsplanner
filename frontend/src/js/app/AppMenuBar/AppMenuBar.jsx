import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import MenuButton from "./MenuButton.jsx";
import Button from "@material-ui/core/Button";
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import AuthenticationProvider from "../security/AuthenticationProvider.js";

const styles = {
  root: {
    flexGrow: 1
  },
  undecorateLink: {
    textDecoration: "none",
    color: "white"
  }
};

const roleAssigment = AuthenticationProvider.roleAssignment;

function AppMenuBar(props) {
  const { classes } = props;

  const trainingMenuOptions = [
    { name: "My Training List", link: "/myTrainingList" },
    { name: "New Training", link: "/add/training" },
    { name: "Category", link: "/category" },
    { name: "Product", link: "/product" },
    { name: "Location", link: "/location" }
  ];

  const personMenuOptions = [
    { name: "Client", link: "/person", requiredRoles: roleAssigment.ALL },
    { name: "Employee", link: "/employee", requiredRoles: roleAssigment.HR }
  ];

  const invoiceMenuOptions = [{ name: "Invoice List", link: "/invoice" }];

  const adminMenuOptions = [{ name: "User Role", link: "/user" }];

  function showMenuButton(buttonName, buttonOptions, requiredRoles) {
    if (AuthenticationProvider.isUserAuthorized(requiredRoles) === true) {
      return <MenuButton name={buttonName} options={buttonOptions} />;
    } else {
      return "";
    }
  }

  const getUsername = () => {
    const username = AuthenticationProvider.getLoggedInUserName();
    if (username) {
      return (
        <Link className={classes.undecorateLink} to={"/changePassword"}>
          {username}
        </Link>
      );
    }

    return "";
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                wrap="nowrap"
              >
                <IconButton color="inherit" component={Link} to={"/"}>
                  <HomeOutlinedIcon />
                </IconButton>
                {showMenuButton(
                  "Training",
                  trainingMenuOptions,
                  roleAssigment.TRAINING
                )}
                {showMenuButton("Person", personMenuOptions, roleAssigment.ALL)}
                {showMenuButton(
                  "Invoice",
                  invoiceMenuOptions,
                  roleAssigment.ACCOUNTING
                )}
                {showMenuButton("Admin", adminMenuOptions, roleAssigment.ADMIN)}
              </Grid>
            </Grid>
            <Grid item>
              <Grid
                container
                direction="row"
                justify="flex-start"
                alignItems="center"
                wrap="nowrap"
              >
                {getUsername()}
                &nbsp;&nbsp;
                <Button
                  color="inherit"
                  type="button"
                  component={Link}
                  to={
                    AuthenticationProvider.isUserLoggedIn()
                      ? "/logout"
                      : "/login"
                  }
                >
                  {AuthenticationProvider.isUserLoggedIn() ? "Logout" : "Login"}
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}

AppMenuBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(AppMenuBar);
