import React from "react";
import Button from "@material-ui/core/Button";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import { Link } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";
import PropTypes from "prop-types";
import AuthenticationProvider from "../security/AuthenticationProvider.js";

const styles = theme => ({
  root: {
    display: "flex"
  },
  popper: {
    zIndex: "101"
  }
});

class MenuButton extends React.Component {
  state = {
    open: false
  };

  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    const { classes, name, options } = this.props;
    return (
      <div className={classes.root}>
        <Button
          color="inherit"
          buttonRef={node => {
            this.anchorEl = node;
          }}
          aria-owns={open ? "menu-list-grow" : undefined}
          aria-haspopup="true"
          onClick={this.handleToggle}
        >
          {name}
        </Button>
        <Popper
          className={classes.popper}
          open={open}
          anchorEl={this.anchorEl}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              id="menu-list-grow"
              style={{
                transformOrigin:
                  placement === "bottom" ? "center top" : "center bottom"
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={this.handleClose}>
                  <MenuList>
                    {options.map(option => {
                      if (
                        option.requiredRoles === undefined ||
                        AuthenticationProvider.isUserAuthorized(
                          option.requiredRoles
                        ) === true
                      ) {
                        return (
                          <MenuItem
                            key={option.link}
                            onClick={this.handleClose}
                            component={Link}
                            to={option.link}
                          >
                            {option.name}
                          </MenuItem>
                        );
                      } else {
                        return "";
                      }
                    })}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </div>
    );
  }
}

MenuButton.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(MenuButton);
