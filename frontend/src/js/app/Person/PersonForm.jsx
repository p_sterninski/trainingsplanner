import React from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";
import appValidator from "../AppValidator.js";
import Grid from "@material-ui/core/Grid";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import ContactDetails from "./ContactDetails.jsx";
import AddressDetails from "./AddressDetails.jsx";
import EmployeeDetails from "./EmployeeDetails.jsx";
import AuthenticationProvider from "../security/AuthenticationProvider.js";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import gridStyle from "../../styles/GridStyle.module.css";

const maxEmployeeNoLength = 10;
const maxPositionLength = 30;

class PersonForm extends React.Component {
  state = {
    //Natural Person
    nationalNo: "",

    //Natural/Legal Person
    taxNo: "",

    //Person
    name: "",
    address: "",
    phone: "",
    email: "",

    //Employee
    employeeNo: "",
    position: "",
    departmentId: "",
    isActive: true,
    workEmail: "",

    //Other
    countryId: "",
    cityName: "",
    countryArray: [],
    cityArray: [],
    departmentArray: [],
    fetchCounter: 0,
    isEmployee: false,
    isEmployeeDetailsVisible: false
  };

  componentDidMount() {
    this.getCountries();
    this.getDepartments();

    if (this.props.match.params.personId) {
      this.getPerson(this.props.match.params.personId);
    } else {
      this.getCities(); //if person exists cities are retrived in getPerson() based on existing countryId
    }
  }

  getCountries() {
    axios.get("/api/country").then(response => {
      if (response.data) {
        this.setState({
          countryArray: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  getCities() {
    if (arguments.length === 0) {
      axios.get("/api/city").then(response => {
        if (response.data) {
          this.setState({
            cityArray: response.data,
            fetchCounter: this.state.fetchCounter + 1
          });
        }
      });
    } else if (arguments.length === 1) {
      axios.get("/api/country/cities/" + arguments[0]).then(response => {
        if (response.data) {
          this.setState({ cityArray: response.data });
        }
      });
    }
  }

  getDepartments() {
    axios.get("/api/department").then(response => {
      if (response.data) {
        this.setState({
          departmentArray: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  getPerson = personId => {
    axios
      .get("/api/person/" + personId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustPersonFromDatabaseToState)
      .catch(error => {
        console.log(error);
      });
  };

  adjustPersonFromDatabaseToState = person => {
    if (person) {
      var isHRRoleLoggedIn = AuthenticationProvider.isUserAuthorized(
        AuthenticationProvider.roleAssignment.HR
      );

      this.setState({
        personId: person.personId,
        nationalNo: person.nationalNo,
        taxNo: person.taxNo,
        name: person.name,
        address: person.address,
        phone: person.phone,
        email: person.email,
        cityName: person.city.name,
        countryId: person.city.country.countryId,
        cityArray: this.getCities(person.city.country.countryId),
        employeeNo: person.employeeNo ? person.employeeNo : "",
        position: person.position ? person.position : "",
        departmentId: person.department ? person.department.departmentId : "",
        isActive: person.employeeNo ? person.isActive : true,
        isEmployeeDetailsVisible:
          isHRRoleLoggedIn && person.employeeNo ? true : false,
        isEmployee: person.employeeNo ? true : false,
        workEmail: person.workEmail ? person.workEmail : "",
        fetchCounter: this.state.fetchCounter + 1
      });
    } else {
      console.log("Person does not exist");
    }
  };

  adjustPersonEmployeeFromStateToDatabase = () => {
    var city = {
      name: this.state.cityName,
      country: { countryId: this.state.countryId }
    };
    var newPerson = {
      personId: this.state.personId,
      nationalNo: this.state.nationalNo,
      taxNo: this.state.taxNo,
      name: this.state.name,
      address: this.state.address,
      phone: this.state.phone,
      email: this.state.email,
      isLegalPerson: this.props.isLegalPerson,
      city: city
    };

    if (this.state.isEmployeeDetailsVisible) {
      var department = this.state.departmentArray.find(department => {
        return department.departmentId === this.state.departmentId;
      });

      //add employee attributes
      newPerson.employeeNo = this.state.employeeNo;
      newPerson.position = this.state.position;
      newPerson.isActive = this.state.isActive;
      newPerson.workEmail = this.state.workEmail;
      newPerson.department = department;
    }

    return newPerson;
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
    if (input === "countryId") {
      this.getCities(event.target.value);
    }
  };

  handleChangeCheckbox = input => event => {
    this.setState({
      [input]: event.target.checked
    });
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.name) ||
      appValidator.isEmpty(this.state.address) ||
      appValidator.isEmpty(this.state.countryId) ||
      appValidator.isEmpty(this.state.cityName) ||
      !appValidator.isEmail(this.state.email);

    //only for legal Person
    if (this.props.isLegalPerson) {
      isNotValid = isNotValid || appValidator.isEmpty(this.state.taxNo);
    }

    //only for employee
    if (this.state.isEmployeeDetailsVisible) {
      isNotValid =
        isNotValid ||
        !appValidator.isCorrectLength(this.state.employeeNo, {
          min: 1,
          max: maxEmployeeNoLength
        }) ||
        !appValidator.isCorrectLength(this.state.position, {
          min: 1,
          max: maxPositionLength
        }) ||
        appValidator.isEmpty(this.state.departmentId) ||
        appValidator.isEmpty(this.state.workEmail) ||
        !appValidator.isEmail(this.state.workEmail);
    }

    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    var person = this.adjustPersonEmployeeFromStateToDatabase();
    if (person.personId) {
      this.updatePersonEmployee(person);
    } else {
      this.saveNewPersonEmployee(person);
    }
  };

  saveNewPersonEmployee = person => {
    var link = this.state.isEmployeeDetailsVisible
      ? "/api/employee"
      : "/api/person";
    axios
      .post(link, person)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  updatePersonEmployee = person => {
    var link;
    if (this.state.isEmployeeDetailsVisible) {
      link = "/api/employee/";
    } else {
      link = "/api/person/";
    }
    axios
      .put(link + person.personId, person)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
        return response.data;
      })
      .then(this.adjustPersonFromDatabaseToState)
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.fetchCounter < 3) {
      return <LoadingPageIndicator />;
    }

    var isHRRoleLoggedIn = AuthenticationProvider.isUserAuthorized(
      AuthenticationProvider.roleAssignment.HR
    );

    return (
      <div>
        <PageHeader title={this.props.title} />

        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
            className={gridStyle.gridWithElementsOnRow}
          >
            <Grid item>
              <ContactDetails
                isLegalPerson={this.props.isLegalPerson}
                nationalNo={this.state.nationalNo}
                onNationalNoChange={this.handleChange("nationalNo")}
                taxNo={this.state.taxNo}
                onTaxNoChange={this.handleChange("taxNo")}
                name={this.state.name}
                onNameChange={this.handleChange("name")}
                phone={this.state.phone}
                onPhoneChange={this.handleChange("phone")}
                email={this.state.email}
                onEmailChange={this.handleChange("email")}
              />

              <br />
            </Grid>
            <Grid item>
              <AddressDetails
                address={this.state.address}
                onAddressChange={this.handleChange("address")}
                countryId={this.state.countryId}
                onCountryIdChange={this.handleChange("countryId")}
                countryArray={this.state.countryArray}
                cityName={this.state.cityName}
                onCityNameChange={this.handleChange("cityName")}
                cityArray={this.state.cityArray}
              />

              <br />
            </Grid>
            <Grid item>
              {this.state.isEmployeeDetailsVisible ? (
                <EmployeeDetails
                  isNameVisible={false}
                  employeeNo={this.state.employeeNo}
                  onEmployeeNoChange={this.handleChange("employeeNo")}
                  isActive={this.state.isActive}
                  onIsActiveChange={this.handleChangeCheckbox("isActive")}
                  position={this.state.position}
                  onPositionChange={this.handleChange("position")}
                  workEmail={this.state.workEmail}
                  onWorkEmailChange={this.handleChange("workEmail")}
                  departmentId={this.state.departmentId}
                  onDepartmentIdChange={this.handleChange("departmentId")}
                  departmentArray={this.state.departmentArray}
                  maxEmployeeNoLength={maxEmployeeNoLength}
                  maxPositionLength={maxPositionLength}
                />
              ) : (
                ""
              )}
            </Grid>
          </Grid>

          <br />
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
          >
            <Button
              disabled={this.isFormNotValid()}
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="submit"
            >
              Save
            </Button>

            {isHRRoleLoggedIn && !this.state.isEmployee ? (
              <Button
                variant="contained"
                color="primary"
                className={textFieldStyle.textField}
                size="medium"
                type="button"
                onClick={() =>
                  this.setState({
                    isEmployeeDetailsVisible: !this.state
                      .isEmployeeDetailsVisible
                  })
                }
              >
                {this.state.isEmployeeDetailsVisible
                  ? "Hide Employee Data"
                  : "Add Employee Data"}
              </Button>
            ) : (
              ""
            )}
          </Grid>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(PersonForm);
