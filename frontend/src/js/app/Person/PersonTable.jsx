import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class PersonTable extends React.Component {
  state = {
    initialData: [],
    isLoading: true
  };

  componentDidMount() {
    if (this.props.onlyLegalPerson) {
      this.getDataOnlyLegalPerson();
    } else {
      this.getData();
    }
  }

  getData() {
    axios.get("/api/person").then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  getDataOnlyLegalPerson() {
    axios.get("/api/person/legalPerson").then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "name", label: "Name" },
      { name: "address", label: "Address" },
      { name: "city", label: "City" },
      { name: "country", label: "Country" },
      { name: "email", label: "Email" },
      { name: "phone", label: "Phone" },
      { name: "isLegalPerson", label: "Is Legal Person" }
    ];

    const data = this.state.initialData.map(row => {
      return [
        row.personId,
        row.name,
        row.address,
        row.city.name,
        row.city.country.name,
        row.email,
        row.phone,
        row.isLegalPerson ? "Yes" : "No"
      ];
    });

    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.personId === rowData[0];
        });
        if (this.props.actionOnChosenData) {
          this.props.actionOnChosenData(chosenObject);
        } else {
          this.setState({
            chosenPersonId: chosenObject.personId,
            isLegalPerson: chosenObject.isLegalPerson
          });
        }
      }
    };

    let createNewPersonButtons = show => {
      if (show) {
        return (
          <div>
            <br />

            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={"/add/naturalPerson"}
            >
              New Natural Person
            </Button>

            <Button
              variant="contained"
              color="primary"
              className={textFieldStyle.textField}
              size="medium"
              type="button"
              component={Link}
              to={"/add/legalPerson"}
            >
              New Legal Person
            </Button>
          </div>
        );
      }
    };

    if (this.state.chosenPersonId) {
      if (this.state.isLegalPerson) {
        return (
          <Redirect push to={"/legalPerson/" + this.state.chosenPersonId} />
        );
      } else {
        return (
          <Redirect push to={"/naturalPerson/" + this.state.chosenPersonId} />
        );
      }
    }

    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <MUIDataTable
          title={"Client List"}
          data={data}
          columns={columns}
          options={options}
        />

        {createNewPersonButtons(!this.props.noButtons)}
      </div>
    );
  }
}

export default PersonTable;
