import "date-fns";
import React from "react";
import TextField from "@material-ui/core/TextField";
import textFieldStyle from "../../styles/TextFieldStyle.module.css";

const PersonShortData = props => {
  return (
    <div>
      <TextField
        label="Provider name"
        className={textFieldStyle.longTextField}
        value={props.providerName}
        margin="normal"
        InputProps={{
          readOnly: true
        }}
      />

      <TextField
        label="Provider address"
        className={textFieldStyle.longTextField}
        value={props.providerAddress}
        margin="normal"
        InputProps={{
          readOnly: true
        }}
      />
    </div>
  );
};

export default PersonShortData;
