import React from "react";
import TextField from "@material-ui/core/TextField";
import appValidator from "../AppValidator.js";
import MenuItem from "@material-ui/core/MenuItem";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import PaperForm from "../other/PaperForm.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const EmployeeDetails = ({
  employeeNo,
  onEmployeeNoChange,
  name = "",
  onNameChange,
  isActive,
  onIsActiveChange,
  position,
  onPositionChange,
  workEmail,
  onWorkEmailChange,
  departmentId,
  onDepartmentIdChange,
  departmentArray,
  isDisabled = false,
  isNameVisible = true,
  maxEmployeeNoLength,
  maxPositionLength
}) => {
  return (
    <PaperForm className={paperStyle.standardPaper} title="Employee Details">
      <Grid container alignItems="flex-end">
        <Grid item>
          <TextField
            required={true}
            disabled={isDisabled}
            error={
              appValidator.sayIfNotCorrectLength(employeeNo, {
                max: maxEmployeeNoLength
              })
                ? true
                : false
            }
            helperText={appValidator.sayIfNotCorrectLength(employeeNo, {
              max: maxEmployeeNoLength
            })}
            label="Employee number"
            className={textFieldStyle.textField}
            value={employeeNo}
            onChange={onEmployeeNoChange}
            margin="normal"
          />
        </Grid>
        <Grid item>
          <FormControlLabel
            disabled={isDisabled}
            control={
              <Checkbox
                checked={isActive}
                onChange={onIsActiveChange}
                value="isActive"
                color="primary"
              />
            }
            label="Active"
            className={textFieldStyle.textField}
          />
        </Grid>
      </Grid>

      {isNameVisible ? (
        <React.Fragment>
          <br />
          <TextField
            required={true}
            disabled={isDisabled}
            error={appValidator.sayIfEmpty(name) ? true : false}
            helperText={appValidator.sayIfEmpty(name)}
            label="Name"
            className={textFieldStyle.longTextField}
            value={name}
            onChange={onNameChange}
            margin="normal"
          />
          <br />
        </React.Fragment>
      ) : (
        ""
      )}

      <br />

      <TextField
        required={true}
        disabled={isDisabled}
        error={
          appValidator.sayIfNotCorrectLength(position, {
            max: maxPositionLength
          })
            ? true
            : false
        }
        helperText={appValidator.sayIfNotCorrectLength(position, {
          max: maxPositionLength
        })}
        label="Position"
        className={textFieldStyle.longTextField}
        value={position}
        onChange={onPositionChange}
        margin="normal"
      />

      <br />
      <br />

      <TextField
        required={true}
        disabled={isDisabled}
        error={appValidator.sayIfNotEmail(workEmail) ? true : false}
        helperText={appValidator.sayIfNotEmail(workEmail)}
        label="Work e-mail"
        className={textFieldStyle.longTextField}
        value={workEmail}
        onChange={onWorkEmailChange}
        margin="normal"
      />

      <br />
      <br />

      <TextField
        select
        required={true}
        disabled={isDisabled}
        label="Department"
        className={textFieldStyle.longTextField}
        value={departmentId}
        onChange={onDepartmentIdChange}
        margin="normal"
      >
        {departmentArray.map(item => (
          <MenuItem key={item.departmentId} value={item.departmentId}>
            {item.name}
          </MenuItem>
        ))}
      </TextField>
    </PaperForm>
  );
};

export default EmployeeDetails;
