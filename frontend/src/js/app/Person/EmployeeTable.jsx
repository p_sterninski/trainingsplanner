import React from "react";
import axios from "axios";
import MUIDataTable from "mui-datatables";
import { Redirect } from "react-router";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";

class PersonTable extends React.Component {
  state = {
    initialData: [],
    isLoading: true
  };

  componentDidMount() {
    this.getData();
  }

  getData() {
    var url = "/api/employee";
    if (this.props.type === "onlyTrainingAndActive") {
      url += "/onlyTrainingAndActive";
    }

    axios.get(url).then(response => {
      if (response.data) {
        this.setState({ initialData: response.data, isLoading: false });
      }
    });
  }

  render() {
    const columns = [
      { name: "id", label: "id", options: { display: "false", filter: false } },
      { name: "name", label: "Name" },
      { name: "employeeNo", label: "Employee No" },
      { name: "position", label: "Position" },
      { name: "department", label: "Department" },
      { name: "isActive", label: "Is Active" },
      { name: "isLegalPerson", label: "Is Legal Person" }
    ];

    const data = this.state.initialData.map(row => {
      return [
        row.personId,
        row.name,
        row.employeeNo,
        row.position,
        row.department.name,
        row.isActive ? "Yes" : "No",
        row.isLegalPerson ? "Yes" : "No"
      ];
    });

    const options = {
      filterType: "textField",
      selectableRows: "none",
      download: false,
      viewColumns: false,
      selectableRowsOnClick: true,
      print: false,
      fixedHeader: true,
      sort: false,
      onRowClick: (rowData, rowMeta) => {
        var chosenObject = this.state.initialData.find(object => {
          return object.personId === rowData[0];
        });
        if (this.props.actionOnChosenData) {
          this.props.actionOnChosenData(chosenObject);
        } else {
          this.setState({
            chosenPersonId: chosenObject.personId,
            isLegalPerson: chosenObject.isLegalPerson
          });
        }
      }
    };

    var path = "";
    if (this.props.nextPath) {
      path = this.props.nextPath;
    } else {
      if (this.state.isLegalPerson) {
        path = "/legalPersonEmployee/";
      } else {
        path = "/naturalPersonEmployee/";
      }
    }

    if (this.state.chosenPersonId) {
      return <Redirect push to={path + this.state.chosenPersonId} />;
    }

    if (this.state.isLoading) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <MUIDataTable
          title={"Employee List"}
          data={data}
          columns={columns}
          options={options}
        />
      </div>
    );
  }
}

export default PersonTable;
