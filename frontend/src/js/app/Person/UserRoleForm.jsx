import React from "react";
import Button from "@material-ui/core/Button";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import LoadingPageIndicator from "../other/LoadingPageIndicator.jsx";
import PageHeader from "../other/PageHeader.jsx";
import EmployeeDetails from "./EmployeeDetails.jsx";
import UserRoleList from "./UserRoleList.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import gridStyle from "../../styles/GridStyle.module.css";

class UserRoleForm extends React.Component {
  state = {
    personId: "",
    name: "",
    employeeNo: "",
    position: "",
    workEmail: "",
    departmentId: "",
    isActive: true,
    employeeRoles: [],

    allRoles: [],
    allDepartments: [],
    fetchCounter: 0
  };

  componentDidMount() {
    this.getRoles();
    this.getEmployee(this.props.match.params.personId);
    this.getDepartments();
  }

  getRoles() {
    axios.get("/api/role").then(response => {
      if (response.data) {
        this.setState({
          allRoles: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  getEmployee = personId => {
    axios
      .get("/api/employee/" + personId)
      .then(response => {
        return response.data;
      })
      .then(this.adjustEmployeeFromDatabaseToState)
      .catch(error => {
        console.log(error);
      });
  };

  getDepartments() {
    axios.get("/api/department").then(response => {
      if (response.data) {
        this.setState({
          allDepartments: response.data,
          fetchCounter: this.state.fetchCounter + 1
        });
      }
    });
  }

  adjustEmployeeFromDatabaseToState = employee => {
    if (employee) {
      this.setState({
        personId: employee.personId,
        name: employee.name,
        employeeNo: employee.employeeNo,
        position: employee.position,
        workEmail: employee.workEmail,
        departmentId: employee.department.departmentId,
        isActive: employee.isActive,
        employeeRoles: employee.roles,
        fetchCounter: this.state.fetchCounter + 1
      });
    } else {
      console.log("Employee does not exist");
    }
  };

  adjustRolesFromStateToDatabase = () => {
    return this.state.employeeRoles;
  };

  handleEmployeeRolesChange = newEmployeeRoles => {
    this.setState({
      employeeRoles: newEmployeeRoles
    });
  };

  handleSubmit = event => {
    event.preventDefault();
    var roles = this.adjustRolesFromStateToDatabase();

    axios
      .put("/api/employee/" + this.state.personId + "/role", roles)
      .then(response => {
        this.props.addStandardSaveMessageDetails();
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    if (this.state.fetchCounter < 3) {
      return <LoadingPageIndicator />;
    }

    return (
      <div>
        <PageHeader title={this.props.title} />

        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
            className={gridStyle.gridWithElementsOnRow}
          >
            <Grid item>
              <EmployeeDetails
                isDisabled={true}
                employeeNo={this.state.employeeNo}
                name={this.state.name}
                isActive={this.state.isActive}
                position={this.state.position}
                workEmail={this.state.workEmail}
                departmentId={this.state.departmentId}
                departmentArray={this.state.allDepartments}
              />
            </Grid>
            <Grid item>
              <UserRoleList
                allRoles={this.state.allRoles}
                userRoles={this.state.employeeRoles}
                onUserRoleChange={this.handleEmployeeRolesChange}
              />
            </Grid>
          </Grid>

          <Button
            variant="contained"
            color="primary"
            className={textFieldStyle.textField}
            size="medium"
            type="submit"
          >
            Save
          </Button>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(UserRoleForm);
