import React from "react";
import TextField from "@material-ui/core/TextField";
import appValidator from "../AppValidator.js";
import PaperForm from "../other/PaperForm.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const ContactDetails = ({
  isLegalPerson,
  nationalNo,
  onNationalNoChange,
  taxNo,
  onTaxNoChange,
  name,
  onNameChange,
  phone,
  onPhoneChange,
  email,
  onEmailChange,
  hideNaturalNoAndTaxNo = false
}) => {
  let showNaturalNoTextField = isLegalPerson => {
    if (!isLegalPerson) {
      return (
        <TextField
          label="National number"
          className={textFieldStyle.textField}
          value={appValidator.replaceNull(nationalNo, "")}
          onChange={onNationalNoChange}
          margin="normal"
        />
      );
    }
  };

  return (
    <PaperForm className={paperStyle.standardPaper} title="Contact Details">
      {hideNaturalNoAndTaxNo ? (
        ""
      ) : (
        <React.Fragment>
          {showNaturalNoTextField(isLegalPerson)}

          <TextField
            required={isLegalPerson}
            error={
              isLegalPerson && appValidator.sayIfEmpty(taxNo) ? true : false
            }
            helperText={isLegalPerson && appValidator.sayIfEmpty(taxNo)}
            label="Tax number"
            className={textFieldStyle.textField}
            value={appValidator.replaceNull(taxNo, "")}
            onChange={onTaxNoChange}
            margin="normal"
          />

          <br />
          <br />
        </React.Fragment>
      )}

      <TextField
        required={true}
        error={appValidator.sayIfEmpty(name) ? true : false}
        helperText={appValidator.sayIfEmpty(name)}
        label="Name"
        className={textFieldStyle.longTextField}
        value={name}
        onChange={onNameChange}
        margin="normal"
      />

      <br />
      <br />

      <TextField
        label="Phone number"
        className={textFieldStyle.longTextField}
        value={appValidator.replaceNull(phone, "")}
        onChange={onPhoneChange}
        margin="normal"
      />

      <br />
      <br />

      <TextField
        error={appValidator.sayIfNotEmail(email) ? true : false}
        helperText={appValidator.sayIfNotEmail(email)}
        label="E-mail"
        className={textFieldStyle.longTextField}
        value={appValidator.replaceNull(email, "")}
        onChange={onEmailChange}
        margin="normal"
      />
    </PaperForm>
  );
};

export default ContactDetails;
