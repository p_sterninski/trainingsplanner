import React from "react";
import TextField from "@material-ui/core/TextField";
import appValidator from "../AppValidator.js";
import MenuItem from "@material-ui/core/MenuItem";
import AutocompleteScrollBar from "../other/AutocompleteScrollBar.jsx";
import PaperForm from "../other/PaperForm.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const SELECT_ITEM_LIST_HEIGHT = 400;
const SelectProps = {
  MenuProps: {
    PaperProps: {
      style: {
        maxHeight: SELECT_ITEM_LIST_HEIGHT
      }
    }
  }
};

const AddressDetails = ({
  address,
  onAddressChange,
  countryId,
  onCountryIdChange,
  countryArray,
  cityName,
  onCityNameChange,
  cityArray
}) => {
  return (
    <PaperForm className={paperStyle.standardPaper} title="Address Details">
      <TextField
        required={true}
        error={appValidator.sayIfEmpty(address) ? true : false}
        helperText={appValidator.sayIfEmpty(address)}
        label="Address"
        className={textFieldStyle.longTextField}
        value={address}
        onChange={onAddressChange}
        margin="normal"
      />
      <br />
      <br />

      <TextField
        select
        required={true}
        label="Country"
        className={textFieldStyle.longTextField}
        value={countryId}
        onChange={onCountryIdChange}
        margin="normal"
        SelectProps={SelectProps}
      >
        {countryArray.map(item => (
          <MenuItem key={item.countryId} value={item.countryId}>
            {item.name}
          </MenuItem>
        ))}
      </TextField>

      <br />
      <br />
      <br />

      <AutocompleteScrollBar
        array={cityArray}
        label="City"
        value={cityName}
        handleChange={onCityNameChange}
      />
    </PaperForm>
  );
};

export default AddressDetails;
