import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import PaperForm from "../other/PaperForm.jsx";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";
import paperStyle from "../../styles/PaperStyle.module.css";

const UserRoleList = ({ allRoles, userRoles, onUserRoleChange }) => {
  const ADMIN_ROLE_ID = 1;

  var allWithMarkedRoles = allRoles.map(role => {
    var isUsed = userRoles.some(userRole => {
      return userRole.roleId === role.roleId;
    });
    return [role, isUsed];
  });

  var isAdminActive = userRoles.some(userRole => {
    return userRole.roleId === ADMIN_ROLE_ID;
  });

  const handleRoleChange = index => event => {
    allWithMarkedRoles[index][1] = event.target.checked;
    var newEmployeeRoles = allWithMarkedRoles
      .filter(roleWithMarked => {
        if (allWithMarkedRoles[index][0].roleId === ADMIN_ROLE_ID) {
          return (
            roleWithMarked[0].roleId === ADMIN_ROLE_ID &&
            roleWithMarked[1] === true
          );
        } else {
          return (
            roleWithMarked[0].roleId !== ADMIN_ROLE_ID &&
            roleWithMarked[1] === true
          );
        }
      })
      .map(roleWithMarked => {
        return roleWithMarked[0];
      });

    return onUserRoleChange(newEmployeeRoles);
  };

  return (
    <PaperForm className={paperStyle.standardPaper} title="User Roles">
      {allWithMarkedRoles.map((roleWithMarked, index) => {
        return (
          <React.Fragment key={roleWithMarked[0].roleId}>
            <br />
            <FormControlLabel
              disabled={
                isAdminActive && roleWithMarked[0].roleId !== ADMIN_ROLE_ID
                  ? true
                  : false
              }
              control={
                <Checkbox
                  checked={roleWithMarked[1]}
                  onChange={handleRoleChange(index)}
                  value={roleWithMarked[0].name}
                  color="primary"
                />
              }
              label={roleWithMarked[0].name.substring(5)}
              className={textFieldStyle.textField}
            />
          </React.Fragment>
        );
      })}
    </PaperForm>
  );
};

export default UserRoleList;
