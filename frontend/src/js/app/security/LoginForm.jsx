import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import appValidator from "../AppValidator.js";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import PageHeader from "../other/PageHeader.jsx";
import AuthenticationProvider from "./AuthenticationProvider.js";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class LoginForm extends Component {
  state = {
    username: "",
    password: "",
    hasLoginFailed: false,
    showSuccessMessage: false
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.username) ||
      appValidator.isEmpty(this.state.password);

    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    AuthenticationProvider.authenticateUser(
      this.state.username,
      this.state.password
    )
      .then(() => {
        this.setState({ showSuccessMessage: true });
        this.setState({ hasLoginFailed: false });
        const sourceState = this.props.location.state || { path: "/" };
        this.props.history.replace(sourceState.path);
      })
      .catch(() => {
        this.setState({ showSuccessMessage: false });
        this.setState({ hasLoginFailed: true });
        this.props.addMessageDetails("Invalid Credentials", true);
      });
  };

  render() {
    if (
      this.state.showSuccessMessage === false &&
      this.state.hasLoginFailed === false &&
      AuthenticationProvider.isUserLoggedIn()
    ) {
      return (
        <div>
          <h2>User is already logged in</h2>
          <p>
            Probably you don't have sufficient rights to open that page. If you
            want to login with other credentials, please logout first. If this
            problem will happen again please contact administrator.
          </p>
        </div>
      );
    } else {
      return (
        <div>
          <PageHeader title={this.props.title} />

          <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item>
                <TextField
                  required
                  label="Username"
                  className={textFieldStyle.textField}
                  value={this.state.username}
                  onChange={this.handleChange("username")}
                  margin="normal"
                />

                <br />

                <TextField
                  required
                  label="Password"
                  className={textFieldStyle.textField}
                  value={this.state.password}
                  onChange={this.handleChange("password")}
                  type="password"
                  autoComplete="current-password"
                  margin="normal"
                />

                <br />
                <br />
                <br />

                <Button
                  disabled={this.isFormNotValid()}
                  variant="contained"
                  color="primary"
                  className={textFieldStyle.textField}
                  size="medium"
                  type="submit"
                >
                  Login
                </Button>

                <br />
                <br />

                <Link
                  className={textFieldStyle.textField}
                  to={"/login/forgotPassword"}
                >
                  Forgot your password?
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
      );
    }
  }
}

export default withConfirmationSnackbar(LoginForm);
