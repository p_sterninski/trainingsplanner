import axios from "axios";

const roleAssignment = {
  TRAINING: ["ROLE_ADMIN", "ROLE_TRAINING"],
  ACCOUNTING: ["ROLE_ADMIN", "ROLE_ACCOUNTING"],
  HR: ["ROLE_ADMIN", "ROLE_HR"],
  ADMIN: ["ROLE_ADMIN"],
  ALL: ["ROLE_ADMIN", "ROLE_TRAINING", "ROLE_ACCOUNTING", "ROLE_HR"]
};

const LOCAL_USERNAME = "loggedInUser";
const LOCAL_ROLE = "userRole";

class AuthenticationProvider {
  authenticateUser(username, password) {
    return axios
      .post("/api/login", "username=" + username + "&password=" + password)
      .then(response => {
        if (response.status === 200) {
          this.prepareAndSaveUser(response.data);
        } else {
          throw "wrong user";
        }
      });
  }

  logout() {
    return axios
      .post("/api/logout")
      .then(response => {
        if (response.status === 200) {
          this.deleteUserCredentials();
        }
      })
      .catch(error => {
        console.log("logout error");
        console.log(error.response);
      });
  }

  prepareAndSaveUser(currentUser) {
    var roles = currentUser.authorities.map(value => {
      return value.authority;
    });
    this.saveUserCredentials(currentUser.username, roles);
  }

  saveUserCredentials(username, roles) {
    localStorage.setItem(LOCAL_USERNAME, username);
    localStorage.setItem(LOCAL_ROLE, JSON.stringify(roles));
  }

  deleteUserCredentials() {
    localStorage.removeItem(LOCAL_USERNAME);
    localStorage.removeItem(LOCAL_ROLE);
  }

  isUserLoggedIn() {
    let username = localStorage.getItem(LOCAL_USERNAME);
    return username !== null;
  }

  getLoggedInUserName() {
    let username = localStorage.getItem(LOCAL_USERNAME);
    if (username !== null) {
      return username;
    }
    return "";
  }

  getLoggedInRoles() {
    let userRoleJson = localStorage.getItem(LOCAL_ROLE);
    if (userRoleJson !== null) {
      return JSON.parse(userRoleJson);
    }
    return [];
  }

  //only getter - roleAssignment cannot be changed
  get roleAssignment() {
    return roleAssignment;
  }

  isUserAuthorized(requiredRoles) {
    let authenticatedRole = this.getLoggedInRoles().find(value => {
      return requiredRoles.indexOf(value) > -1;
    });

    let isAuthorized = authenticatedRole !== undefined;

    return this.isUserLoggedIn() === true && isAuthorized === true;
  }
}

export default new AuthenticationProvider();
