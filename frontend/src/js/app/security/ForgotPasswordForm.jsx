import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import appValidator from "../AppValidator.js";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import PageHeader from "../other/PageHeader.jsx";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class ForgotPasswordForm extends Component {
  state = {
    employeeNo: ""
  };

  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  isFormNotValid = () => {
    var isNotValid = appValidator.isEmpty(this.state.employeeNo);
    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    axios
      .put("/api/employee/" + this.state.employeeNo + "/resetPassword")
      .finally(() => this.props.history.push("/login"));
  };

  render() {
    return (
      <div>
        <PageHeader title={this.props.title} />

        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item>
              <TextField
                required
                error={
                  appValidator.sayIfEmpty(this.state.employeeNo) ? true : false
                }
                helperText={appValidator.sayIfEmpty(this.state.employeeNo)}
                label="Type your Employee No."
                className={textFieldStyle.textField}
                value={this.state.employeeNo}
                onChange={this.handleChange("employeeNo")}
                margin="normal"
              />

              <br />
              <br />
              <br />

              <Button
                disabled={this.isFormNotValid()}
                variant="contained"
                color="primary"
                className={textFieldStyle.textField}
                size="medium"
                type="submit"
              >
                Reset Password
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(ForgotPasswordForm);
