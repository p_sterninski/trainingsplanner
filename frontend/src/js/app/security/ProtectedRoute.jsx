import React from "react";
import { Route, Redirect } from "react-router-dom";
import AuthenticationProvider from "./AuthenticationProvider.js";

const ProtectedRoute = ({
  component: Component,
  componentProps,
  requiredRoles,
  ...rest
}) => {
  return (
    <Route
      {...rest}
      render={props =>
        AuthenticationProvider.isUserAuthorized(requiredRoles) === true ? (
          <Component {...props} {...componentProps} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { path: props.location.pathname }
            }}
          />
        )
      }
    />
  );
};

export default ProtectedRoute;
