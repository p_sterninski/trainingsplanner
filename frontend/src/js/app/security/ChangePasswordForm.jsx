import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import appValidator from "../AppValidator.js";
import withConfirmationSnackbar from "../other/withConfirmationSnackbar.jsx";
import PageHeader from "../other/PageHeader.jsx";
import Grid from "@material-ui/core/Grid";
import axios from "axios";

import textFieldStyle from "../../styles/TextFieldStyle.module.css";

class ChangePasswordForm extends Component {
  state = {
    currentPassword: "",
    newPassword: "",
    newPasswordConfirmation: ""
  };

  isNewPasswordConfirmationCorrect = () => {
    return this.state.newPassword === this.state.newPasswordConfirmation;
  };
  handleChange = input => event => {
    this.setState({
      [input]: event.target.value
    });
  };

  isFormNotValid = () => {
    var isNotValid =
      appValidator.isEmpty(this.state.currentPassword) ||
      appValidator.isEmpty(this.state.newPassword) ||
      !this.isNewPasswordConfirmationCorrect();
    return isNotValid;
  };

  handleSubmit = event => {
    event.preventDefault();

    axios
      .put(
        "/api/employee/changePassword/" +
          this.state.currentPassword +
          "/" +
          this.state.newPassword
      )
      .then(response => {
        this.props.addMessageDetails("Password has been changed successfully");
      })
      .catch(this.props.addErrorMessageDetailsFromPromise);
  };

  render() {
    return (
      <div>
        <PageHeader title={this.props.title} />

        <form noValidate autoComplete="off" onSubmit={this.handleSubmit}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Grid item>
              <TextField
                required
                error={
                  appValidator.sayIfEmpty(this.state.currentPassword)
                    ? true
                    : false
                }
                helperText={appValidator.sayIfEmpty(this.state.currentPassword)}
                label="Current password"
                className={textFieldStyle.textField}
                value={this.state.currentPassword}
                onChange={this.handleChange("currentPassword")}
                type="password"
                autoComplete="current-password"
                margin="normal"
              />

              <br />
              <br />

              <TextField
                required
                error={
                  appValidator.sayIfEmpty(this.state.newPassword) ? true : false
                }
                helperText={appValidator.sayIfEmpty(this.state.newPassword)}
                label="New password"
                className={textFieldStyle.textField}
                value={this.state.newPassword}
                onChange={this.handleChange("newPassword")}
                type="password"
                autoComplete="current-password"
                margin="normal"
              />

              <br />

              <TextField
                required
                error={!this.isNewPasswordConfirmationCorrect() ? true : false}
                helperText={
                  !this.isNewPasswordConfirmationCorrect()
                    ? "Different than new password"
                    : ""
                }
                label="Confirm new password"
                className={textFieldStyle.textField}
                value={this.state.newPasswordConfirmation}
                onChange={this.handleChange("newPasswordConfirmation")}
                type="password"
                autoComplete="current-password"
                margin="normal"
              />

              <br />
              <br />
              <br />

              <Button
                disabled={this.isFormNotValid()}
                variant="contained"
                color="primary"
                className={textFieldStyle.textField}
                size="medium"
                type="submit"
              >
                Change Password
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    );
  }
}

export default withConfirmationSnackbar(ChangePasswordForm);
