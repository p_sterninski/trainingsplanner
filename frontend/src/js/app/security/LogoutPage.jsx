import React from "react";
import AuthenticationProvider from "./AuthenticationProvider.js";

class LogoutPage extends React.Component {
  constructor(props) {
    super(props);
    if (AuthenticationProvider.isUserLoggedIn()) {
      AuthenticationProvider.logout().then(() => {
        if (AuthenticationProvider.isUserLoggedIn() === false) {
          this.props.history.push("/login");
        }
      });
    } else {
      this.props.history.push("/login");
    }
  }

  render() {
    return <div></div>;
  }
}

export default LogoutPage;
