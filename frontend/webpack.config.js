var webpack = require("webpack");
var path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");

var BUILD_DIR = path.resolve("../src/main/resources/static");
//var BUILD_DIR = path.resolve(__dirname, "src/js/public");
var APP_DIR = path.resolve(__dirname, "src/js/app");
var TEMPLATE_DIR = path.resolve(__dirname, "src/js/public");

var config = {
  entry: APP_DIR + "/index.jsx",
  output: {
    path: BUILD_DIR,
    filename: "bundle.js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: APP_DIR,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.css$/,
        use: [
          "style-loader",
          { loader: "css-loader", options: { importLoaders: 1, modules: true } }
        ],
        include: /\.module\.css$/
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
        exclude: /\.module\.css$/
      }
    ]
  },
  devServer: {
    port: 50000,
    historyApiFallback: true,
    proxy: {
      "/api": {
        target: "http://localhost:8080"
      }
    }
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: path.resolve(TEMPLATE_DIR, "template", "index.html"),
      filename: BUILD_DIR + "/index.html"
    })
  ]
};

module.exports = config;
