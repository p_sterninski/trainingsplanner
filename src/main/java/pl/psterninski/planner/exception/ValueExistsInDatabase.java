package pl.psterninski.planner.exception;

public class ValueExistsInDatabase extends Exception {
	public ValueExistsInDatabase(String message) {
		super(message);
	}
}
