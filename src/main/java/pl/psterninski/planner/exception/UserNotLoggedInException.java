package pl.psterninski.planner.exception;

public class UserNotLoggedInException extends RuntimeException {
	public UserNotLoggedInException(String message) {
		super(message);
	}
	
	public UserNotLoggedInException() {
		super("User is not logged in");
	}
}
