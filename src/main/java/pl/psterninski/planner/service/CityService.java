package pl.psterninski.planner.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.psterninski.planner.dao.CityRepository;
import pl.psterninski.planner.dao.CountryRepository;
import pl.psterninski.planner.model.City;
import pl.psterninski.planner.model.Country;

@Service
public class CityService {

	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	

	public Iterable<City> findAll() {
		return cityRepository.findAll();
	}
	
	public City findById(Long id) {
		return cityRepository.findById(id).orElseThrow();
	}
	
	public Optional<City> findByNameIgnoreCaseAndCountryId (String cityName, Long countryId) {
		return cityRepository.findByNameIgnoreCaseAndCountryCountryId(cityName, countryId);
	}
		
	public City save(City newCity) {
		Long newCityCountryId = newCity.getCountry().getCountryId();
		Optional<City> city = cityRepository.findByNameIgnoreCaseAndCountryCountryId(newCity.getName(), newCityCountryId);
		if(city.isPresent()) {
			return city.orElseThrow();
		}
		Country country = countryRepository.findById(newCityCountryId).orElseThrow();
		newCity.setCountry(country);
		return cityRepository.save(newCity);
	}
}
