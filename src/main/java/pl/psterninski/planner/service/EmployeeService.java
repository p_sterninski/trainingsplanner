package pl.psterninski.planner.service;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import pl.psterninski.planner.dao.EmployeeRepository;
import pl.psterninski.planner.exception.InvalidCredentialsException;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Employee;
import pl.psterninski.planner.model.Person;
import pl.psterninski.planner.model.Role;
import pl.psterninski.planner.security.PasswordGenerator;

@Service
public class EmployeeService extends PersonService<Employee> {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private PersonService<Person> personService;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	
	public Employee findByEmployeeNo(String employeeNo) {
		return employeeRepository.findByEmployeeNo(employeeNo).orElseThrow(); 	//if Training not available throws NoSuchElementException
	}
	
	public Employee create(Employee employee) throws ValueExistsInDatabase {
		ifEmployeeNoExistsGetException(employee.getEmployeeNo());
		prepareAndValidateBeforeCreate(employee);
		return save(employee);
	}
	
	public Employee update(Long id, Employee employee) throws ValueExistsInDatabase {
		ifEmployeeNoExistsGetException(employee.getEmployeeNo(), id);
		prepareAndValidateBeforeUpdate(id, employee);

		//below is check if Employee exists in database
		//, if yes then values will be updated
		//, if not Employee attributes will be inserted for given personId and person data will be updated
		Employee savedEmployee = null;
		Optional<Employee> employeeFromDB = employeeRepository.findById(employee.getPersonId());
		if(employeeFromDB.isPresent()) {
			savedEmployee = save(Employee.copyEmployeeAndReplaceEmptyFields(employee, employeeFromDB.orElseThrow()));
		} else {
			employeeRepository.insertNewEmployeeForExisitingPerson(employee.getPersonId(), employee.getEmployeeNo(), employee.isActive(), employee.getPosition(), employee.getWorkEmail(), employee.getDepartment().getDepartmentId());
			personService.save(Person.copyPerson(employee));
			savedEmployee = employee;
		}
		return savedEmployee;
	}
	
	public Employee updatePersonalData(Long id, Person employeePersonalData) throws ValueExistsInDatabase {
		personService.prepareAndValidateBeforeUpdate(id, employeePersonalData);
		Employee employeeFromDB = employeeRepository.findById(id).orElseThrow();
		return save(Employee.copyEmployeeAndReplaceEmptyFields(employeePersonalData, employeeFromDB));
	}
	
	public Employee updateRoles(Long personId, Set<Role> newRoles) {
		Employee employee = findById(personId);
		if(employee.getWorkEmail() == null) {
			throw new NoSuchElementException("Email for employee " + employee.getName() + " (" + employee.getEmployeeNo() + " ) is not available");
		}
		
		employee.setRoles(newRoles);
		if (employee.getPassword() == null) {
			employee = resetAndSavePassword(10, employee);
			
		} else {
			employee = save(employee);
		}
		
		return employee;
	}
	
	public boolean forgotPassword(String employeeNo) {
		boolean isPasswordAdded = false;
		try{
			Employee employee = findByEmployeeNo(employeeNo);
			if (employee.getPassword() != null) {
				resetAndSavePassword(10, employee);
				isPasswordAdded = true;
			}
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Cannot reset password because employee no.: " + employeeNo + " doesn't exist");
		}
		
		return isPasswordAdded;
		
	}
	
	
	public Employee resetAndSavePassword(int passwordLength, Employee employee) {
		String newPassword = PasswordGenerator.generate(passwordLength);
		employee.setPassword(passwordEncoder.encode(newPassword));
		Employee employeeAfterSave = save(employee);
		String emailText = "Hi " + employee.getName() + ",\nyour new password for Trainings Planner is: " + newPassword;
		emailService.sendEmail(employee.getWorkEmail(), "Trainings Planner reset account", emailText);
		
		return employeeAfterSave;
	}
	
	public boolean changePassword(String employeeNo, String currentPasswordToConfirm, String newPassword) {
		boolean isPasswordChanged = false;
		try{
			Employee employee = findByEmployeeNo(employeeNo);
			if(passwordEncoder.matches(currentPasswordToConfirm, employee.getPassword())) {
				employee.setPassword(passwordEncoder.encode(newPassword));
				save(employee);
				isPasswordChanged = true;
			} else {
				throw new InvalidCredentialsException();
			}
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Cannot change password because employee no.: " + employeeNo + " doesn't exist");
		}
		return isPasswordChanged;
	}

//private
	private boolean ifEmployeeNoExistsGetException(String employeeNo, Long exceptionEmployeeId) throws ValueExistsInDatabase {
		Optional<Employee> employee = employeeRepository.findByEmployeeNo(employeeNo);
		if(employee.isPresent() && employee.get().getPersonId() != exceptionEmployeeId) {
			throw new ValueExistsInDatabase("Employee with employee no. " +  employeeNo + " exists in database");
		}

		return false;
	}
	
	
	private boolean ifEmployeeNoExistsGetException(String employeeNo) throws ValueExistsInDatabase {
		return ifEmployeeNoExistsGetException(employeeNo, null);
	}
}
