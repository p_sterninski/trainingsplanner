package pl.psterninski.planner.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.psterninski.planner.dao.InvoiceItemRepository;
import pl.psterninski.planner.dao.InvoiceRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Invoice;
import pl.psterninski.planner.model.InvoiceItem;

@Service
public class InvoiceService {

	@Autowired
	public InvoiceRepository invoiceRepository;

	@Autowired
	public InvoiceItemRepository invoiceItemRepository;
	

	public Iterable<Invoice> findAll() {
		return invoiceRepository.findAll();
	}
	
	public Invoice findById(Long id) {
		return invoiceRepository.findById(id).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
	public Invoice findByInvoiceNo(String invoiceNo) {
		return invoiceRepository.findByInvoiceNo(invoiceNo).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
	private Invoice prepareInvoice(Invoice invoice) {
		BigDecimal value = new BigDecimal(0);
		for(InvoiceItem item : invoice.getItems()) {
			item.setInvoice(invoice);
			value = value.add(item.getValue());
		}
		invoice.setValue(value);
		return invoice;
	}
	
	private Invoice prepareInvoice(Invoice invoice, Invoice initialInvoice) {
		invoice.setInitialInvoice(initialInvoice);
		return prepareInvoice(invoice);
	}
	
	private Invoice prepareCancellationInvoice(Invoice previousInvoice, String cancellationInvoiceNo, boolean cancellationInvoiceIsLast, boolean cancellationInvoiceIsCancelled) {
		Invoice cancellationInvoice = new Invoice();
		cancellationInvoice.setInvoiceId(null);
		cancellationInvoice.setInvoiceNo(cancellationInvoiceNo);
		cancellationInvoice.setValue(previousInvoice.getValue().negate());
		cancellationInvoice.setSalesDate(previousInvoice.getSalesDate());
		cancellationInvoice.setInvoiceDate(previousInvoice.getInvoiceDate());
		cancellationInvoice.setCostInvoice(previousInvoice.isCostInvoice());
		cancellationInvoice.setCorrection(true);
		cancellationInvoice.setCancelled(cancellationInvoiceIsCancelled);
		cancellationInvoice.setLast(cancellationInvoiceIsLast);
		cancellationInvoice.setInitialInvoice(getInitialInvoice(previousInvoice));
		cancellationInvoice.setPerson(previousInvoice.getPerson());
		for(InvoiceItem previousItem : previousInvoice.getItems()) {
			InvoiceItem cancellationItem = new InvoiceItem();
			cancellationItem.setItemId(null);
			cancellationItem.setValue(previousItem.getValue().negate());
			cancellationItem.setAmount(-previousItem.getAmount());
			cancellationItem.setDescription(previousItem.getDescription());
			cancellationItem.setTraining(previousItem.getTraining());
			cancellationItem.setInvoice(cancellationInvoice);
			cancellationInvoice.getItems().add(cancellationItem);
		}
		return cancellationInvoice;
	}
	
	
	private Invoice saveWithItems(Invoice invoice) throws ValueExistsInDatabase {
		if(invoiceRepository.findByInvoiceNo(invoice.getInvoiceNo()).isPresent()) {
			throw new ValueExistsInDatabase("Invoice " + invoice.getInvoiceNo() + " exists in database");
		}
		Invoice newInvoice = invoiceRepository.save(invoice);
		invoiceItemRepository.saveAll(newInvoice.getItems());
		return newInvoice;
	}
	
	
	public Invoice createInvoice(Invoice invoice) throws ValueExistsInDatabase {
		Invoice newInvoice = prepareInvoice(invoice);
		Invoice newInvoiceAfterSave = saveWithItems(newInvoice);
		return newInvoiceAfterSave;
	}
	
	public Invoice cancelInvoice(Long previousInvoiceId, String cancellationInvoiceNo) throws ValueExistsInDatabase {
		Invoice previousInvoice = invoiceRepository.findById(previousInvoiceId).orElseThrow(); //if Invoice not found throws NoSuchElementException
		previousInvoice.setLast(false);
		Invoice cancellationInvoice = prepareCancellationInvoice(previousInvoice, cancellationInvoiceNo, true, true);
		cancellationInvoice = saveWithItems(cancellationInvoice);
		invoiceRepository.save(previousInvoice); //update isLast property
		return cancellationInvoice;
		
	}
	
	public Invoice cancelOldAndCreateNewInvoice(Long previousInvoiceId, Invoice invoice) throws ValueExistsInDatabase {
		Invoice previousInvoice = invoiceRepository.findById(previousInvoiceId).orElseThrow(); //if Invoice not found throws NoSuchElementException
		Invoice initialInvoice = getInitialInvoice(previousInvoice);
		previousInvoice.setLast(false);
		Invoice cancellationInvoice = prepareCancellationInvoice(previousInvoice, "C" + previousInvoice.getInvoiceNo(), false, false);
		Invoice correctiveInvoice = prepareInvoice(invoice, initialInvoice);
		Invoice correctiveInvoiceAfterSave = saveWithItems(correctiveInvoice);
		saveWithItems(cancellationInvoice);
		invoiceRepository.save(previousInvoice); //update isLast property 
		return correctiveInvoiceAfterSave;
		
	}
	
	private Invoice getInitialInvoice(Invoice invoice) {
		Invoice initialInvoice = null;
		if(invoice.getInitialInvoice() == null) {
			initialInvoice = invoice;
		} else {
			initialInvoice = invoice.getInitialInvoice();
		}
		
		return initialInvoice;
	}
	
	
	
	
}
