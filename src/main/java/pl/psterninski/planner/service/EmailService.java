package pl.psterninski.planner.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    
	@Autowired
    private JavaMailSender javaMailSender;
	
	private final String senderEmail = "trainings.planner.test@o2.pl";
	
	public void sendEmail(String recipient, String subject, String textContent) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setTo(recipient); 
        message.setSubject(subject); 
        message.setText(textContent);
        message.setFrom(this.getSenderEmail());
        javaMailSender.send(message);
	}
	
	public String getSenderEmail() {
		return senderEmail;
	}
	
}
