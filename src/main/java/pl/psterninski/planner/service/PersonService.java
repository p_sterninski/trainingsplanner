package pl.psterninski.planner.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.psterninski.planner.dao.PersonRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.City;
import pl.psterninski.planner.model.Person;

@Service
public class PersonService<T extends Person> {

	@Autowired
	private PersonRepository<T> personRepository;
	
	@Autowired
	private PersonRepository<Person> onlyPersonRepository;
	
	@Autowired
	private CityService cityService;
	
	
	public Iterable<T> findAll() {
		return personRepository.findAll();
	}
	
	public T findById(Long id) {
		return personRepository.findById(id).orElseThrow();
	}
	
	public T findByTaxNo(String taxNo) {
		return personRepository.findByTaxNo(taxNo).orElseThrow();
	}
	
	public T findByNationalNo(String nationalNo) {
		return personRepository.findByNationalNo(nationalNo).orElseThrow();
	}
	
	public Iterable<T> findByIsLegalPerson(boolean isLegalPerson){
		return personRepository.findByIsLegalPerson(isLegalPerson);
	}
	
	public T findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(String name, String address, Long cityId) {
		return personRepository.findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(name, address, cityId).orElseThrow();
	}
	
	public boolean existsById(Long id) {
		return personRepository.existsById(id);
	}
	
	
	public T create(T person) throws ValueExistsInDatabase {
		prepareAndValidateBeforeCreate(person);
		return save(person);
	}
	
	public T update(Long id, T person) throws ValueExistsInDatabase {
		prepareAndValidateBeforeUpdate(id, person);
		return save(person);
	}

	
//private
	private boolean ifTaxNoExistsGetException(String taxNo, Long exceptionPersonId) throws ValueExistsInDatabase {
		if(taxNo != null) {
			Optional<Person> person = onlyPersonRepository.findByTaxNo(taxNo);
			if(person.isPresent() && person.get().getPersonId() != exceptionPersonId) {
				throw new ValueExistsInDatabase("Person with tax no. " +  taxNo + " exists in database");
			}
		}
		return false;
	}
	
	
	private boolean ifTaxNoExistsGetException(String taxNo) throws ValueExistsInDatabase {
		return ifTaxNoExistsGetException(taxNo, null);
	}
	
	
	private boolean ifNationalNoExistsGetException(String nationalNo, Long exceptionPersonId) throws ValueExistsInDatabase {
		if(nationalNo != null) {
			Optional<Person> person = onlyPersonRepository.findByNationalNo(nationalNo);
			if(person.isPresent() && person.get().getPersonId() != exceptionPersonId) {
				throw new ValueExistsInDatabase("Person with national no. " +  nationalNo + " exists in database");
			}
		}
		return false;
	}
	
	private boolean ifNationalNoExistsGetException(String nationalNo) throws ValueExistsInDatabase {
		return ifNationalNoExistsGetException(nationalNo, null);
	}
	
	private boolean ifNameAndAddressAndCityExistsGetException(String name, String address, City city, Long exceptionPersonId) throws ValueExistsInDatabase {
		Optional<Person> person = onlyPersonRepository.findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(name, address, city.getCityId());
		if(person.isPresent() && person.get().getPersonId() != exceptionPersonId) {
			throw new ValueExistsInDatabase("Person " + name + ", " + address + ", " + city.getName() + " exists in database");
		}
		return false;
	}
	
	private boolean ifNameAndAddressAndCityExistsGetException(String name, String address, City city) throws ValueExistsInDatabase {
		return ifNameAndAddressAndCityExistsGetException(name, address, city, null);
	}
	
	private T preparePerson(T person) {
		person.changeBlankStringToNullForNullableColumn();
		
		Optional<City> cityFromDatabase = cityService.findByNameIgnoreCaseAndCountryId(person.getCity().getName(), person.getCity().getCountry().getCountryId());
		if(cityFromDatabase.isPresent()) {
			person.setCity(cityFromDatabase.get());
		}
		
		return person;
	}
	

//protected
	//outside Service class use create() or update() instead (EmployeeService has additional forms of update())
	protected T save(T person) {
		if(person.getCity().getCityId() == null) {
			City city = cityService.save(person.getCity());
			person.setCity(city);
		}
		return personRepository.save(person);
	}
	
	protected T prepareAndValidateBeforeCreate(T person) throws ValueExistsInDatabase {
		preparePerson(person);
		
		ifTaxNoExistsGetException(person.getTaxNo());
		ifNationalNoExistsGetException(person.getNationalNo());
		ifNameAndAddressAndCityExistsGetException(person.getName(), person.getAddress(), person.getCity());
		
		return person;
	}
	
	
	protected T prepareAndValidateBeforeUpdate(Long id, T person) throws ValueExistsInDatabase {
		preparePerson(person);
		Optional<T> currentPerson = personRepository.findById(id);
		if(currentPerson.isPresent()) {
			if(currentPerson.orElseThrow().equals(person)) {
				throw new ValueExistsInDatabase("Person exists in database");
			}
		}
		
		ifTaxNoExistsGetException(person.getTaxNo(), id);
		ifNationalNoExistsGetException(person.getNationalNo(), id);
		ifNameAndAddressAndCityExistsGetException(person.getName(), person.getAddress(), person.getCity(), id);
		
		return person;
	}
	
}
