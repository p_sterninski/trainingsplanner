package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.RoleRepository;
import pl.psterninski.planner.model.Role;

@RestController
@RequestMapping("/api/role")
public class RoleController {
	
	@Autowired
	RoleRepository roleRepository;
	
//GET
	@GetMapping
	public Iterable<Role> findAll() {
		return roleRepository.findAll();
	}
}
