package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.LocationRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.City;
import pl.psterninski.planner.model.Location;
import pl.psterninski.planner.service.CityService;

@RestController
@RequestMapping("/api/location")
public class LocationController {

	@Autowired
	private LocationRepository locationRepository;
	
	@Autowired
	private CityService cityService;
	
	
//GET
	@GetMapping
	public Iterable<Location> findAll() {
		return locationRepository.findAll();
	}

	@GetMapping("/{id}")
	public Location findById(@PathVariable Long id) {
		return locationRepository.findById(id).orElseThrow(); //if Location not available throws NoSuchElementException
	}
	
//POST
	@PostMapping
	public Location create(@RequestBody Location location) throws ValueExistsInDatabase {
		City city = cityService.save(location.getCity());
		location.setCity(city);
		if(locationRepository
				.findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(location.getName(), location.getAddress(), location.getCity().getCityId())
				.isPresent()) 
		{
			throw new ValueExistsInDatabase("Location " 
											+ location.getName() + ", " + location.getAddress() + ", " + location.getCity().getName() 
											+ " exists in database");
		}
		return locationRepository.save(location);
	}

//PUT
	@PutMapping("/{id}")
	public Location update(@PathVariable Long id, @RequestBody Location location) throws ValueExistsInDatabase {
		City city = cityService.save(location.getCity());
		location.setCity(city);
		if(locationRepository
				.findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(location.getName(), location.getAddress(), location.getCity().getCityId())
				.isPresent()) 
		{
			throw new ValueExistsInDatabase("Location " 
											+ location.getName() + ", " + location.getAddress() + ", " + location.getCity().getName() 
											+ " exists in database");
		}
		return locationRepository.save(location);
	}
}
