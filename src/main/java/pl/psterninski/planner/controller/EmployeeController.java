package pl.psterninski.planner.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.EmployeeRepository;
import pl.psterninski.planner.exception.UserNotLoggedInException;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Employee;
import pl.psterninski.planner.model.Person;
import pl.psterninski.planner.model.Role;
import pl.psterninski.planner.security.UserDetailsImp;
import pl.psterninski.planner.service.EmployeeService;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployeeService employeeService;

	
	
//GET
	@GetMapping
	public Iterable<Employee> findAll() {
		return employeeRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Person findById(@PathVariable Long id) {
		return employeeRepository.findById(id).orElseThrow(); 	//if Employee not available throws NoSuchElementException
	}
	
	@GetMapping("/{employeeNo}/role")
	public String[] getRole(@PathVariable String employeeNo) {
		Set<Role> roles = employeeRepository.findByEmployeeNo(employeeNo).orElseThrow().getRoles(); //if Employee not available throws NoSuchElementException
		return roles.stream()
					.map(role -> role.getName())
					.toArray(size -> new String[size]);
	}
	
	//Available for Training users but they shouldn't be able to see other Employees
	@GetMapping("/onlyTrainingAndActive") 
	public Iterable<Employee> findOnlyTrainingAndActive() {
		return employeeRepository.findByIsActiveAndDepartmentDepartmentId(true, 1L);
	}
	
	@GetMapping("/isActive/{isActive}/department/{departmentId}")
	public Iterable<Employee> findByIsActiveAndDepartmentId(@PathVariable boolean isActive, @PathVariable Long departmentId) {
		return employeeRepository.findByIsActiveAndDepartmentDepartmentId(isActive, departmentId);
	}	

//POST
	@PostMapping
	public Employee create(@RequestBody Employee employee) throws ValueExistsInDatabase {
		return employeeService.create(employee);
	}

//PUT
	@PutMapping("/{id}")
	public Employee update(@PathVariable Long id, @RequestBody Employee employee) throws ValueExistsInDatabase {
		return employeeService.update(id, employee);
	}
	
	@PutMapping("/{personId}/role")
	public Employee roleUpdate(@PathVariable Long personId, @RequestBody Set<Role> newRoles) throws ValueExistsInDatabase {
		return employeeService.updateRoles(personId, newRoles);
	}
	
	@PutMapping("/{employeeNo}/resetPassword")
	public boolean roleUpdate(@PathVariable String employeeNo) {
		return employeeService.forgotPassword(employeeNo);
	}
	
	@PutMapping("/changePassword/{currentPassword}/{newPassword}")
	public boolean roleUpdate(@PathVariable String currentPassword, @PathVariable String newPassword) {
		String currentUsername = UserDetailsImp.getCurrentUserDetails().orElseThrow(UserNotLoggedInException::new).getUsername();
		return employeeService.changePassword(currentUsername, currentPassword, newPassword);
	}
}
