package pl.psterninski.planner.controller;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.TrainingRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Training;
import pl.psterninski.planner.security.UserDetailsImp;

@RestController
@RequestMapping("/api/training")
public class TrainingController {

	@Autowired
	private TrainingRepository trainingRepository; 
	
//GET
	@GetMapping
	public Iterable<Training> findAll() {
		return trainingRepository.findAll();
	}
	
	@GetMapping("/currentUser")
	public Iterable<Training> findByUsername() {
		UserDetails currentUser = UserDetailsImp.getCurrentUserDetails().orElse(null);
		if(currentUser != null) {
			Set<String> userRoleName = currentUser.getAuthorities()
																.stream()
																.map(GrantedAuthority::getAuthority)
																.collect(Collectors.toSet());
			if(userRoleName.contains("ROLE_ADMIN")) {
				return trainingRepository.findAll();
			} else {
				return trainingRepository.findByEmployeesEmployeeNo(currentUser.getUsername());
			}
		} else {
			return null;
		}
	}
	
	@GetMapping("/{id}")
	public Training findById(@PathVariable Long id) {
		return trainingRepository.findById(id).orElseThrow(); 		//if Training not available throws NoSuchElementException
	}
	
	@GetMapping("/code/{code}")
	public Training findByCode(@PathVariable String code) {
		return trainingRepository.findByCodeIgnoreCase(code).orElseThrow(); 	//if Training not available throws NoSuchElementException
	}
	
	
//POST
	@PostMapping
	public Training create(@RequestBody Training training) throws ValueExistsInDatabase {
		if(trainingRepository.findByCodeIgnoreCase(training.getCode()).isPresent()) {
			throw new ValueExistsInDatabase("Training " + training.getCode() + " exists in database");
		}
		return trainingRepository.save(training);
	}
	
//PUT
	@PutMapping("/{id}")
	public Training update(@PathVariable Long id, @RequestBody Training training) throws ValueExistsInDatabase {
		Training trainingFromDatabase = null;
		try {
			trainingFromDatabase = trainingRepository.findById(training.getTrainingId()).orElseThrow();
		} catch (NoSuchElementException e) {
			throw new NoSuchElementException("Cannot find training Id: " + training.getTrainingId());
		}
		
		Optional<Training> trainingFromDatabaseByCode = null; 
		trainingFromDatabaseByCode = trainingRepository.findByCodeIgnoreCase(training.getCode());
		if(trainingFromDatabaseByCode.isPresent() && (trainingFromDatabaseByCode.get().getTrainingId() != training.getTrainingId())) {
			throw new ValueExistsInDatabase("Training code " + trainingFromDatabase.getCode() + " exists in database");
		} 

		training.setStartDate(trainingFromDatabase.getStartDate());
		training.setEndDate(trainingFromDatabase.getEndDate());
		return trainingRepository.save(training);
			
		
		
			
	}
}
