package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.DepartmentRepository;
import pl.psterninski.planner.model.Department;

@RestController
@RequestMapping("/api/department")
public class DepartmentController {

	@Autowired
	public DepartmentRepository departmentRepository;
	
//GET
	@GetMapping
	public Iterable<Department> findAll() {
		return departmentRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Department findById(@PathVariable Long id) {
		return departmentRepository.findById(id).orElseThrow();	//if Training not available throws NoSuchElementException
	}

//POST
	@PostMapping
	public Department create(@RequestBody Department department) {
		return departmentRepository.save(department);
	}
}
