package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Person;
import pl.psterninski.planner.service.EmployeeService;
import pl.psterninski.planner.service.PersonService;


@RestController
@RequestMapping("/api/person")
public class PersonController {
	
	@Autowired
	private PersonService<Person> personService;
	
	@Autowired
	private EmployeeService employeeService;
	
	
//GET
	@GetMapping
	public Iterable<? extends Person> findAll() {
		return personService.findAll();
	}
	

	@GetMapping("/legalPerson")
	public Iterable<Person> findOnlyLegalPerson() {
		return personService.findByIsLegalPerson(true);
	}
	
	@GetMapping("/{id}")
	public Person findById(@PathVariable Long id) {
		return personService.findById(id); 	//if Training not available throws NoSuchElementException
	}
	
	@GetMapping("/taxNo/{taxNo}")
	public Person findByTaxNo(@PathVariable String taxNo) {
		return personService.findByTaxNo(taxNo); 	//if Training not available throws NoSuchElementException
	}
	
//POST
	@PostMapping
	public Person create(@RequestBody Person person) throws ValueExistsInDatabase {
		return personService.create(person);
	}
	
//PUT
	@PutMapping("/{id}")
	public Person update(@PathVariable Long id, @RequestBody Person person) throws ValueExistsInDatabase {
		if(employeeService.existsById(person.getPersonId())) {
			return employeeService.updatePersonalData(id, person);
		}
		return personService.update(id, person);
	}
	
}
