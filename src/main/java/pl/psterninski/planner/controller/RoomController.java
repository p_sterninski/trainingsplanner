package pl.psterninski.planner.controller;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.RoomRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Room;

@RestController
@RequestMapping("/api/room")
public class RoomController {

	@Autowired
	private RoomRepository roomRepository; 
	
//GET
	@GetMapping
	public Iterable<Room> findAll() {
		return roomRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Room findById(@PathVariable Long id) {
		return roomRepository.findById(id).orElseThrow(); 		//if Training not available throws NoSuchElementException
	}
	

	@GetMapping("location/{locationId}")
	public Iterable<Room> findByLocationLocationId(@PathVariable Long locationId) {
		return roomRepository.findByLocationLocationId(locationId);
	}

//POST
	@PostMapping
	public Room create(@RequestBody Room room) throws ValueExistsInDatabase {
		Room roomFromDatabase = null;
		
		try {
			roomFromDatabase = roomRepository.findByRoomNoIgnoreCaseAndLocationLocationId(room.getRoomNo(), room.getLocation().getLocationId()).orElseThrow();
		} catch (NoSuchElementException e) {
			return roomRepository.save(room);
		}
		
		throw new ValueExistsInDatabase("Room " + roomFromDatabase.getRoomNo() + " (" + roomFromDatabase.getLocation().getName() + ") exists in database");		
	}
	
//PUT
	@PutMapping("/{id}")
	public Room update(@PathVariable Long id, @RequestBody Room room) throws ValueExistsInDatabase {
		Room roomFromDatabase = null;
		
		try {
			roomFromDatabase = roomRepository.findByRoomNoIgnoreCaseAndLocationLocationId(room.getRoomNo(), room.getLocation().getLocationId()).orElseThrow();
		} catch (NoSuchElementException e) {
			return roomRepository.save(room);
		}
		
		throw new ValueExistsInDatabase("Room " + roomFromDatabase.getRoomNo() + " (" + roomFromDatabase.getLocation().getName() + ") exists in database");	
	}
}
