package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.CountryRepository;
import pl.psterninski.planner.model.City;
import pl.psterninski.planner.model.Country;

@RestController
@RequestMapping("/api/country")
public class CountryController {

	@Autowired CountryRepository countryRepository;
	
//GET
	@GetMapping
	public Iterable<Country> findAll() {
		return countryRepository.findAll();
	}
	
	@GetMapping("/cities/{id}")
	public Iterable<City> findById(@PathVariable Long id) {
		Country country = countryRepository.findById(id).orElseThrow();	//if Country not available throws NoSuchElementException
		return country.getCities();
	}
	
//POST	
	@PostMapping
	public Country create(@RequestBody Country country) {
		return countryRepository.save(country);
	}
}
