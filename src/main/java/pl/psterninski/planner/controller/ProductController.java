package pl.psterninski.planner.controller;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.ProductRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Product;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	@Autowired
	private ProductRepository productRepository;
	
	
//GET
	@GetMapping
	public Iterable<Product> findAll() {
		return productRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Product findById(@PathVariable Long id) {
		return productRepository.findById(id).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
	@GetMapping("/name/{name}")
	public Product findByName(@PathVariable String name) {
		return productRepository.findByNameIgnoreCase(name).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
//POST
	@PostMapping
	public Product create(@RequestBody Product product) throws ValueExistsInDatabase {
		Product productFromDatabase = null;
		
		try {
			productFromDatabase = productRepository.findByNameIgnoreCase(product.getName()).orElseThrow();
		} catch (NoSuchElementException e) {
			return productRepository.save(product);
		}
		
		throw new ValueExistsInDatabase("Product " + productFromDatabase.getName() + " exists in database");		
	}
	
//PUT
	@PutMapping("/{id}")
	public Product update(@PathVariable Long id, @RequestBody Product product) throws ValueExistsInDatabase {
		Product productFromDatabase = null;
		
		try {
			productFromDatabase = productRepository.findByNameIgnoreCase(product.getName()).orElseThrow();
		} catch (NoSuchElementException e) {
			return productRepository.save(product);
		}
		
		throw new ValueExistsInDatabase("Product " + productFromDatabase.getName() + " exists in database");	
	}
}
