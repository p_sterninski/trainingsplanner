package pl.psterninski.planner.controller;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.TrainingCategoryRepository;
import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.TrainingCategory;

@RestController
@RequestMapping("/api/trainingCategory")
public class TrainingCategoryController {

	@Autowired
	public TrainingCategoryRepository trainingCategoryRepository;
	
//GET
	@GetMapping
	public Iterable<TrainingCategory> findAll() {
		return trainingCategoryRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public TrainingCategory findById(@PathVariable Long id) {
		return trainingCategoryRepository.findById(id).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
	@GetMapping("/name/{name}")
	public TrainingCategory findByNameIgnoreCase(@PathVariable String name) {
		return trainingCategoryRepository.findByNameIgnoreCase(name).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
//POST
	@PostMapping
	public TrainingCategory create(@RequestBody TrainingCategory trainingCategory) throws ValueExistsInDatabase {
		TrainingCategory trainingCategoryFromDatabase = null;
		
		try {
			trainingCategoryFromDatabase = trainingCategoryRepository.findByNameIgnoreCase(trainingCategory.getName()).orElseThrow();
		} catch (NoSuchElementException e) {
			trainingCategory.setName(trainingCategory.getName().toUpperCase());
			return trainingCategoryRepository.save(trainingCategory);
		}
		
		throw new ValueExistsInDatabase("Category " + trainingCategoryFromDatabase.getName() + " exists in database");		
	}

//PUT
	@PutMapping("/{id}")
	public TrainingCategory update(@PathVariable Long id, @RequestBody TrainingCategory trainingCategory) throws ValueExistsInDatabase {
		TrainingCategory trainingCategoryFromDatabase = null;
		
		try {
			trainingCategoryFromDatabase = trainingCategoryRepository.findByNameIgnoreCase(trainingCategory.getName()).orElseThrow();
		} catch (NoSuchElementException e) {
			trainingCategory.setName(trainingCategory.getName().toUpperCase());
			return trainingCategoryRepository.save(trainingCategory);
		}
		
		throw new ValueExistsInDatabase("Category " + trainingCategoryFromDatabase.getName() + " exists in database");	
	}
}
