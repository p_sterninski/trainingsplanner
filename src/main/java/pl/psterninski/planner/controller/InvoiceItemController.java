package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.InvoiceItemRepository;
import pl.psterninski.planner.model.InvoiceItem;

@RestController
@RequestMapping("/api/invoiceItem")
public class InvoiceItemController {

	@Autowired
	public InvoiceItemRepository invoiceItemRepository;
	
//GET
	@GetMapping
	public Iterable<InvoiceItem> findAll() {
		return invoiceItemRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public InvoiceItem findById(@PathVariable Long id) {
		return invoiceItemRepository.findById(id).orElseThrow();	//if Training not available throws NoSuchElementException
	}
	
//POST
	@PostMapping
	public InvoiceItem create(InvoiceItem costInvoice) {
		return invoiceItemRepository.save(costInvoice);
	}
}
