package pl.psterninski.planner.controller;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.GiveawayTaskRepository;
import pl.psterninski.planner.dao.LecturerTaskRepository;
import pl.psterninski.planner.dao.LocationTaskRepository;
import pl.psterninski.planner.dao.TrainingRepository;
import pl.psterninski.planner.model.GiveawayTask;
import pl.psterninski.planner.model.LecturerTask;
import pl.psterninski.planner.model.LocationTask;
import pl.psterninski.planner.model.Training;

@RestController
@RequestMapping("/api/locationTask")
public class LocationTaskController {

	@Autowired
	private LocationTaskRepository locationTaskRepository;
	
	@Autowired
	private LecturerTaskRepository lecturerTaskRepository;
	
	@Autowired
	private GiveawayTaskRepository giveawayTaskRepository;
	
	@Autowired
	private TrainingRepository trainingRepository;
	
	
//GET
	@GetMapping
	public Iterable<LocationTask> findAll() {
		return locationTaskRepository.findAll();
	}

	@GetMapping("/{id}")
	public LocationTask findById(@PathVariable Long id) {
		return locationTaskRepository.findById(id).orElseThrow(); 		//if PlaceTask not available throws NoSuchElementException
	}
	
//POST
	@PostMapping
	public LocationTask create(@RequestBody LocationTask locationTask) {
		LocationTask newLocationTask = locationTaskRepository.save(locationTask);
		
		//set locationTask to lecturerTasks
		for(LecturerTask lecturerTask : locationTask.getLecturerTasks()) {
			lecturerTask.setLocationTask(newLocationTask);
		}
		lecturerTaskRepository.saveAll(locationTask.getLecturerTasks());
		
		//set locationTask to giveawayTasks
		for(GiveawayTask giveawayTask : locationTask.getGiveawayTasks()) {
			giveawayTask.setLocationTask(newLocationTask);
		}
		giveawayTaskRepository.saveAll(locationTask.getGiveawayTasks());
		
		updateTrainingDateAfterSave(locationTask);
		
		return newLocationTask;
	}
	
//PUT
	@PutMapping("/{id}")
	public LocationTask update(@PathVariable Long id, @RequestBody LocationTask locationTask) {
		LocationTask oldLocationTask = findById(id);
		boolean toDelete = true;
		
		//delete not used lecturerTasks
		for(LecturerTask oldLecturerTask : oldLocationTask.getLecturerTasks()) {
			for(LecturerTask lecturerTask : locationTask.getLecturerTasks()) {
				if(lecturerTask.getTaskId() != null && lecturerTask.getTaskId().equals(oldLecturerTask.getTaskId())) {
					toDelete = false;
				}
			}
			if(toDelete == true) {
				lecturerTaskRepository.delete(oldLecturerTask);
			} else {
				toDelete = true;
			}
		}
		
		//set locationTask to updated lecturerTasks
		for(LecturerTask lecturerTask : locationTask.getLecturerTasks()) {
			lecturerTask.setLocationTask(locationTask);
		}
		lecturerTaskRepository.saveAll(locationTask.getLecturerTasks());
		
		
		//delete not used giveawayTasks
		for(GiveawayTask oldGiveawayTask : oldLocationTask.getGiveawayTasks()) {
			for(GiveawayTask giveawayTask : locationTask.getGiveawayTasks()) {
				if(giveawayTask.getTaskId() != null && giveawayTask.getTaskId().equals(oldGiveawayTask.getTaskId())) {
					toDelete = false;
				}
			}
			if(toDelete == true) {
				giveawayTaskRepository.delete(oldGiveawayTask);
			} else {
				toDelete = true;
			}
		}
		
		//set locationTask to updated giveawayTasks
		for(GiveawayTask giveawayTask : locationTask.getGiveawayTasks()) {
			giveawayTask.setLocationTask(locationTask);
		}
		giveawayTaskRepository.saveAll(locationTask.getGiveawayTasks());
		
		updateTrainingDateAfterSave(locationTask);
		
		return locationTaskRepository.save(locationTask);
	}
	
//DELETE
	@DeleteMapping("/{id}")
	public boolean delete(@PathVariable Long id) {
		Optional<LocationTask> locationTaskOptional = locationTaskRepository.findById(id);

		if(locationTaskOptional.isEmpty()) {
			return false;
		}
		
		LocationTask locationTask = locationTaskOptional.get();
		lecturerTaskRepository.deleteAll(locationTask.getLecturerTasks());
		giveawayTaskRepository.deleteAll(locationTask.getGiveawayTasks());
		locationTaskRepository.delete(locationTask);
		
		updateTrainingDateAfterDelete(locationTask);
		
		return true;
		
	}

	

//OTHER

	private void updateTrainingDateAfterSave(LocationTask locationTask) {
		boolean shouldTrainingBeChanged = false;
		Training training = locationTask.getTraining();
		if(training.getStartDate() == null || locationTask.getStartDate().isBefore(training.getStartDate())) {
			shouldTrainingBeChanged = true;
			training.setStartDate(locationTask.getStartDate());
		}
		
		if(training.getEndDate() == null || locationTask.getEndDate().isAfter(training.getEndDate())) {
			shouldTrainingBeChanged = true;
			training.setEndDate(locationTask.getEndDate());
		}
		
		if(shouldTrainingBeChanged == true) {
			trainingRepository.save(training);
		}
	}
	
	private void updateTrainingDateAfterDelete(LocationTask locationTask) {
		Training training = locationTask.getTraining();
		boolean shouldTrainingBeChanged = false;
		if(locationTask.getStartDate().isEqual(training.getStartDate())) {
			shouldTrainingBeChanged = true;
			LocalDateTime minDateTime = null;
			if(training.getLocationTasks() != null) {
				minDateTime = training.getLocationTasks().stream()
															.map(LocationTask::getStartDate)
															.min((s1, s2) -> {return s1.compareTo(s2);})
															.orElse(null);
			}
			training.setStartDate(minDateTime);
		}
		
		if(locationTask.getEndDate().isEqual(training.getEndDate())) {
			shouldTrainingBeChanged = true;
			LocalDateTime maxDateTime = null;
			if(training.getLocationTasks() != null) {
				maxDateTime = training.getLocationTasks().stream()
															.map(LocationTask::getEndDate)
															.max((s1, s2) -> {return s1.compareTo(s2);})
															.orElse(null);
			}
			training.setEndDate(maxDateTime);
		}
		
		if(shouldTrainingBeChanged == true) {
			trainingRepository.save(training);
		}
	}
}
	