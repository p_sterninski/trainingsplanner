package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.LecturerTaskRepository;
import pl.psterninski.planner.model.LecturerTask;

@RestController
@RequestMapping("/api/lecturerTask")
public class LacturerTaskController {

	@Autowired
	private LecturerTaskRepository lecturerTaskRepository;
	
	
//GET
	@GetMapping
	public Iterable<LecturerTask> findAll() {
		return lecturerTaskRepository.findAll();
	}

	
//POST
	@PostMapping
	public LecturerTask create(@RequestBody LecturerTask lecturerTask) {
		return lecturerTaskRepository.save(lecturerTask);
	}
}
