package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.GiveawayTaskRepository;
import pl.psterninski.planner.model.GiveawayTask;

@RestController
@RequestMapping("/api/giveawayTask")
public class GiveawayTaskController {
	
	@Autowired
	private GiveawayTaskRepository giveawayTaskRepository;
	
	
//GET
	@GetMapping
	public Iterable<GiveawayTask> findAll() {
		return giveawayTaskRepository.findAll();
	}

	
//POST
	@PostMapping
	public GiveawayTask create(@RequestBody GiveawayTask gratisTask) {
		return giveawayTaskRepository.save(gratisTask);
	}
}
