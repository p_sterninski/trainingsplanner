package pl.psterninski.planner.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.exception.ValueExistsInDatabase;
import pl.psterninski.planner.model.Invoice;
import pl.psterninski.planner.service.InvoiceService;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceController {

	@Autowired
	public InvoiceService invoiceService;

	
//GET
	@GetMapping
	public Iterable<Invoice> findAll() {
		return invoiceService.findAll();
	}
	
	@GetMapping("/{id}")
	public Invoice findById(@PathVariable Long id) {
		return invoiceService.findById(id);
	}
	
	@GetMapping("/no/{invoiceNo}")
	public Invoice findByInvoiceNo(@PathVariable String invoiceNo) {
		return invoiceService.findByInvoiceNo(invoiceNo);
	}

	
//POST
	@PostMapping
	public Invoice create(@RequestBody Invoice invoice) throws ValueExistsInDatabase {
		return invoiceService.createInvoice(invoice);
	}
	
	@PostMapping("/correction/{previousInvoiceId}")
	public Invoice createCorrection(@PathVariable Long previousInvoiceId, @RequestBody Invoice invoice) throws ValueExistsInDatabase {
		return invoiceService.cancelOldAndCreateNewInvoice(previousInvoiceId, invoice);
	}
	
	@PostMapping("/cancellation/{previousInvoiceId}/{invoiceNo}")
	public Invoice createCorrection(@PathVariable Long previousInvoiceId, @PathVariable String invoiceNo) throws ValueExistsInDatabase {
		return invoiceService.cancelInvoice(previousInvoiceId, invoiceNo);
	}
}
