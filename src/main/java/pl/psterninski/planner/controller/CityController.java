package pl.psterninski.planner.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pl.psterninski.planner.dao.CityRepository;
import pl.psterninski.planner.dao.CountryRepository;
import pl.psterninski.planner.model.City;
import pl.psterninski.planner.model.Country;

@RestController
@RequestMapping("/api/city")
public class CityController {
	
	@Autowired
	private CityRepository cityRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
//GET
	@GetMapping
	public Iterable<City> findAll() {
		return cityRepository.findAll();
	}
	
	@GetMapping("/country/{countryId}/name/{name}")
	public City findByNameAndCountryId(@PathVariable String name, @PathVariable Long countryId) {
		return cityRepository.findByNameIgnoreCaseAndCountryCountryId(name, countryId).orElseThrow();
	}
	
//POST
	@PostMapping
	public City create(@RequestBody City newCity) {
		Long newCityCountryId = newCity.getCountry().getCountryId();
		Optional<City> city = cityRepository.findByNameIgnoreCaseAndCountryCountryId(newCity.getName(), newCityCountryId);
		if(city.isPresent()) {
			return city.orElseThrow();
		} 
		
		Country country = countryRepository.findById(newCityCountryId).orElseThrow();
		newCity.setCountry(country);
		return cityRepository.save(newCity);
	}
}
