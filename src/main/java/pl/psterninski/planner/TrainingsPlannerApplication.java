package pl.psterninski.planner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import pl.psterninski.planner.dao.EmployeeRepository;

@SpringBootApplication
@EnableJpaRepositories(basePackageClasses = EmployeeRepository.class)
public class TrainingsPlannerApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(TrainingsPlannerApplication.class, args);
		
	}
}


