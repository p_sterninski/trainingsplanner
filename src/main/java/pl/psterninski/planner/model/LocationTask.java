package pl.psterninski.planner.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name = "LOCATION_TASK")
@Table(name = "LOCATION_TASK")
@PrimaryKeyJoinColumn(name = "TASK_ID")
@Data
public class LocationTask extends Task {
	
	@Column(name = "START_DATE", nullable = false)
	private LocalDateTime startDate;
	
	@Column(name = "END_DATE", nullable = false)
	private LocalDateTime endDate;
	
	@Column(name = "EXPECTED_NO_OF_ATTENDEES", nullable = false)
	private int expectedNoOfAttendees;
	
	@Column(name = "ACTUAL_NO_OF_ATTENDEES")
	private Integer actualNoOfAttendees;
	
	@ManyToOne
	@JoinColumn(name="TRAINING_ID", nullable = false)
	@JsonIgnoreProperties(value = "locationTasks", allowSetters = true)
	private Training training;
	
	@ManyToOne
	@JoinColumn(name = "ROOM_ID", nullable = false)
	@JsonIgnoreProperties(value = "locationTasks", allowSetters = true)
	private Room room;
	
	@OneToMany(mappedBy="locationTask")
	@JsonIgnoreProperties(value = "locationTask", allowSetters = true)
	private List<GiveawayTask> giveawayTasks = new ArrayList<GiveawayTask>();
	
	@OneToMany(mappedBy="locationTask")
	@JsonIgnoreProperties(value = "locationTask", allowSetters = true)
	private List<LecturerTask> lecturerTasks = new ArrayList<LecturerTask>();
}
