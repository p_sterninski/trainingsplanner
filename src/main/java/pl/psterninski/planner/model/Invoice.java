package pl.psterninski.planner.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.ToString;

@Entity(name="INVOICE")
@Table(name="INVOICE")
@Data
@ToString(exclude="items")
public class Invoice {

	@Id
	@SequenceGenerator(name="invoice_seq", sequenceName="invoice_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="invoice_seq")
	@Column(name="INVOICE_ID")
	private Long invoiceId;

	@Column(name="INVOICE_NO", nullable = false)
	private String invoiceNo;

	@Column(name="VALUE", nullable = false)
	private BigDecimal value;

	@Column(name="INVOICE_DATE", nullable = false)
	private LocalDate invoiceDate;

	@Column(name="SALES_DATE", nullable = false)
	private LocalDate salesDate;

	@Column(name="IS_COST_INVOICE", nullable = false)
	private boolean isCostInvoice;
	
	@Column(name="IS_CORRECTION", nullable = false)
	private boolean isCorrection;
	
	@Column(name="IS_CANCELLED", nullable = false)
	private boolean isCancelled;
	
	@Column(name="IS_LAST", nullable = false)
	private boolean isLast;
	
	@ManyToOne
	@JoinColumn(name="INITIAL_INVOICE_ID")
	@JsonIgnoreProperties(value = {"initialInvoice"}, allowSetters = true)
	private Invoice initialInvoice;
	
	@ManyToOne
	@JoinColumn(name="PERSON_ID", nullable = false)
	@JsonIgnoreProperties(value = "invoices", allowSetters = true)
	private Person person;
	
	@OneToMany(mappedBy="invoice")
	@JsonIgnoreProperties(value = "invoice", allowSetters = true)
	private List<InvoiceItem> items = new ArrayList<InvoiceItem>();
	
	
	//During json deseralization, jackson looks for setter with prefix "set" so for example for isCostInvoice it looks for setIsCostInvoice
	//, but because that method doesn't exists assignment does not happen.
	//Using annotation @JsonProperty("isCostInvoice") jackson knows that isCostInvoice property in json should be processed by annotated method.
	@JsonProperty("isCostInvoice")
	public void setCostInvoice(boolean isCostInvoice) {
		this.isCostInvoice = isCostInvoice;
	}
	
	@JsonProperty("isCorrection")
	public void setCorrection(boolean isCorrection) {
		this.isCorrection = isCorrection;
	}
	
	@JsonProperty("isCancelled")
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}
	
	@JsonProperty("isLast")
	public void setLast(boolean isLast) {
		this.isLast = isLast;
	}
}
