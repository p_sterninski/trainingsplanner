package pl.psterninski.planner.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name="TRAINING")
@Table(name="TRAINING")
@Data
public class Training {

	@Id
	@SequenceGenerator(name="training_seq", sequenceName="training_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="training_seq")
	@Column(name="TRAINING_ID")
	private Long trainingId;
	
	@Column(name = "CODE", nullable = false, length = 5)
	private String code;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@Column(name = "START_DATE", nullable = true)
	private LocalDateTime startDate;
	
	@Column(name = "END_DATE", nullable = true)
	private LocalDateTime endDate;
	
	@ManyToOne
	@JoinColumn(name="CATEGORY_ID", nullable = false)
	@JsonIgnoreProperties(value = "trainings", allowSetters = true)
	private TrainingCategory category;
	
	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(
			name = "TRAINING_TEAM",
			joinColumns = {@JoinColumn(name = "trainingId")},
			inverseJoinColumns = { @JoinColumn(name = "personId") }
			)
	@JsonIgnoreProperties(value = "trainings", allowSetters = true)
	private List<Employee> employees = new ArrayList<Employee>();
	
	@OneToMany(mappedBy = "training")
	@JsonIgnoreProperties(value = "training", allowSetters = true)
	private List<LocationTask> locationTasks = new ArrayList<LocationTask>();
	
	@OneToMany(mappedBy = "training")
	@JsonIgnoreProperties(value = "training", allowSetters = true)
	private List<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>();
}
