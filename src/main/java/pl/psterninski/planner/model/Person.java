package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity(name="PERSON")
@Table(name="PERSON")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude="invoices")
@EqualsAndHashCode //exclude={"invoices", "city"} and additionally include={getCityId()}
public class Person {

	@Id
	@SequenceGenerator(name="person_seq", sequenceName="person_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="person_seq")
	@Column(name="PERSON_ID")
	private Long personId;

	@Column(name="IS_LEGAL_PERSON", nullable = false)
	private boolean isLegalPerson;

	@Column(name="NAME", nullable = false, length = 40)
	private String name;

	@Column(name="ADDRESS", nullable = false)
	private String address;
	
	@Column(name="PHONE", length = 15)
	private String phone;
	
	@Column(name="EMAIL")
	private String email;
	
	@Column(name="TAX_NUMBER")
	private String taxNo;		//tax identification number
	
	@Column(name="NATIONAL_NUMBER")
	private String nationalNo; 	//national identification number
	
	@ManyToOne
	@JoinColumn(name="CITY_ID", nullable = false)
	@JsonIgnoreProperties(value = {"people", "locations"}, allowSetters = true)
	@EqualsAndHashCode.Exclude
	private City city;
	
	@OneToMany(mappedBy="person")
	@JsonIgnoreProperties(value = {"person", "initialInvoice"}, allowSetters = true)
	@EqualsAndHashCode.Exclude
	private List<Invoice> invoices = new ArrayList<Invoice>();
	
	
	@EqualsAndHashCode.Include
	private Long getCityId() {
		return this.getCity().getCityId();
	}
	//During json deseralization, jackson looks for setter with prefix "set" so for isLegalPerson it looks for setIsLegalPerson
	//, but because that method doesn't exists assignment does not happen.
	//Using annotation @JsonProperty("isLegalPerson") jackson knows that isLegalPerson property in json should be processed by annotated method.
	@JsonProperty("isLegalPerson")
	public void setLegalPerson(boolean isLegalPerson) {
		this.isLegalPerson = isLegalPerson;
	}
	
	public void changeBlankStringToNullForNullableColumn() {
		if(getPhone() != null && getPhone().isBlank()) {
			setPhone(null);
		}
		
		if(getEmail() != null && getEmail().isBlank()) {
			setEmail(null);
		}
		
		if(getNationalNo() != null && getNationalNo().isBlank()) {
			setNationalNo(null);
		}
		
		if(getTaxNo() != null && getTaxNo().isBlank()) {
			setTaxNo(null);
		}
	}
	
	public static Person copyPerson(Person person) {
		return new Person(
				person.getPersonId()
				, person.isLegalPerson()
				, person.getName()
				, person.getAddress()
				, person.getPhone()
				, person.getEmail()
				, person.getTaxNo()
				, person.getNationalNo()
				, person.getCity()
				, person.getInvoices()
				);
	}
	
	public static Person copyPersonAndReplaceEmptyFields(Person person, Person personReplacement) {
		return new Person(
				person.getPersonId() == null ? personReplacement.getPersonId() : person.getPersonId()
				, person.isLegalPerson()
				, person.getName() == null ? personReplacement.getName() : person.getName()
				, person.getAddress() == null ? personReplacement.getAddress() : person.getAddress()
				, person.getPhone() == null ? personReplacement.getPhone() : person.getPhone()
				, person.getEmail() == null ? personReplacement.getEmail() : person.getEmail()
				, person.getTaxNo() == null ? personReplacement.getTaxNo() : person.getTaxNo()
				, person.getNationalNo() == null ? personReplacement.getNationalNo() : person.getNationalNo()
				, person.getCity() == null ? personReplacement.getCity() : person.getCity()
				, person.getInvoices().isEmpty() ? personReplacement.getInvoices() : person.getInvoices()
				);
	}
	
	public void addPersonDataWithoutInvoicesFrom(Person person) {
		this.setPersonId(person.getPersonId());
		this.setName(person.getName());
		this.setLegalPerson(person.isLegalPerson());
		this.setNationalNo(person.getNationalNo());
		this.setTaxNo(person.getTaxNo());
		this.setAddress(person.getAddress());
		this.setCity(person.getCity());
		this.setEmail(person.getEmail());
		this.setPhone(person.getPhone());
	}
	
}
