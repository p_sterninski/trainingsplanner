package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity(name="EMPLOYEE")
@Table(name="EMPLOYEE")
@PrimaryKeyJoinColumn(name="PERSON_ID")
@Data
@NoArgsConstructor
@ToString(callSuper=true, exclude={"trainings", "roles"})
@EqualsAndHashCode(callSuper=true) // exclude={"department", "trainings", "roles"}
public class Employee extends Person {
	
	@Column(name="EMPLOYEE_NO", nullable = false)
	private String employeeNo;
	
	@Column(name="IS_ACTIVE", nullable = false)
	private boolean isActive;
	
	@Column(name="POSITION", nullable = false)
	private String position;
	
	//Password can be null because not every employee has to have authenticated access to application
	@EqualsAndHashCode.Exclude
	@Column(name="PASSWORD")
	private String password;
	
	@Column(name="WORK_EMAIL", nullable = false)
	private String workEmail;
	
	@ManyToOne
	@JoinColumn(name="DEPARTMENT_ID", nullable = false)
	@JsonIgnoreProperties(value = "employees", allowSetters = true)
	@EqualsAndHashCode.Exclude
	private Department department;
	
	@ManyToMany(mappedBy = "employees")
	@JsonIgnoreProperties(value = "employees", allowSetters = true)
	@EqualsAndHashCode.Exclude
	private List<Training> trainings = new ArrayList<Training>();
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "USER_ROLE",
			joinColumns = { @JoinColumn(name = "userId") },
			inverseJoinColumns = { @JoinColumn(name = "roleId") }
			)
	@JsonIgnoreProperties(value = "users", allowSetters = true)
	@EqualsAndHashCode.Exclude
	private Set<Role> roles = new HashSet<Role>();
	
	//During json deseralization, jackson looks for setter with prefix "set" so for isActive it looks for setIsActive
	//, but because that method doesn't exists assignment does not happen.
	//Using annotation @JsonProperty("isActive") jackson knows that isActive property in json should be processed by annotated method.
	@JsonProperty("isActive")
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	@EqualsAndHashCode.Include
	private Long getDepartmentId() {
		return this.getDepartment().getDepartmentId();
	}
	
	public Employee(Long personId, boolean isLegalPerson, String name, String address, String phone, String email, String taxNo, String nationalNo, City city, List<Invoice> invoices
					, String employeeNo, boolean isActive, String position, String password, String workEmail, Department department, List<Training> trainings, Set<Role> roles) {
		super(personId, isLegalPerson, name, address, phone, email, taxNo, nationalNo, city, invoices);
		this.setEmployeeNo(employeeNo);
		this.setActive(isActive);
		this.setPosition(position);
		this.setPassword(password);
		this.setWorkEmail(workEmail);
		this.setDepartment(department);
		this.setTrainings(trainings);
		this.setRoles(roles);
	}
	
	public static Employee copyEmployeeAndReplaceEmptyFields(Employee employee, Employee employeeReplacement) {
		return new Employee(
				employee.getPersonId() == null ? employeeReplacement.getPersonId() : employee.getPersonId()
				, employee.isLegalPerson()
				, employee.getName() == null ? employeeReplacement.getName() : employee.getName()
				, employee.getAddress() == null ? employeeReplacement.getAddress() : employee.getAddress()
				, employee.getPhone() == null ? employeeReplacement.getPhone() : employee.getPhone()
				, employee.getEmail() == null ? employeeReplacement.getEmail() : employee.getEmail()
				, employee.getTaxNo() == null ? employeeReplacement.getTaxNo() : employee.getTaxNo()
				, employee.getNationalNo() == null ? employeeReplacement.getNationalNo() : employee.getNationalNo()
				, employee.getCity() == null ? employeeReplacement.getCity() : employee.getCity()
				, employee.getInvoices().isEmpty() ? employeeReplacement.getInvoices() : employee.getInvoices()
				//Employee data
				, employee.getEmployeeNo() == null ? employeeReplacement.getEmployeeNo(): employee.getEmployeeNo()
				, employee.isActive()
				, employee.getPosition() == null ? employeeReplacement.getPosition(): employee.getPosition()
				, employee.getPassword() == null ? employeeReplacement.getPassword(): employee.getPassword()
				, employee.getWorkEmail() == null ? employeeReplacement.getWorkEmail(): employee.getWorkEmail()
				, employee.getDepartment() == null ? employeeReplacement.getDepartment(): employee.getDepartment()
				, employee.getTrainings().isEmpty() ? employeeReplacement.getTrainings(): employee.getTrainings()
				, employee.getRoles().isEmpty() ? employeeReplacement.getRoles(): employee.getRoles()
				);
	}

	public static Employee copyEmployeeAndReplaceEmptyFields(Person person, Employee employeeReplacement) {
		return new Employee(
				person.getPersonId() == null ? employeeReplacement.getPersonId() : person.getPersonId()
				, person.isLegalPerson()
				, person.getName() == null ? employeeReplacement.getName() : person.getName()
				, person.getAddress() == null ? employeeReplacement.getAddress() : person.getAddress()
				, person.getPhone() == null ? employeeReplacement.getPhone() : person.getPhone()
				, person.getEmail() == null ? employeeReplacement.getEmail() : person.getEmail()
				, person.getTaxNo() == null ? employeeReplacement.getTaxNo() : person.getTaxNo()
				, person.getNationalNo() == null ? employeeReplacement.getNationalNo() : person.getNationalNo()
				, person.getCity() == null ? employeeReplacement.getCity() : person.getCity()
				, person.getInvoices().isEmpty() ? employeeReplacement.getInvoices() : person.getInvoices()
				//Employee data
				, employeeReplacement.getEmployeeNo()
				, employeeReplacement.isActive()
				, employeeReplacement.getPosition()
				, employeeReplacement.getPassword()
				, employeeReplacement.getWorkEmail()
				, employeeReplacement.getDepartment()
				, employeeReplacement.getTrainings()
				, employeeReplacement.getRoles()
				);
	}
}
