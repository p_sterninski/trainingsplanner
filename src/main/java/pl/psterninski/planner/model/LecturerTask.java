package pl.psterninski.planner.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name = "LECTURER_TASK")
@Table(name = "LECTURER_TASK")
@PrimaryKeyJoinColumn(name = "TASK_ID")
@Data
public class LecturerTask extends Task {

	@ManyToOne
	@JoinColumn(name = "PERSON_ID", nullable = false)
	@JsonIgnoreProperties(value = "trainings", allowSetters = true)
	private Person lecturer;
	
	@ManyToOne
	@JoinColumn(name = "LOCATION_TASK_ID", nullable = false)
	@JsonIgnoreProperties(value = "lecturerTasks", allowSetters = true)
	private LocationTask locationTask;
	
	
}
