package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name = "LOCATION")
@Table(name="LOCATION")
@Data
public class Location {

	@Id
	@SequenceGenerator(name="location_seq", sequenceName="location_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="location_seq")
	@Column(name="LOCATION_ID")
	private Long locationId;
	
	@Column(name="NAME", nullable = false)
	private String name;
	
	@Column(name="ADDRESS", nullable = false)
	private String address;
	
	@Column(name="PHONE", length = 15)
	private String phone;
	
	@Column(name="EMAIL")
	private String email;
	
	@ManyToOne
	@JoinColumn(name="CITY_ID", nullable = false)
	@JsonIgnoreProperties(value = {"locations", "people"}, allowSetters = true)
	private City city;
	
	@OneToMany(mappedBy = "location")
	@JsonIgnoreProperties(value = "location", allowSetters = true)
	private List<Room> rooms = new ArrayList<Room>();
}
