package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name = "PRODUCT")
@Table(name = "PRODUCT")
@Data
public class Product {
	@Id
	@SequenceGenerator(name="product_seq", sequenceName="product_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="product_seq")
	@Column(name="PRODUCT_ID")
	private Long productId;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@OneToMany(mappedBy = "product")
	@JsonIgnoreProperties(value = "product", allowSetters = true)
	private List<GiveawayTask> giveawayTasks = new ArrayList<GiveawayTask>();
	
}
