package pl.psterninski.planner.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity(name = "TASK")
@Table(name = "TASK")
@Inheritance(strategy = InheritanceType.JOINED)
@Data
public class Task {
	
	@Id
	@SequenceGenerator(name="task_seq", sequenceName="task_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="task_seq")
	@Column(name = "TASK_ID")
	private Long taskId;
	
	@Column(name = "DESCRIPTION")
	private String description;
}
