package pl.psterninski.planner.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity(name = "ROLE")
@Table(name = "ROLE")
@Data
public class Role {
	@Id
	@SequenceGenerator(name = "role_seq", sequenceName = "role_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="role_seq")
	@Column(name = "ROLE_ID")
	private Long roleId;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@ManyToMany(mappedBy = "roles", fetch = FetchType.EAGER)
	@JsonIgnoreProperties(value = "roles", allowSetters = true)
	@EqualsAndHashCode.Exclude
	private Set<Employee> users = new HashSet<Employee>();
	
}
