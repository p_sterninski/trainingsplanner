package pl.psterninski.planner.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name="GIVEAWAY_TASK")
@Table(name="GIVEAWAY_TASK")
@PrimaryKeyJoinColumn(name="TASK_ID")
@Data
public class GiveawayTask extends Task {
	
	@Column(name = "AMOUNT", nullable = false)
	private int amount;
	
	@ManyToOne
	@JoinColumn(name = "PRODUCT_ID", nullable = false)
	@JsonIgnoreProperties(value = "giveawayTasks", allowSetters = true)
	private Product product;
	
	@ManyToOne
	@JoinColumn(name = "LOCATION_TASK_ID", nullable = false)
	@JsonIgnoreProperties(value = "giveawayTasks", allowSetters = true)
	private LocationTask locationTask;
	
}
