package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Entity(name="DEPARTMENT")
@Table(name="DEPARTMENT")
@Data
@ToString(exclude="employees")
@EqualsAndHashCode(exclude="employees")
public class Department {
	@Id
	@SequenceGenerator(name="department_seq", sequenceName="department_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="department_seq")
	@Column(name="DEPARTMENT_ID")
	private Long departmentId;
	
	@Column(name="NAME", nullable = false)
	private String name;
	
	@OneToMany(mappedBy="department")
	@JsonIgnoreProperties(value = "department", allowSetters = true)
	private List<Employee> employees = new ArrayList<Employee>();
}
