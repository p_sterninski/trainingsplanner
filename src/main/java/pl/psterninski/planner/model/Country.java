package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Entity(name="COUNTRY")
@Table(name="COUNTRY")
@Data
@ToString(exclude="cities")
public class Country {
	@Id
	@SequenceGenerator(name="country_seq", sequenceName="country_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="country_seq")
	@Column(name = "COUNTRY_ID")
	private Long countryId;
	
	@Column(name = "NAME")
	private String name;
	
	@OneToMany(mappedBy = "country")
	@JsonIgnoreProperties(value = "country", allowSetters = true)
	private List<City> cities = new ArrayList<City>();
}
