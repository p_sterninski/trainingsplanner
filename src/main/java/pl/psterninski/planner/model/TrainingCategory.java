package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Entity(name="TRAINING_CATEGORY")
@Table(name="TRAINING_CATEGORY")
@Data
@ToString(exclude="trainings")
public class TrainingCategory {

	@Id
	@SequenceGenerator(name="training_category_seq", sequenceName="training_category_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="training_category_seq")
	@Column(name="CATEGORY_ID")
	private Long categoryId;
	
	@Column(name="NAME", nullable = false)
	private String name;
	
	@OneToMany(mappedBy="category")
	@JsonIgnoreProperties(value = "category", allowSetters = true)
	private List<Training> trainings = new ArrayList<Training>();
}
