
package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name = "ROOM")
@Table(name="ROOM")
@Data
public class Room {

	@Id
	@SequenceGenerator(name="room_seq", sequenceName="room_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="room_seq")
	@Column(name="ROOM_ID")
	private Long roomId;
	
	@Column(name = "ROOM_NO", nullable = false)
	private String roomNo;
	
	@Column(name = "CAPACITY", nullable = false)
	private int capacity;
	
	@Column(name = "FLOOR_NO", nullable = false)
	private int floorNo;
	
	@ManyToOne
	@JoinColumn(name="LOCATION_ID", nullable = false)
	@JsonIgnoreProperties(value = "rooms", allowSetters = true)
	private Location location;
	
	@OneToMany(mappedBy = "room")
	@JsonIgnoreProperties(value = "room", allowSetters = true)
	private List<LocationTask> locationTasks = new ArrayList<LocationTask>();
}
