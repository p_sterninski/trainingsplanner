package pl.psterninski.planner.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Entity(name="INVOICE_ITEM")
@Table(name="INVOICE_ITEM")
@Data

public class InvoiceItem {

	@Id
	@SequenceGenerator(name="invoice_item_seq", sequenceName="invoice_item_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="invoice_item_seq")
	@Column(name="INVOICE_ITEM_ID")
	private Long itemId;
	
	@Column(name="VALUE", nullable = false)
	private BigDecimal value;
	
	@Column(name="AMOUNT", nullable = false)
	private int amount;

	@Column(name="DESCRIPTION", nullable = false)
	private String description;
	
	@ManyToOne
	@JoinColumn(name="TRAINING_ID", nullable = false)
	@JsonIgnoreProperties(value = {"invoiceItems", "employees", "locationTasks"}, allowSetters = true)
	private Training training;

	@ManyToOne
	@JoinColumn(name="INVOICE_ID", nullable = false)
	@JsonIgnoreProperties(value = {"items", "initialInvoice"}, allowSetters = true)
	private Invoice invoice;
	
	
}
