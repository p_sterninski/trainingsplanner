package pl.psterninski.planner.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.ToString;

@Entity(name="CITY")
@Table(name="CITY")
@Data
@ToString(exclude= {"people", "locations"})
public class City {
	@Id
	@SequenceGenerator(name="city_seq", sequenceName="city_seq", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="city_seq")
	@Column(name="CITY_ID")
	private Long cityId;
	
	@Column(name = "NAME", nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name="COUNTRY_ID", nullable = false)
	@JsonIgnoreProperties("cities")
	private Country country;
	
	@OneToMany(mappedBy = "city")
	@JsonIgnoreProperties(value = "city", allowSetters = true)
	private List<Person> people = new ArrayList<Person>();
	
	@OneToMany(mappedBy = "city")
	@JsonIgnoreProperties(value = "city", allowSetters = true)
	private List<Location> locations = new ArrayList<Location>();
}
