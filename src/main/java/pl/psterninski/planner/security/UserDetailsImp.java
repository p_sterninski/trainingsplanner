package pl.psterninski.planner.security;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import pl.psterninski.planner.model.Employee;

public class UserDetailsImp implements UserDetails {

	private String username;
	@JsonIgnore
	private String password;
	private boolean isActive;
	private List<GrantedAuthority> authorities;
	
	public UserDetailsImp(Employee user) {
		this.username = user.getEmployeeNo();
		this.password = user.getPassword();
		this.isActive = user.isActive();
		this.authorities = user.getRoles().stream()
									.map(role -> new SimpleGrantedAuthority(role.getName()))
									.collect(Collectors.toList());
	}
	
	public UserDetailsImp() {
	}
	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return isActive;
	}

	@Override
	public boolean isAccountNonLocked() {
		return isActive;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return isActive;
	}

	@Override
	public boolean isEnabled() {
		return isActive;
	}
	
	
	public static Optional<UserDetails> getCurrentUserDetails() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		//System.out.println("static aouth: " + authentication);
		UserDetails currentUser = null;
    	if(!(authentication instanceof AnonymousAuthenticationToken)) {
    		currentUser = (UserDetails) authentication.getPrincipal();
    	}
    	return Optional.ofNullable(currentUser);
	}

}
