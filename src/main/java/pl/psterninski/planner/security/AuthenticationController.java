package pl.psterninski.planner.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
	
	@GetMapping("/userDetails")
	public UserDetails getUserDetails() {
		return UserDetailsImp.getCurrentUserDetails().orElse(null);
	}
    
    @GetMapping("/username")
    public String getUsername() {
    	String username = "";
    	if (UserDetailsImp.getCurrentUserDetails().isPresent()) {
    		username = UserDetailsImp.getCurrentUserDetails().get().getUsername();
    	}
    	return username;
    } 
}
