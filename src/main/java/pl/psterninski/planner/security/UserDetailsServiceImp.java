package pl.psterninski.planner.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pl.psterninski.planner.model.Employee;
import pl.psterninski.planner.service.EmployeeService;

@Service
public class UserDetailsServiceImp implements UserDetailsService {
	
	@Autowired
	private EmployeeService employeeService;
	
	@Override
	public UserDetails loadUserByUsername(String employeeNo) throws UsernameNotFoundException {
		Employee employee;
		
		try{
			employee = employeeService.findByEmployeeNo(employeeNo);
		} catch(Exception e) {
			throw new UsernameNotFoundException("Not found: " + employeeNo);
		}

		return new UserDetailsImp(employee);
	}

}
