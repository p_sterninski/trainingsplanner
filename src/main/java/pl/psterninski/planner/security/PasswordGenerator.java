package pl.psterninski.planner.security;

import java.security.SecureRandom;
import java.util.Random;

public class PasswordGenerator {
	
	private final static Random random = new SecureRandom();
	private final static String CHARACTER_SET = "0123456789abcdefghijklmnopqrstuvwxyz";
	
	
	public static String generate(int passwordLength) {
		StringBuilder password = new StringBuilder();
		
		for(int i = 0; i < passwordLength; i++) {
			char single_char = CHARACTER_SET.charAt(random.nextInt(CHARACTER_SET.length()));
			if(Character.isLetter(single_char) && random.nextInt(2) == 0) {
				single_char = Character.toUpperCase(single_char);
			}
			password.append(single_char);
		}
		
		return password.toString();
	}
}
