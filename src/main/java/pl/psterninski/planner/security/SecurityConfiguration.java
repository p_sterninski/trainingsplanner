package pl.psterninski.planner.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter{
	
	@Autowired
	UserDetailsService userDetailsService;
	
	@Autowired
	private RESTAuthenticationSuccessHandler restAuthenticationSuccessHandler;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService)
			.passwordEncoder(getPasswordEncoder());
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
			.authorizeRequests()
				//.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				//Not logged
				.antMatchers(
						"/"
						, "/bundle.js"
						, "/login"
						, "/api/login"
						, "/api/auth/username"
						, "/api/auth/userDetails"
						, "/api/training"
						, "/api/training/{id}"
						, "/api/employee/{employeeNo}/resetPassword").permitAll()
	        	.antMatchers("/h2-console/**").permitAll()
				//All authenticated
	        	.antMatchers("/api/employee/changePassword/{currentPassword}/{newPassword}").authenticated()
	        	//ADMIN
				.antMatchers("/api/role/**").hasAnyRole("ADMIN")
				.antMatchers("/api/employee/{personId}/role").hasAnyRole("ADMIN")
	        	//ACCOUNTING
				.antMatchers("/api/invoice/**").hasAnyRole("ADMIN", "ACCOUNTING")
				.antMatchers("/api/invoiceItem/**").hasAnyRole("ADMIN", "ACCOUNTING")
				//TRAINING
				.antMatchers("/api/trainingCategory/**").hasAnyRole("ADMIN", "TRAINING")
				.antMatchers("/api/training/**").hasAnyRole("ADMIN", "TRAINING")
				.antMatchers("/api/giveawayTask/**").hasAnyRole("ADMIN", "TRAINING")
				.antMatchers("/api/lecturerTask/**").hasAnyRole("ADMIN", "TRAINING")
				.antMatchers("/api/locationTask/**").hasAnyRole("ADMIN", "TRAINING")
				.antMatchers("/api/product/**").hasAnyRole("ADMIN", "TRAINING")
				.antMatchers("/api/employee/onlyTrainingAndActive").hasAnyRole("ADMIN", "TRAINING", "HR")
				//HR
				.antMatchers("/api/employee/**").hasAnyRole("ADMIN", "HR")
	        	//Other request only for authenticated
	        	.anyRequest().authenticated()
        	.and()
        	.formLogin()
	    		.loginPage("/login")
	    		.loginProcessingUrl("/api/login")
	    		.successHandler(restAuthenticationSuccessHandler)
	    		.permitAll()
    		.and()
        	.logout()
        		.logoutUrl("/api/logout").permitAll()
        		.deleteCookies("JSESSIONID")
        		.logoutSuccessHandler((httpServletRequest, httpServletResponse, authentication) -> {
        			httpServletResponse.setStatus(HttpStatus.OK.value());
        		});
		

		http.headers().frameOptions().disable();
	}
	
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
}
