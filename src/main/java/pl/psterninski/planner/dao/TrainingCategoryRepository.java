package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.TrainingCategory;

public interface TrainingCategoryRepository extends CrudRepository<TrainingCategory, Long> {
	
	Optional<TrainingCategory> findByNameIgnoreCase(String name);
}
