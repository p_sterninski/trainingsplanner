package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Person;

public interface PersonRepository<T extends Person> extends CrudRepository<T, Long> {

	Optional<T> findByTaxNo(String taxNo);
	Optional<T> findByNationalNo(String nationalNo);
	Iterable<T> findByIsLegalPerson(boolean isLegalPerson);
	Optional<T> findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(String name, String address, Long cityId);
}
