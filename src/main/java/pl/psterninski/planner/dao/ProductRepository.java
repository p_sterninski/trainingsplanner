package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Product;

public interface ProductRepository extends CrudRepository<Product, Long> {

	Optional<Product> findByNameIgnoreCase(String name);
}
