package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.GiveawayTask;

public interface GiveawayTaskRepository extends CrudRepository<GiveawayTask, Long> {

}
