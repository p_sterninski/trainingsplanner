package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Location;

public interface LocationRepository extends CrudRepository<Location, Long>  {
	
	Optional<Location> findByNameIgnoreCaseAndAddressIgnoreCaseAndCityCityId(String name, String address, Long cityId);

}
