package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import pl.psterninski.planner.model.Employee;

public interface EmployeeRepository extends PersonRepository<Employee> {

	Optional<Employee> findByEmployeeNo(String employeeNo);
	Iterable<Employee> findByIsActiveAndDepartmentDepartmentId(boolean isActive, Long departmentId);
    
	@Modifying
    @Query(value = "INSERT INTO EMPLOYEE (PERSON_ID, EMPLOYEE_NO, IS_ACTIVE, POSITION, WORK_EMAIL, DEPARTMENT_ID) "
    			+ "	VALUES (:personId, :employeeNo, :isActive, :position, :workEmail, :departmentId)", nativeQuery = true)
    @Transactional
    int insertNewEmployeeForExisitingPerson(@Param("personId") Long personId, @Param("employeeNo") String employeeNo, @Param("isActive") boolean isActive, @Param("position") String position, @Param("workEmail") String workEmail, @Param("departmentId") Long departmentId);
}
