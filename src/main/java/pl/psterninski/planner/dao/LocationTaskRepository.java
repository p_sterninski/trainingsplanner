package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.LocationTask;

public interface LocationTaskRepository extends CrudRepository<LocationTask, Long> {

}
