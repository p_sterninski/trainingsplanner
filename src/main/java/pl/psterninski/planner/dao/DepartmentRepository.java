package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Department;

public interface DepartmentRepository extends CrudRepository<Department, Long>  {

}
