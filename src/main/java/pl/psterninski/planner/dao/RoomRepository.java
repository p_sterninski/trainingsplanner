package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Room;

public interface RoomRepository extends CrudRepository<Room, Long> {
	Iterable<Room> findByLocationLocationId(Long locationId);
	Optional<Room> findByRoomNoIgnoreCaseAndLocationLocationId(String roomNo, Long locationId);

}
