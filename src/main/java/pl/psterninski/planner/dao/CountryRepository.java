package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Country;

public interface CountryRepository extends CrudRepository<Country, Long> {

}
