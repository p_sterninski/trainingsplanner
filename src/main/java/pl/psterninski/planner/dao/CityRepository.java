package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.City;

public interface CityRepository extends CrudRepository<City, Long> {
	Optional<City> findByNameIgnoreCaseAndCountryCountryId(String name, Long countryId);
}
