package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Invoice;

public interface InvoiceRepository extends CrudRepository<Invoice, Long> {

	Optional<Invoice> findByInvoiceNo(String invoiceNo);

}
