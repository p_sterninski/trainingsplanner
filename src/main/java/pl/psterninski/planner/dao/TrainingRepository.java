package pl.psterninski.planner.dao;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pl.psterninski.planner.model.Training;

@Repository
public interface TrainingRepository extends CrudRepository<Training, Long>  {
	
	Optional<Training> findByCodeIgnoreCase(String code);
	Iterable<Training> findByEmployeesEmployeeNo(String employeeNo);
}
