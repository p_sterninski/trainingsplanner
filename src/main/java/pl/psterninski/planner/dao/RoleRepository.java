package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

}
