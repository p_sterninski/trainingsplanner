package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.LecturerTask;

public interface LecturerTaskRepository extends CrudRepository<LecturerTask, Long> {

}
