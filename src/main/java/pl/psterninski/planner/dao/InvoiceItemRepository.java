package pl.psterninski.planner.dao;

import org.springframework.data.repository.CrudRepository;

import pl.psterninski.planner.model.InvoiceItem;

public interface InvoiceItemRepository extends CrudRepository<InvoiceItem, Long>{

}
